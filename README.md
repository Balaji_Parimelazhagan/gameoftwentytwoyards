#GAME OF TWENTY TWO YARDS

##ABOUT
Game Of Twenty Two Yards is a cricket based apllication that offers you
to store the details of a player, create teams with the combination of players,
create match between two teams.

##DEPENDENTS MUST BE INSTALLED
 * JAVA (Recommanded version - 1.8.0_222).
 * MySQL Database.
 * Hibernate Framework.

##TECHNOLOGY USED
 * **Front end _JSP_**.
 * **Back end _Java_**.
 * **Database _MySQL_**.

##AUTHOR & COPYRIGHTS
 **Balaji Parimelazhagan**

##BUILD WITH
 Maven 4.0
