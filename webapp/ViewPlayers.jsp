<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>players</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/viewplayers.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <script src="/js/ViewPlayersAjax.js"></script>
</head>
<body>
  <div class="header">
    <a href="MainMenu.jsp">
      <button id="backButton"><i class="fa fa-arrow-left"></i></button>
    </a>
    <button onclick="location.href='createPlayer';" class="addPlayer">
      <i class="fa fa-user-plus"></i>
    </button>
    <span>PLAYERS</span>
  </div>
  <div class="container">
    <c:forEach var="player" items="${players}">
      <div class= "card">
        <form action="editPlayer" method="post" >
          <input type="hidden" name="id"  value="${player.id}" class= "edit" />
          <button type="submit" id= "editButton">
            <i class='fas fa-user-edit'></i></button>
        </form>
        <form action="viewPlayer" method="post">
          <input type="hidden" name="id"  value="${player.id}" class= "view" />
          <button id= "viewButton" class="viewReference">
            <c:out value="${player.id}"/></button>
        </form>
        <form action="deletePlayer" method="post">
          <input type="hidden" name="id"  value="${player.id}" class= "delete" />
          <button type="submit" id= "deleteButton">
          <i class='fas fa-trash-alt'></i></button>
        </form>
        <div class="content">
          <b><c:out value="${player.playerName}"/></b><br>
          <c:out value="${player.playerRole}"/><br>
          <c:out value="${player.battingStyle}"/><br>
          <c:out value="${player.bowlingStyle}"/><br>
          <c:out value="${player.paceType}"/><br>
          <c:out value="${player.countryName}"/><br>
          <c:out value="${player.playerAge}"/><br>
        </div>
      </div>
    </c:forEach>
  </div>
  <div class="pages">
    <button onclick="callAjax('previous','0');" id="previousPage"><i class="fas fa-angle-left"></i></button>
    <c:forEach var="page" items="${pages}">
      <input type="hidden" name="currentPage"  value="${page}" id= "currentPage" />
      <button onclick="callAjax('number','${page}');"><c:out value="${page}"/></button>
    </c:forEach>
    <button onclick="callAjax('next','0');" id="nextPage"><i class="fas fa-angle-right"></i></button>
  </div>
  <div id="check"></div>
  <div id="checker"></div>
<!--<script>
function callAjax(pageInput, pageNumber) {
    httpRequest = new XMLHttpRequest();
    var page;
    if ("number" === pageInput) {
        document.getElementById('currentPage').value=page;
        page = pageNumber;
    } else if ("previous" === pageInput){
        document.getElementById('check').innerHTML= document.getElementById('currentPage').value;
        page = document.getElementById('currentPage').value;
        page = page - 1;
        document.getElementById('currentPage').value=page;
    } else if ("next" === pageInput){
        document.getElementById('check').innerHTML= document.getElementById('currentPage').value;
        page = document.getElementById('currentPage').value;
        page = page * 1 + 1;
        document.getElementById('currentPage').value=page;
    }
    document.getElementById('check').innerHTML= page;
    httpRequest.open('GET', 'viewAllPlayers?page='+ page);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var array = httpRequest.response;
                var content = document.getElementsByClassName('content');
                var card = document.getElementsByClassName('card');
                for (var i = 0; i < array.length; i++) {
                    card[i].style.display = "inline-block";
                    content[i].innerHTML = "<pre>" + array[i].playerName  
                            + "\n" + array[i].bowlingStyle + "\n" + array[i].battingStyle
                            + "</pre>";
                    document.getElementsByClassName('viewReference')[i].innerHTML = array[i].id;
                    for (var j = array.length; j < card.length; j++) {
                        card[j].style.display = "none";
                    }
                }
            } else {
                console.log('Something went wrong..!!');
            }
        }
    }
}
</script>-->
</body>
</html>
