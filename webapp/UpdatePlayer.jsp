<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<html>
  <head>
    <title>update_player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/playerform.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class = "header">
    <button onclick="location.href='viewPlayers';" id="backButton">
      <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
    </button>
    </div>
    <div class = "container">      
      <form:form action="updatePlayer" method="post"  modelAttribute="playerInfo"
        enctype="multipart/form-data" >
      <fieldset class = "form">
      <legend><input type="file" name="file">
       <i class="fa fa-user-circle-o" style="font-size:80px;color:white;"></i></input>
      </legend>
      <table>
        <tr><td>Name:</td>
            <td><form:input type="text" title="Please enter name with length within 25 characters" name="name" pattern="[A-Za-z][A-Za-z\s]*{25}" placeholder="example: Kaipulla" value="${playerInfo.playerName}" path="playerName" required="required"/></td>
        </tr>
        <div class = "role">
          <tr><td>Role:</td>
          <td class = "roleOption">
          <form:select name="role" style="width:155px" path="playerRole" >
            <option>${playerInfo.playerRole}</option>
            <option>Batsman</option>
            <option>Bowler</option>
            <option>All-Rounder</option>
            <option>Wicket-Keeper</option>
          </form:select></td></tr>
        </div>
        <tr><td>Batting Style:</td><td>
          <form:select name="battingStyle" style="width:155px" path="battingStyle">
            <option>${playerInfo.battingStyle}</option>
            <option>Right-Arm</option>
            <option>Left-Arm</option>
          </form:select></td></tr>
        <tr><td>Bowling Style:</td><td>
          <form:select name="bowlingStyle" style="width:155px" path="bowlingStyle">
            <option>${playerInfo.bowlingStyle}</option>
            <option>Right-Arm</option>
            <option>Left-Arm</option>
          </form:select></td></tr>
        <tr><td>Pace Type:</td><td>
          <form:select name="paceType" style="width:155px" path="paceType">
            <option>${playerInfo.paceType}</option>
            <option>Spin</option>
            <option>Medium-Fast</option>
            <option>Fast</option>
          </form:select></td></tr>
        <tr><td>Date Of Birth</td>
        <td><form:input type = "date" name = "DOB" place-holder = "2018-07-22" path="DOB" 
                  min = "1910-01-01" max = "2004-01-01" value="${playerInfo.DOB}" /></td></tr>
        <tr><td>Country:</td><td>  
          <form:select name="country" style="width:155px" path="countryName">
            <option>${playerInfo.countryName}</option>
            <option>India</option>
            <option>Australia</option>
            <option>Afghanistan</option>
            <option>England</option>
            <option>Pakistan</option>
            <option>Bangladesh</option>
            <option>WestIndies</option>
            <option>SouthAfrica</option>
            <option>SriLanka</option>
            <option>NewZealand</option>
            <option>Zimbawe</option>
          </form:select>
        </td></tr>
        <tr><td>Address:</td><td>
        <form:input type="text" placeholder="No.10, dubai cross street, dubai main road."
          value="${playerInfo.address}" name="adress" id="address" 
          path="address"/></td></tr>
        <tr><td>City:</td><td>
        <form:input type="text" name="city" pattern="[A-Za-z][A-Za-z\s]*{25}"
           placeholder="example: dubai"
           value="${playerInfo.city}" path="city"/></td></tr>
        <tr><td>Contact Number:</td><td>
        <form:input type="number" pattern="\\d{10}" name="contactNumber" 
                placeholder="example: 9566044044"
                value="${playerInfo.contactNumber}" path="contactNumber"/></td></tr>
        <tr><td colspan="2" align="center">
            <form:hidden path="id"/>
            <form:hidden path="contactId"/>
            <input type="hidden" name="imagePath" value="${playerInfo.imagePath}"/></td></tr>
            <input type="submit" name="submit" value="update"/></td></tr>
      </table>
      </fieldset>
      </form:form>
    </div>
    <div class = "footer">
    <!--<h1 id = "webPageName">GAME OF 22 YARDS</h1>-->
    </div>
  </body>
</html>
