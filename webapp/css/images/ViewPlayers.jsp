<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>view_players</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <div align="center">
        <table border="1" cellpadding="5">
            <caption><h2>Players</h2></caption>
            <tr>
                <th>id</th>
                <th>playerName</th>
                <th>playerRole</th>
                <th>battingStyle</th>
                <th>bowlingStyle</th>
                <th>countryName</th>
                <th>playerAge</th>
            </tr>
            <c:forEach var="player" items="${players}">
                <tr>
                    <td><c:out value="${player.id}" /></td>
                    <td><c:out value="${player.playerName}"/></td>
                    <td><c:out value="${player.playerRole}"/></td>
                    <td><c:out value="${player.battingStyle}"/></td>
                    <td><c:out value="${player.bowlingStyle}"/></td>
                    <td><c:out value="${player.countryName}"/></td>
                    <td><c:out value="${player.playerAge}"/></td>
                    <td>
                      <form action="PlayerController" method="post">
                        <input type="hidden" name="playerId" value="${player.id}" />
                        <button name ="submit" value="Edit Player" type="submit">Edit</button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="hidden" name="playerId" value="${player.id}" />
                        <button name ="submit" value="Delete Player" type="submit">Delete</button>
                      </form>  
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>   
</body>
</html>
