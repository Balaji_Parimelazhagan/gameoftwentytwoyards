<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  

<html>
<head>
    <title>view_players</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/playersfetch.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
  <div class="header">
    <button onclick="location.href='viewTeams';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
    </button>
    <form:form action="saveTeam" method="post" modelAttribute="teamInfo">
      <form:hidden  path="id"/>
      <form:hidden  path="country"/>
      <form:hidden  path="teamName"/>
      <button type="submit" name="submit" value="save"><i class="fa fa-save"></i></button>
    <span>PLAYERS</span>
  </div>
  <div class="container">
    <c:forEach var="player" items="${players}">
      <input type="checkbox" name="playerIds" value="${player.id}" id="${player.id}"
       class="check"/>
      <label class="card" for="${player.id}">
        <b><c:out value="${player.id}"/></b><br>
        <b><c:out value="${player.playerName}"/></b><br>
        <c:out value="${player.playerRole}"/><br>
        <c:out value="${player.battingStyle}"/><br>
        <c:out value="${player.bowlingStyle}"/><br>
        <c:out value="${player.paceType}"/><br>
        <c:out value="${player.playerAge}"/><br>
      </label>
    </c:forEach>
    </form:form>
</body>
</html>
