<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
  <head>
    <title>create_player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/playerform.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class = "header">
    </div>
    <div class = "container">
      <form:form action="saveUser" method="post" modelAttribute="userInfo">
        <fieldset class = "form">
        <table>
          <tr><td>Name:</td>
            <td><form:input type="text" name="name" pattern="[A-Za-z][A-Za-z \s]*"
                 placeholder="example: Kaipulla" maxlength="25" path="userName"
                 required="required"/></td>
          </tr>
          <tr><td>Gender:</td>
            <td><form:select name="userGender" style="width:155px" path="userGender"
             required="required">
            <option>Male</option>
            <option>Female</option>
            <option>Transgender</option></form:select>
            </td>
          </tr>
          <tr><td>Role:</td><td>
          <form:select name="userRole" style="width:155px" path="userRole" required="required">
            <option>Administrator</option>
            </form:select></td>
        </tr>
         <tr><td></td><td><div>${message}</div></td></tr>
        <tr><td>Email-Id:</td><td><input type="email" name="userEmailId" path="userEmailId"
          id="email-id"/></td></tr>
        <tr><td><form:input type="password" id="pass" name="password" path="password"
           minlength="8" required="required"/></td></tr>
        <tr><td colspan="2" align="center"><input type="submit" name="submit" value="save"/></td></tr>
      </table>
      </fieldset>
      </form:form>
    </div>
  </body>
</html>
