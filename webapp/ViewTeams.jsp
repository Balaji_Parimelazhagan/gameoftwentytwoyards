<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>teams</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/viewteams.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <script src="/js/ViewTeamsAjax.js"></script>
</head>
<body>
  <div class="header">
    <a href="MainMenu.jsp">
      <button id="backButton"><i class="fa fa-arrow-left"></i></button>
    </a>
    <button onclick="location.href='addTeam';" class="addTeam">
      <i class="fa fa-user-plus"></i>
    </button>
    <span>TEAMS</span>
  </div>
  <div class="container">
    <c:forEach var="team" items="${teams}">
      <div class= "card">
        <form action="editTeam" method="post" id="form">
          <input type="hidden" name="id"  value="${team.id}" class= "edit" />
          <button type="submit" id= "editButton">
            <i class='fas fa-user-edit'></i></button>
        </form>
        <form action="viewTeam" method="post" id="form">
          <input type="hidden" name="id"  value="${team.id}" class= "view" />
          <button id= "viewButton" class="viewReference">
            <c:out value="${team.id}"/></button>
        </form>
        <form action="deleteTeam" method="post" id="form">
          <input type="hidden" name="id"  value="${team.id}" class= "delete" />
          <button type="submit" id= "deleteButton">
          <i class='fas fa-trash-alt'></i></button>
        </form>
        <div class="content">
          <b><c:out value="${team.teamName}"/></b><br>
          <c:out value="${team.country}"/><br>
          <c:out value="${team.teamStatus}"/><br>
        </div>
      </div>
    </c:forEach>
  </div>
  <div class="pages">
    <button onclick="callAjax('previous','0');"><i class="fas fa-angle-left"></i></button>
    <c:forEach var="page" items="${pages}">
      <input type="hidden" name="currentPage"  value="${page}" id= "currentPage" />
      <button onclick="callAjax('number','${page}');"><c:out value="${page}"/></button>
    </c:forEach>
    <button onclick="callAjax('next','0');"><i class="fas fa-angle-right"></i></button>
  </div>
  <div id="modal">
    <div id="confirm-popup">
      <span id="confirm">delete</span>
      The Data will be lost!!!
      <span id="close">x</span>
    </div>
  </div>
</body>
</html>
