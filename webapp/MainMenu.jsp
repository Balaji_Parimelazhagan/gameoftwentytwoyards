<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
  <head>
    <title>game_of_22_yards</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
  </head>
  <body>
    <div class = "header">
    <button onclick="location.href='logOut';"/>LogOut</button>
    </div>
    <div class = "container">
      <div id = "blank"></div>
      <h1 id = "welcome_note">
        <q><span>CRICKET</span> is not just a Sport</br>
        It's an <span>EMOTION</span></q>
      </h1>
      <button onclick="location.href='viewPlayers';" class = "player">
        <div id = "player-text">Player</div>
      </button>
      <button onclick="location.href='viewTeams';" class = "team">
        <div id = "team-text">Team</div>
      </button>
      <button onclick="location.href='viewMatches';" class = "match">
        <div id = "match-text">Match</div>
      </button>
    </div>
    <div class = "footer">
    </div>
  </body>
</html>

