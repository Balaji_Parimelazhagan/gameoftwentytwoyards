<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<html>
  <head>
    <title>create_match</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/creatematch.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class = "header">
      <button onclick="location.href='viewMatches';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
      </button>
    </div>
    <div class = "container">      
      <form:form action="createMatch" method="post" modelAttribute="matchInfo">
        <fieldset class = "form">
          <legend><i class="fa fa-user-circle-o" style="font-size:80px;color:white;"></i></legend>
          <table>
            <tr><td>Name:</td>
              <td><form:input type="text" title="Please enter name within 25 characters"
               name="name" pattern="[A-Za-z][A-Za-z\s]*{25}" path="matchName"
               placeholder="example: World Cup"
               required="required"/></td>
            </tr>
            <tr><td>Location:</td>
              <td><form:input type="text" name="location" pattern="[A-Za-z][A-Za-z\s]*{25}"
               placeholder="example: Chennai" path="matchLocation" required="required"/></td>
            </tr>
            <tr><td>Match Date</td>
              <td><input type = "date" id = "start" name = "date"
               place-holder = "2018-07-22" min = "2017-01-01" 
               required="required"/>
              </td>
            </tr>
            <tr><td>Match-Type:</td><td>  
              <form:select name="type" style="width:155px" path="matchType">
                <option value="t20Match">T20</option>
                <option value="OneDayInternationalMatch">ODI</option>
                <option value="TestMatch">TEST</option>
              </form:select>
            </td></tr>
            <tr><td colspan="2" align="center">
              <input type="submit" name="submit" value="create"/>
              </td></tr>
          </table>
        </fieldset>
      </form:form>
    </div>
  </body>
</html>
