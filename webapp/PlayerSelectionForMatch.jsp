<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>select_players</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/playerselectionformatch.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
</head>
<body>
  <div class="header">
    <button onclick="location.href='viewMatches';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
    </button>
    </button>
    <div id="teamId">
      <c:out value="${match.id}"/>
    </div>
    <span>
      <c:out value="${match.matchName}"/>
    </span>
  </div>
  <form action="startOver" method="post" >
    <div class="container">
      <input type="hidden" name="matchId"  value="${match.id}" />
        <div class = "teamCard">
          <input type="hidden" name="battingTeamName"  value="${battingTeam.teamName}" />
          <b><c:out value="${battingTeam.id}"/><br></b>
          <c:out value="${battingTeam.teamName}"/>
          <div class = "playerCard">
            <c:forEach var="player" items="${battingTeam.playersInTeam}">
                <input type="checkbox" name="batsmen" value="${player.id}" id="${player.id}"
                  class="check"/>
                <label class="player" for="${player.id}">
                  <b><c:out value="${player.playerName}"/></b>&nbsp;&nbsp;
                  <c:out value="${player.playerRole}"/><br>
                </label>
            </c:forEach> 
          </div>
        </div>
        <div class = "teamCard">
          <input type="hidden" name="bowlingTeamName"  value="${bowlingTeam.teamName}" />
          <b><c:out value="${bowlingTeam.id}"/><br></b>
          <c:out value="${bowlingTeam.teamName}"/>
          <div class = "playerCard">
            <c:forEach var="player" items="${bowlingTeam.playersInTeam}">
                <input type="checkbox" name="bowler" value="${player.id}" id="${player.id}"
                  class="check"/>
                <label class="player" for="${player.id}">
                  <b><c:out value="${player.playerName}"/></b>&nbsp;&nbsp;
                  <c:out value="${player.playerRole}"/><br>
                </label>
            </c:forEach> 
          </div>
        </div>
    </div>
    <button type="submit" id= "start">
  </form>
</body>
</html>
