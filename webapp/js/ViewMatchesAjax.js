function callAjax(pageInput, pageNumber) {
    httpRequest = new XMLHttpRequest();
    var page;
    if ("number" === pageInput) {
        document.getElementById('currentPage').value=page;
        page = pageNumber;
    } else if ("previous" === pageInput){
        document.getElementById('check').innerHTML= document.getElementById('currentPage').value;
        page = document.getElementById('currentPage').value;
        page = page - 1;
        document.getElementById('currentPage').value=page;
    } else if ("next" === pageInput){
        document.getElementById('check').innerHTML= document.getElementById('currentPage').value;
        page = document.getElementById('currentPage').value;
        page = page * 1 + 1;
        document.getElementById('currentPage').value=page;
    }
    httpRequest.open('GET', 'viewAllMatches?page='+ page);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var array = httpRequest.response;
                var content = document.getElementsByClassName('content');
                var card = document.getElementsByClassName('card');
                var editValue = document.getElementsByClassName('edit');
                var deleteValue = document.getElementsByClassName('delete');
                var viewValue = document.getElementsByClassName('view');
                for (var i = 0; i < array.length; i++) {
                    card[i].style.display = "inline-block";
                    content[i].innerHTML = "<pre>" + array[i].matchName + "\n" + "VS"  
                            + "\n" + array[i].matchLocation + "\n" + array[i].matchDate 
                            + "\n" + array[i].matchType + "</pre>";
                    editValue[i].value = array[i].id;
                    deleteValue[i].value = array[i].id;
                    viewValue[i].value = array[i].id;
                    document.getElementsByClassName('viewReference')[i].innerHTML = array[i].id;
                    for (var j = array.length; j < card.length; j++) {
                        card[j].style.display = "none";
                    }
                }
            } else {
                console.log('Something went wrong..!!');
            }
        }
    }
}
function popup() {
    var model = document.getElementById('model')
    model.style.display="block";
    var span = document.getElementsByClassName("close")[0];
    var cancel = document.getElementsByClassName('cancel')[0];
    confirm.onclick = function() {
        document.deleteMatch.action="deleteMatch";
    }
    span.onclick = function() {
        document.form.action="deleteMatch";
    }
    cancel.onclick = function() {
        model.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == model) {
            model.style.display = "none";
        }
    }
}
