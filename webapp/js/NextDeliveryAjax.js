function bowlNextDelivery(firstBatsmanId, secondBatsmanId, bowlerId, overId ,matchId) {
    showDiv();
    var strikerId = document.getElementById('strikerId').value;
    var ballNumber = document.getElementById('ballNumber').value;
    httpRequest = new XMLHttpRequest();
    httpRequest.open('GET', 'bowlNextDelivery?firstBatsmanId='+ firstBatsmanId +'&&secondBatsmanId='+
            secondBatsmanId + '&&bowlerId=' + bowlerId + '&&overId=' + overId +
            '&&matchId=' + matchId + '&&strikerId=' + strikerId + '&&ballNumber=' + ballNumber);
    httpRequest.responseType = 'json';
    httpRequest.send();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
                var scoreSheet = httpRequest.response;
                var existingTotalRuns = document.getElementById('runs').innerHTML;
                var newTotalRuns = (existingTotalRuns * 1) + scoreSheet.runsOnBall;
                document.getElementById('runs').innerHTML = "" + newTotalRuns + "";
                document.getElementById('totalRuns').value = newTotalRuns;

                var batsmanId = document.getElementById("strikerId").value;
                var batsmanRuns = document.getElementById(""+batsmanId+"").value;
                var newBatsmanRuns = (batsmanRuns * 1) + scoreSheet.runsByBatsman;
                document.getElementById(""+batsmanId+"").value = "" + newBatsmanRuns + "";
                
                var firstBatsmanId = document.getElementById("firstBatsmanId").value;
                var firstBatsmanScore = document.getElementById(""+firstBatsmanId+"").value;
                document.getElementById("firstBatsman-score").innerHTML = "" + firstBatsmanScore;
                
                var secondBatsmanId = document.getElementById("secondBatsmanId").value;
                var secondBatsmanScore = document.getElementById(""+secondBatsmanId+"").value;
                document.getElementById("secondBatsman-score").innerHTML = "" + secondBatsmanScore;

                document.getElementById("strikerId").value = scoreSheet.strikerId;

                var content = document.getElementById('delivery-result');
                content.innerHTML= "" + scoreSheet.ballOutcome + "";

                var overDetail = document.getElementById("over-detail");
                var ballDetail = document.createElement("div");
                ballDetail.setAttribute("class", "ball-detail");
                ballDetail.innerHTML = "" + scoreSheet.ballOutcome + "";
                document.getElementById("over-detail").appendChild(ballDetail);
                
                var ballsBowled = document.getElementById("balls-bowled");
                ballsBowled.innerHTML= "" + scoreSheet.ballNumber + "";
                document.getElementById("ballNumber").value = scoreSheet.ballNumber ;

                if(6 == scoreSheet.ballNumber) {
                    document.getElementById("bowl").style.display = "none";
                    document.getElementById("chooseBowler").style.display = "block";
                } else {
                    document.getElementById("chooseBowler").style.display = "none";
                    document.getElementById("bowl").style.display = "block";
                }
                if("W" === scoreSheet.ballOutcome) {
                    document.getElementById("bowl").style.display = "none";
                    document.getElementById("chooseBatsman").style.display = "block";
                } else {
                    document.getElementById("bowl").style.display = "block";
                    document.getElementById("chooseBatsman").style.display = "none";
                }
            } else {
                console.log('Something went wrong..!!');
            }
        }
    }
}
function showDiv() {
	var model = document.getElementById('model');
	model.style.display = "block";
	setTimeout(function () {document.getElementById('model').style.display='none'}, 1000);
	window.onclick = function(event) {
        if (event.target == model) {
            model.style.display = "none";
        }
    }
}
function chooseBowler() {
    document.form.action="chooseBowler";
}
function chooseBatsman() {
    document.form.action="chooseBatsman";
}
