<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<html>
<head>
    <title>view_match</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/scoresheet.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
     <script src="/js/NextDeliveryAjax.js"></script>
</head>
<body>
  <div id="model">
     <div id="delivery-result"></div>
  </div>
  <div class="header"></div>
  <div class="container">
      <div id="bowler-stat">
      </div>
      <div id="ball-outcome">
        <input type="hidden" value="${overStartInfo.firstBatsman.id}" id="firstBatsmanId"/>
        <input type="hidden" value="${overStartInfo.secondBatsman.id}" id="secondBatsmanId"/>
        <input type="hidden" value="${overStartInfo.bowler.id}" id="bowlerId"/>
        <input type="hidden" value="${overStartInfo.battingTeamName}" id="battingTeamName"/>
        <input type="hidden" value="${overStartInfo.matchInfo.id}" id="matchId"/>
        <div id="bowl">
          <button onclick="bowlNextDelivery(${overStartInfo.firstBatsman.id},
            ${overStartInfo.secondBatsman.id}, ${overStartInfo.bowler.id}, ${overStartInfo.overNo},
            ${overStartInfo.matchInfo.id});">Bowl</button>
        </div>
      </div>
      <div id="batsman-stat">
      </div>
  </div>
  <div id="team-names">
      <div id="batting-team-name">
        <c:out value="${overStartInfo.battingTeamName}" />
      </div>
      <div id="bowling-team-name">
        <c:out value="${overStartInfo.bowlingTeamName}" />
      </div>
  </div>
  <div class="scorecard">
      <div class="score">
          <div id="runs">
          <c:out value="${overStartInfo.totalRuns}"/></div>
          <span id="over"><c:out value="${overStartInfo.overNo}"/></span>
          <span id="balls-bowled"><c:out value="${overStartInfo.ballNumber}"/></span>
      </div>
      <div class="blank">
      </div>
      <div class="batting-detail">
          <div id="firstBatsman">
              <span><c:out value="${overStartInfo.firstBatsman.playerName}" />
              </span>
          </div>
          <div id="firstBatsman-score">
            <c:out value="${overStartInfo.firstBatsmanRuns}" />
          </div>
          <div id="secondBatsman">
              <span><c:out value="${overStartInfo.secondBatsman.playerName}" />
              </span>
          </div>
          <div id="secondBatsman-score">
            <c:out value="${overStartInfo.secondBatsmanRuns}" />
          </div>
      </div>
      <div class="bowling-detail">
          <div id="bowler">
            <span><c:out value="${overStartInfo.bowler.playerName}" />
            </span>
            <div id="${overStartInfo.bowler.id}">
            </div>
          </div>
          <div id="over-detail">
          </div>
      </div>
  </div>
  <div>
    <form:form name="form" action="" method="post" modelAttribute="overStartInfo">
      <form:hidden path="firstBatsman.id" />
      <form:hidden path="firstBatsman.playerName" />
      <form:hidden path="secondBatsman.id" />
      <form:hidden path="secondBatsman.playerName" />
      <form:hidden path="bowler.id"/>
      <form:hidden path="bowler.playerName"/>
      <form:hidden path="battingTeamName"/>
      <form:hidden path="matchInfo.id"/>
      <form:hidden path="overNo"/>
      <form:input type="hidden" value="${overStartInfo.firstBatsmanRuns}" 
        path="firstBatsmanRuns" id="${overStartInfo.firstBatsman.id}"/>
      <form:input type="hidden" value="${overStartInfo.secondBatsmanRuns}" 
        path="secondBatsmanRuns" id="${overStartInfo.secondBatsman.id}"/>
      <form:input type="hidden" value="${overStartInfo.ballNumber}" path="ballNumber" id="ballNumber"/>
      <form:input type="hidden" value="${overStartInfo.strikerId}" path="strikerId" id="strikerId"/>
      <form:input type="hidden" value="${overStartInfo.totalRuns}" path="totalRuns"  id="totalRuns"/>
      <div id="chooseBowler">
      <input type="submit" onclick="chooseBowler()"  value="Choose Bowler" /></div>
      <div id="chooseBatsman">
      <input type="submit" onclick="chooseBatsman()" value="Choose Batsman" /></div>
    </form:form>
  </div>
</body>
</html>
