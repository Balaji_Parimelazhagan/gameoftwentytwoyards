<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<html>
<head>
    <title>view_teams</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/teamsfetch.css">
    <rel stylesheet='teamsfetch' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet"
     href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script type="text/javascript">
function limit_checkbox(name,obj,max) {
    var count=0;
    var x=document.getElementsByName(name);
    for (var i=0; i < x.length; i++) {
       if(x[i].checked) {
          count = count + 1;
       }
    }	
    if (count > max) {
        alert('Please select only ' + max + ' checkboxes.\
                To select this option unselect one of the others.');
	  obj.checked = false;
    }
}
</script>
</head>
<body>
  <div class="header">
    <button onclick="location.href='viewMatches';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
    </button>
    <form:form action="saveMatch" method="post" modelAttribute="matchInfo">
      <button type="submit" name="submit" value="save"><i class="fa fa-save"></i></button>
    <span>TEAMS</span>
  </div>
  <div class="container">
      <form:hidden  path="id"/>
      <form:hidden  path="matchName"/>
      <form:hidden  path="matchLocation"/>
      <input type="hidden" name="date" value="${matchInfo.matchDate}"/>
      <form:hidden  path="matchType"/>
    <c:forEach var="team" items="${teams}">
        <input type="checkbox" name="teamIds" value="${team.id}" id="${team.id}"
         onclick="limit_checkbox('teamIds', this, 2);" class="check"/>
        <label class="card" for="${team.id}">
        <b><c:out value="${team.id}"/></b><br>
        <b><c:out value="${team.teamName}"/></b><br>
        <c:out value="${team.country}"/><br>
        <c:out value="${team.teamStatus}"/><br>
        </label>
    </c:forEach>
    </form:form>
</body>
</html>
