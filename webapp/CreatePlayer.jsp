<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
  <head>
    <title>create_player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/playerform.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class = "header">
      <button onclick="location.href='viewPlayers';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
      </button>
    </div>
    <div class = "container">
      <form:form action="savePlayer" method="post" modelAttribute="playerInfo"
        enctype="multipart/form-data" >
      <fieldset class = "form">
      <legend><input type="file" name="file">
       <i class="fa fa-user-circle-o" style="font-size:80px;color:white;"></i></input>
      </legend>
      <table>
        <tr><td>Name:</td>
            <td><form:input type="text" path="playerName" pattern="[A-Za-z][A-Za-z \s]*" placeholder="example: Kaipulla" maxlength="25" required="required" /></td>
        </tr>
        <div class = "role">
          <tr><td>Role:</td>
          <td class = "roleOption">
          <form:select path ="playerRole" style="width:155px" required="required" >
            <option>none</option>
            <option>Batsman</option>
            <option>Bowler</option>
            <option>All-Rounder</option>
            <option>Wicket-Keeper</option>
          </form:select></td></tr>
        </div>
        <tr><td>Batting Style:</td><td>
          <form:select path="battingStyle" style="width:155px" required="required" >
            <option>none</option>
            <option>Right-Handed</option>
            <option>Left-Handed</option>
          </form:select></td></tr>
        <tr><td>Bowling Style:</td><td>
          <form:select path="bowlingStyle" style="width:155px" required="required" >
            <option>none</option>
            <option>Right-Arm</option>
            <option>Left-Arm</option>
          </form:select></td></tr>
        <tr><td>Pace Type:</td><td>
          <form:select path="paceType" style="width:155px" required="required" >
            <option>none</option>
            <option>Spin</option>
            <option>Medium-Fast</option>
            <option>Fast</option>
          </form:select></td></tr>
        <tr><td>Date Of Birth</td>
            <td><form:input type = "date" id = "start" path = "DOB" place-holder = "2018-07-22" 
                  min = "1910-01-01" max = "2004-01-01" required="required" /></td>
        </tr>
        <tr><td>Country:</td><td>  
          <form:select name="country" path = "countryName" style="width:155px" >
            <option>India</option>
            <option>Australia</option>
            <option>Afghanistan</option>
            <option>England</option>
            <option>Pakistan</option>
            <option>Bangladesh</option>
            <option>WestIndies</option>
            <option>SouthAfrica</option>
            <option>SriLanka</option>
            <option>NewZealand</option>
            <option>Zimbawe</option>
          </form:select>
        </td></tr>
        <tr><td>Address:</td><td><form:input type="text" 
          placeholder="No.10, dubai cross street, dubai main road."
          path="address" id="address"/></td></tr>
        <tr><td>City:</td><td><form:input type="text" path="city" 
         pattern="[A-Za-z][A-Za-z\s]*{25}" placeholder="example: dubai"/></td></tr>
        <tr><td>Contact Number:</td><td><form:input type="number" pattern="\\d{10}"
         path="contactNumber" maxlength="15" placeholder="example: 9566044044"/></td></tr>
        <tr><td colspan="2" align="center">
            <input type="submit" value="save"/></td></tr>
      </table>
      </fieldset>
      </form:form>
    </div>
  </body>
</html>
