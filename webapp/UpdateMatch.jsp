<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<html>
<head>
    <title>update_match</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/updatematch.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
  <div class="header">
    <button onclick="location.href='viewMatches';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
    </button>
    <span>MATCH</span>
  </div>
  <form:form action="updateMatch" method="post" modelAttribute="matchInfo">
    <button type="submit" ><i class="fa fa-save"></i></button>
    <div class= "matchAttribute">
      <form:hidden  path="id"/>
      <form:input type="text"  name="name" pattern="[A-Za-z][A-Za-z\s]*{25}"
        placeholder="Match Name" id="editableFeild" value="${matchInfo.matchName}"
        path="matchName" required="required"/>
      <form:input type="text" name="location" value="${matchInfo.matchLocation}"
        pattern="[A-Za-z][A-Za-z\s]*{25}" id="editableFeild" placeholder="Location"
        path="matchLocation" required="required"/>
      <input type = "text" name = "date" min = "2017-01-01" id="readOnlyFeild"
        value="${matchInfo.matchDate}" readonly="readonly"/>
       <form:select name="type" style="width:155px" id="editableFeild" path="matchType">
         <option>${matchInfo.matchType}</option>
         <option value="t20Match">T20</option>
         <option value="OneDayInternationalMatch">ODI</option>
         <option value="TestMatch">TEST</option>
       </form:select>
    </div>
    <div class="container">
      <c:forEach var="team" items="${matchInfo.teamsForMatch}">
         <input type="checkbox" name="teamIds" value="${team.id}" id="${team.id}" checked="checked"
          class="check"/>
         <label class="card" for="${team.id}">
          <b><c:out value="${team.id}"/></b><br>
          <c:out value="${team.teamName}"/><br>
          <c:out value="${team.country}"/><br>
          <c:out value="${team.teamStatus}"/><br>
         </label>
      </c:forEach>
      <c:forEach var="team" items="${teams}">
        <input type="checkbox" name="teamIds" value="${team.id}" id="${team.id}" class="check"/>
        <label class="card" for="${team.id}">
        <b><c:out value="${team.id}"/></b><br>
        <c:out value="${team.teamName}"/><br>
        <c:out value="${team.country}"/><br>
        <c:out value="${team.teamStatus}"/><br>
        </label>
      </c:forEach>
    </div>
  </form:form>
</body>
</html>
