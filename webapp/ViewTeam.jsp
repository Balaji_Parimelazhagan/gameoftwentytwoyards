<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>team</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/viewteam.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
</head>
<body>
  <div class="header">
    <button onclick="location.href='viewTeams';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
    </button>
    <button onclick="location.href='UpdateTeam.jsp';" class="editTeam" ><i class="fa fa-edit"></i></button>
    <div id="teamId">
      <c:out value="${teamInfo.id}"/>
    </div>
    <span>
      <c:out value="${teamInfo.teamName}"/>
    </span>
  </div>
  <div class="teamAttribute">
    <i><c:out value="${teamInfo.country}"/></i>
  </div>
  <div class="container">
    <c:forEach var="player" items="${teamInfo.playersInTeam}">
      <div class= "card">
        <b><c:out value="${player.playerName}"/></b><br>
        <c:out value="${player.playerRole}"/><br>
        <c:out value="${player.battingStyle}"/><br>
        <c:out value="${player.bowlingStyle}"/><br>
        <c:out value="${player.paceType}"/><br>
        <c:out value="${player.countryName}"/><br>
        <c:out value="${player.playerAge}"/><br>              
      </div>
    </c:forEach>
  </div>
</body>
</html>
