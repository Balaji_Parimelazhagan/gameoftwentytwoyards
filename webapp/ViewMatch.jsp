<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>view_match</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/viewmatch.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
</head>
<body>
  <div class="header">
    <button onclick="location.href='viewMatches';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
    </button>
    </button>
    <div id="teamId">
      <c:out value="${match.id}"/>
    </div>
    <span>
      <c:out value="${match.matchName}"/>
    </span>
  </div>
  <div class="container">
    <c:forEach var="team" items="${match.teamsForMatch}">
      <div class = "teamCard">
        <b><c:out value="${team.id}"/><br></b>
        <c:out value="${team.teamName}"/>
        <div class = "playerCard">
          <c:forEach var="player" items="${team.playersInTeam}">
            <div class="player">
              <b><c:out value="${player.playerName}"/></b>&nbsp;&nbsp;
              <c:out value="${player.playerRole}"/><br>
            </div>
          </c:forEach> 
        </div>
      </div>
    </c:forEach>
  </div>
</body>
</html>
