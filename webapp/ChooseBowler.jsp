<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<html>
<head>
    <title>select_players</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/chooseBowler.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
</head>
<body>
  <div class="header">
    <button onclick="location.href='viewMatches';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
    </button>
    </button>
    <div id="teamId"> <c:out value="${overStartInfo.bowlingTeam.id}"/><br>
    </div>
    <span>
    </span>
  </div>
  <form:form action="startNextOver" method="post" modelAttribute="overStartInfo">
    <div class="container">
        <div class = "teamCard">
        <form:hidden path="bowler.id"/>
        <form:hidden path="bowler.playerName"/>
        <form:hidden path="bowlingTeam.id"/>
        <form:hidden path="bowlingTeam.teamName"/>
        <form:hidden path="firstBatsman.id"/>
        <form:hidden path="firstBatsman.playerName" />
        <form:hidden path="secondBatsman.id"/>
        <form:hidden path="secondBatsman.playerName" />
        <form:hidden path="matchInfo.id"/>
        <form:hidden path="battingTeamName"/>
        <form:hidden path="totalRuns"/>
        <form:hidden path="firstBatsmanRuns"/>
        <form:hidden path="secondBatsmanRuns"/>
        <form:hidden path="strikerId"/>
          <c:out value="${overStartInfo.bowlingTeam.teamName}"/>
          <div class = "playerCard">
            <c:forEach var="player" items="${overStartInfo.bowlingTeam.playersInTeam}">
                <input type="checkbox" name="bowlerId" value="${player.id}" id="${player.id}"
                  class="check"/>
                <label class="player" for="${player.id}">
                  <b><c:out value="${player.playerName}"/></b>&nbsp;&nbsp;
                  <c:out value="${player.playerRole}"/><br>
                </label>
            </c:forEach> 
          </div>
        </div>
    </div>
    <button type="submit" id= "start">
  </form:form>
</body>
</html>
