<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <title>player</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/viewplayer.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
     integrity='sha384lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
  </head>
  <body>
  <div class="header">
  <button onclick="location.href='viewPlayers';" id="backButton">
    <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
  </button>
  </div>
  <div class="container">
    <div class="inner_container">
      <table class="tableview">
         <caption><h2><c:out value="${playerInfo.id}"/></h2></caption>
         <tr><td><img src="${playerInfo.imagePath}" id="profile-pic"></td></tr>
         <tr><td>Name</td><td><c:out value="${playerInfo.playerName}" /></td></tr>
         <tr><td>Role</td><td><c:out value="${playerInfo.playerRole}" /></td></tr>
         <tr><td>Batting-Style</td><td><c:out value="${playerInfo.battingStyle}" /></td></tr>
         <tr><td>Bowling-Style</td><td><c:out value="${playerInfo.bowlingStyle}" /></td></tr>
         <tr><td>Pace-Type</td><td><c:out value="${playerInfo.paceType}" /></td></tr>
         <tr><td>Country</td><td><c:out value="${playerInfo.countryName}" /></td></tr>
         <tr><td>Age</td><td><c:out value="${playerInfo.playerAge}" /></td></tr>
         <tr><td>Address</td><td><c:out value="${playerInfo.address}"/></td></tr>
         <tr><td>City</td><td><c:out value="${playerInfo.city}"/></td></tr>
         <tr><td>Contact-Number</td><td><c:out value="${playerInfo.contactNumber}"/></td></tr>
       </table>
     </div>
   </div> 
 </body>
<html>
  
