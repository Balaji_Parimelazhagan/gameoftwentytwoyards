<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
  <head>
    <title>game_of_22_yards</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/indexstyle.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class = "header">
    </div>
    <div class = "container">
    <form action="login" method="post">
      <fieldset class = "form">
      <legend><i class="fa fa-user-circle-o"
        style="font-size:80px;color:white;"></i></legend>
        <div>${message}</div>
        <input type="email" name="userEmailId" id="email-id" placeholder="Email-Id"/>
        <input type="password" id="pass" name="password" placeholder="password"
           minlength="8" />
        <button name ="submit" value="login" type="submit">Login</button>
     </fieldset>
    </form>
    <button onclick="location.href='signUp';"/>SignUp</button>
     </div>
     <div class = "footer">
    </div>
  </body>
</html>

