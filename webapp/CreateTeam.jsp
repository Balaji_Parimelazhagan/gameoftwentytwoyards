<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  

<html>
  <head>
    <title>create_team</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="/css/createteam.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
    <div class = "header">
      <button onclick="location.href='viewTeams';" id="backButton">
        <i class="fa fa-arrow-left" style="font-size:30px;color:white;margin:10px;"></i>
      </button>
    </div>
    <div class = "container">      
      <form:form action="createTeam" method="post" modelAttribute="teamInfo" >
        <fieldset class = "form">
          <legend><i class="fa fa-user-circle-o" style="font-size:80px;color:white;"></i></legend>
          <table>
            <tr><td>Name:</td>
              <td><form:input type="text" title="Please enter name with length within 25 characters"
                name="name" pattern="[A-Za-z][A-Za-z\s]*{25}"
                placeholder="example: Chennai Super Kings" path="teamName" required="required" /></td>
            </tr>
            <tr><td>Country:</td><td>  
              <form:select name="country" style="width:155px" path="country" >
                <option>India</option>
                <option>Australia</option>
                <option>Afghanistan</option>
                <option>England</option>
                <option>Pakistan</option>
                <option>Bangladesh</option>
                <option>WestIndies</option>
                <option>SouthAfrica</option>
                <option>SriLanka</option>
                <option>NewZealand</option>
                <option>Zimbawe</option>
              </form:select>
            </td></tr>
            <tr><td colspan="2" align="center"><input type="submit" /></td></tr>
          </table>
        </fieldset>
      </form:form>
    </div>
  </body>
</html>
