package com.ideas2it.gameoftwentytwoyards.configuration;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.ServletRequest;

import com.ideas2it.gameoftwentytwoyards.common.Constants;

import org.apache.log4j.Logger;

public class SessionFilter implements Filter{

    static final Logger logger = Logger.getLogger(SessionFilter.class);

    public void init(FilterConfig arg0) {
    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) {
        try {
     
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession(false);
            boolean isLoggedIn = (null != session 
                                    && null != session.getAttribute(Constants.USER_EMAIL_ID));
            String loginURI = httpRequest.getContextPath() + "/login";
            boolean isLoginRequest = httpRequest.getRequestURI().equals(loginURI);
            String signUpURI = httpRequest.getContextPath() + "/signUp";
            boolean isSignUpRequest = httpRequest.getRequestURI().equals(signUpURI);
            String saveUserURI = httpRequest.getContextPath() + "/saveUser";
            boolean isSaveUserRequest = httpRequest.getRequestURI().equals(saveUserURI);
            if (isLoggedIn || isLoginRequest || isSignUpRequest || isSaveUserRequest) {
                chain.doFilter(request, response);
            } else {
                RequestDispatcher dispatcher =
                        httpRequest.getRequestDispatcher("index.jsp");
                dispatcher.forward(request, response);
            }
        } catch (Throwable e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public void destroy() {
    
    }
}  
