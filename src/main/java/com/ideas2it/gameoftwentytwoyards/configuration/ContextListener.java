package com.ideas2it.gameoftwentytwoyards.configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
 
import org.apache.log4j.PropertyConfigurator;

/**
 * Initiates the logger in the absence of Main class and contains the path of
 * the properties file. 
 */
public class ContextListener implements ServletContextListener {
 
    /**
     * Initialize log4j when the application is being started.
     *
     * @param event of ServletContextEvent, it is the event that initiates the
     * context.
     */
    public void contextInitialized(ServletContextEvent event) {
        PropertyConfigurator.configure("log4j.properties");
    }

    /**
     * Destroys the Listener when the event get finished.
     *
     * @param event of ServletContextEvent, it is the event that destroys the
     * context.
     */
    public void contextDestroyed(ServletContextEvent event) {
    }  
}
