package com.ideas2it.gameoftwentytwoyards.service;

import java.io.IOException;

import java.util.List;

import javax.servlet.http.Part;

import org.json.JSONArray;
import org.json.JSONObject;

import org.springframework.web.multipart.commons.CommonsMultipartFile; 

import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.exception.PlayerNotFoundException;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.info.PlayerInfo;
import com.ideas2it.gameoftwentytwoyards.info.PlayerPaginationInfo;

/**
 * Performs the create, update calculations and delete operations on a player
 * and also prevents the access of dao layer and pojo class.
 */
public interface PlayerService {

    /**
     * Collects the Player information from user and stores stores as an object
     * and stores it in Database.
     *
     * @param Details of the player.
     * @return Player of player, object that contains the details the of the
     * player.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    PlayerInfo createPlayer(PlayerInfo playerInfo, CommonsMultipartFile file)
            throws GameOfTwentyTwoYardsException, IOException;

    /**
     * Fetches the player from Database.
     *
     * @param playerId of the int.
     * @return player of Player, it is the object for corresponding id.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    PlayerInfo fetchPlayer(int playerId)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the player from Database.
     *
     * @param playerId of the int.
     * @return player of Player, it is the object for corresponding id.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Player fetchPlayerWithTeam(int id)
            throws GameOfTwentyTwoYardsException;

    /**
     * Hard deletes a specific player details.
     *
     * @param playerId of String, id of player to be deleted.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    void deletePlayer(int playerId) throws GameOfTwentyTwoYardsException;

    /**
     * Recieves the Profic pic and updates the player with the profile pic.
     *
     * @param player Of Player, it is the object in which the profile pic
     * should be updated.
     * @param fileName of String, name of the file which should be saved and
     * updated.
     * @param existingImagePath of String, path of the existing image.
     * @return player Of Player, it is the updated object.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    PlayerInfo updatePlayerWithPic(Player player, CommonsMultipartFile file
            , String existingImagePath)
            throws GameOfTwentyTwoYardsException, IOException;

    /**
     * Recieves the Profic pic and updates the player with the profile pic.
     *
     * @param player Of Player, it is the object in which the profile pic
     * should be updated.
     * @param fileName of String, name of the file which should be saved and
     * updated.
     * @param existingImagePath of String, path of the existing image.
     * @return player Of Player, it is the updated object.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    public PlayerInfo updatePlayerWithPic(PlayerInfo playerInfo,
            CommonsMultipartFile file, String existingImagePath)
            throws GameOfTwentyTwoYardsException, IOException;

    /**
     * Updates the data on database.
     *
     * @param player of Player, object where the details are present to be
     * updated.
     * @return playerDAO.updatePlayer(player), contains the updated
     * object which is to displayed.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Player upatePlayerToStorage(Player player) 
            throws GameOfTwentyTwoYardsException, IOException;

    /**
     * Gives the number of rows in the table.
     * 
     * @return playerDAO.retrievePlayersCount(), method calculates the number
     * of rows in the table.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    int fetchPlayersCount() throws GameOfTwentyTwoYardsException;

    /**
     * Checks whether database is empty or not.
     * 
     * @return playerDAO.checkEmpty(), method checks the database is empty.
     * true if the database is empty and false if it contains some data.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    List<Player> retrievePlayerByLimit(int viewAllOffset)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the Players By country.
     * 
     * @return viewPlayersByCountry, is the method that gives players in an
     * arrayList which contains 5 teams.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    List<Player> fetchPlayersByCountry(String countryAsString)
            throws GameOfTwentyTwoYardsException;

    /**
     * Adds the player to the team, which is done by updating the team_id
     * column to corressponding team's id.
     * 
     * @param teamId of int, is the team id that should be updated.
     * @param playersId of String, it is ids of the players where the
     * team id should be updated.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    List<Player> fetchPlayersToBeAdded(List<Integer> playerIds)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the total players by role.
     *
     * @param teamID of int, is the team id which should be updated.
     * @param PlayerRole of String, Has the playerRole based on which the
     * players are filtered
     * @return fetchPlayersByRole of String, gives the total players in a
     * specific
     * role.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    int fetchPlayersCountByRole(int teamId,
            String PlayerRole) throws GameOfTwentyTwoYardsException;

    /**
     * Paginates and fetches the players and converts them into a JSON array.
     *
     * @param page of int, page number for which to the playrs to be fetched.
     * @return array of JSONArray, List of Players in Json Array Format.
     */
    JSONArray paginateAndFetchAsJson(int page)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the players by limit of 5.
     *
     * @param curerentPage of int, it is the current page number.
     * @return players of List<Player> List of Players.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    List<Player> paginateAndFetch(int page)
            throws GameOfTwentyTwoYardsException;

    /**
     * Calculates the total number of pages and makes them into a list. It also
     * fetches the initial 5 players.
     *
     * @return playerInfo of PlayerInfo, object that contains pages and initial
     * 5 players.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    PlayerPaginationInfo fetchPagesWithPlayers() throws GameOfTwentyTwoYardsException;
}
