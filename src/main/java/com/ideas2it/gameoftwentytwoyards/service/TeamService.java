package com.ideas2it.gameoftwentytwoyards.service;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.info.TeamInfo;
import com.ideas2it.gameoftwentytwoyards.info.TeamPaginationInfo;

/**
 * Performs the create, update calculations and delete operations on a player
 * and also prevents the access of dao layer and pojo class.
 */
public interface TeamService {

    /**
     * Collects the Team information from user and stores stores as an object
     * and stores it in Database. It also performs the retrieval of players
     * with the teams country.
     *
     * @param team of Team, it is the object team which has to be created and
     * stored in the database.
     * @return teamInfo of TeamInfo, object that contains the details the of the
     * team and players of the same country as the team.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    TeamPaginationInfo createTeam(TeamInfo teamInfo)
            throws GameOfTwentyTwoYardsException;

    /**
     * Validates the team combination and sets the status Whether complete or
     * incomplete and also calls a method to insert the combination into the
     * database.
     *
     * @param team of Team, it is the object where all the details are
     * present.
     * @param teamId of int, contains the team's Id for the addition of players.
     * @return Team of team, object that contains the details the of the
     * team.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Team validateTeamCombination(int teamId, Team team)
            throws GameOfTwentyTwoYardsException;

    /**
     * Views all the teams.
     *
     * @param offsetForView of int, is the offset which is used to display five
     * teams at a time
     * @return viewAllTeams() method that gives the teams details in an
     * arraylist.
     */
    Set<Team> fetchAllTeams() throws GameOfTwentyTwoYardsException;

    /**
     * Views all the players of same country.
     *
     * @param offsetForView of int, is the offset which is used to display five
     * players at a time
     * @param country of String, is the name of the country based on
     * which the players are filtered.
     * @return teamInfo of TeamInfo, object that contains the details the of the
     * team and players of the same country as the team.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    TeamPaginationInfo fetchTeamAndPlayersByCountry(int id)
             throws GameOfTwentyTwoYardsException;

    /**
     * Upadtes the team details in the database.
     * 
     * @param playerIds of String, Player id to be added in the team.
     * @param team of Team, it is the object of the team in which the players
     * to be added.
     * @return updateTeam() method that updates the team details and gives
     * the object
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    TeamInfo updateTeamWithPlayers(String[] playerIds, TeamInfo teamInfo)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the team object and Players that to be added to the team.
     * 
     * @param playerIds of String[], it is the array of player ids for the
     * addition in the team. 
     * @return players of Set<Player>, players in a set.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Set<Player> fetchPlayersToBeAdded(String[] playerIds)
            throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the team details from the database.
     * 
     * @param teamId of int, is the Id of the team which should be retrieved.
     * @return retrieveTeam() method that fetches the team details and gives
     * the object
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    TeamInfo retrieveTeam(int teamId) throws GameOfTwentyTwoYardsException;

    /**
     * Deletes the team from the database.
     * 
     * @param teamId of int, is the Id of the team which should be deleted.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    void deleteTeam(int teamId) throws GameOfTwentyTwoYardsException;

    /**
     * Adds Players to the team.
     * 
     * @param teamId of int, is the Id of the team which should be deleted.
     */
    void addPlayersToTeam(Set<Player> PlayersToTeam, Team team);

    /**
     * Checks wether the teams table is empty and returns correspondingly.
     *
     * @param teamIds of String, Team ids for which the team object should be
     * fetched.
     * @return Teams, List of Teams for Match.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Set<Team> fetchTeamsForMatch(List<Integer> teamIds)
             throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the teams by limit.
     * 
     * @return List of teams of 5 in number.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    List<Team> retrieveTeamsByLimit(int Offset)
            throws GameOfTwentyTwoYardsException;

    /**
     * Paginates and fetches the teams and converts them into a JSON array.
     *
     * @param page of int, page number for which to the playrs to be fetched.
     * @return array of JSONArray, List of Players in Json Array Format.
     */
    JSONArray paginateAndFetchAsJson(int page)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the players by limit of 5.
     *
     * @param curerentPage of int, it is the current page number.
     * @return players of List<Player> List of Players.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    List<Team> paginateAndFetch(int page)
            throws GameOfTwentyTwoYardsException;

    /**
     * Calculates the total number of pages and Fetches them. It also fetches
     * the first five teams from the database.
     *
     * @return teamInfo of TeamInfo, object that contains the details the of the
     * team and players of the same country as the team.
     * @throws GameOfTwentyTwoYardsException, user defined exception. 
     */
    TeamPaginationInfo fetchPagesWithTeams() throws GameOfTwentyTwoYardsException;
}
