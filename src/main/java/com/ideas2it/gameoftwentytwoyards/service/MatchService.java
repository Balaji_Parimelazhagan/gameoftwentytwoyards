package com.ideas2it.gameoftwentytwoyards.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ideas2it.gameoftwentytwoyards.common.MatchType;
import com.ideas2it.gameoftwentytwoyards.entities.Match;
import com.ideas2it.gameoftwentytwoyards.info.MatchInfo;
import com.ideas2it.gameoftwentytwoyards.info.MatchPaginationInfo;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.exception.MatchNotFoundException;

public interface MatchService {

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param matchInfo of the MatchInfo, Info Object to insert the match object
     * to the database.
     * @param date of String, Date of the match which has to be set into
     * match object.
     * @return matchPaginationInfo of MatchPaginationInfo, consist of match and
     * teams without date collisions.
     */
    MatchPaginationInfo createMatch(MatchInfo matchInfo, String date)
            throws GameOfTwentyTwoYardsException;

    /**
     * Upadtes the match details in the database.
     * 
     * @param teamIds of String[]. team ids that has been select by the user for
     * the match and it has to be inserted into the object.
     * @param matchInfo of the MatchInfo, Info Object to insert the match object
     * to the database.
     * @param date of String, Date of the match which has to be set into
     * match object.
     * @return matchInfo MatchInfo, it is the object that contains the details
     * of the match. 
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    MatchInfo updateMatch(String[] teamIds, MatchInfo matchInfo, String date)
            throws GameOfTwentyTwoYardsException;

    /**
     * Filter the Teams For the Match by Date. This avoids the match collisions
     * for a team on a single date.
     *
     * @param id of int, Id of match for which the teams should be obtained.
     * @return matchPaginationInfo of MatchPaginationInfo, consist of match and
     * teams without date collisions.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    MatchPaginationInfo filterTeamsByDate(int id)
             throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the match from the database.
     *
     * @param matchId of int, Id of the match which should be retrieved.
     * @return match Match, it is the object that contains the details of the
     * match.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    MatchInfo retrieveMatch(int matchId)
             throws GameOfTwentyTwoYardsException;

    /**
     * Adds the teams to the Match.
     * 
     * @param teamIds of String[], team Ids that to be added to match.
     * @param matchId of int, Match id to which the teams to be added.
     * @return match Match, it is the object that contains the details of the
     * match.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Match addTeamsForMatch(String[] teamIds, int matchId)
            throws GameOfTwentyTwoYardsException ;

    /**
     * Hard deletes a specific player details.
     *
     * @param matchId of int, id of player to be deleted.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    void deleteMatch(int matchId) throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the teams for the match, teamIds are splitted and collected into
     * objects.
     *
     * @param teamIds of String, ids of teams that to be added.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Set<Team> fetchTeamsForMatch(String[] teamIds)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the matches by limit.
     * 
     * @return List of matches of 5 in number.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    List<Match> retrieveMatchesByLimit(int Offset)
            throws GameOfTwentyTwoYardsException;

    /**
     * Paginates and fetches the matches and converts them into a JSON array.
     *
     * @param page of int, page number for which to the playrs to be fetched.
     * @return array of JSONArray, List of Players in Json Array Format.
     */
    JSONArray paginateAndFetchAsJson(int page)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the players by limit of 5.
     *
     * @param curerentPage of int, it is the current page number.
     * @return players of List<Player> List of Players.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    List<Match> paginateAndFetch(int page)
            throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the players by limit of 5.
     *
     * @param curerentPage of int, it is the current page number.
     * @return players of List<Player> List of Players.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    MatchPaginationInfo fetchTeamsForSelection(int matchId)
            throws GameOfTwentyTwoYardsException;

    /**
     * Calculates the total number of pages and Fetches them. It also fetches
     * the first five matches from the database.
     *
     * @return matchPaginationInfo of MatchPaginationInfo, Object consists of
     * matches and pages in it.
     * @throws GameOfTwentyTwoYardsException, user defined exception. 
     */
    MatchPaginationInfo fetchPagesWithMatches() throws GameOfTwentyTwoYardsException;
}
