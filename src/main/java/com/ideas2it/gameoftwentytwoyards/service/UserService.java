package com.ideas2it.gameoftwentytwoyards.service;

import com.ideas2it.gameoftwentytwoyards.common.Gender;
import com.ideas2it.gameoftwentytwoyards.common.UserRole;
import com.ideas2it.gameoftwentytwoyards.entities.User;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.info.UserInfo;

/**
 * Performs the create, update calculations and delete operations on a user
 * and also prevents the access of dao layer and pojo class.
 */
public interface UserService {

    /**
     * Collects the User information from user and stores stores as an object
     * and stores it in Database.
     *
     * @param Details of the user.
     * @return userId of int.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    void createUser(UserInfo userInfo)
            throws GameOfTwentyTwoYardsException;

    /**
     * Gets the User for the desired emailID, and checks with the corresponding
     * password, if the email is invalid returns false, if the password is
     * incorrect returns false.
     *
     * @param emailid of String, emailid of the user.
     * @return password of String.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    public boolean validateUser(String userEmailId, String rawPassword)
            throws GameOfTwentyTwoYardsException;

    /**
     * Checks the Email id exist or not.
     *
     * @param userEmailId of String, Email id to check user valid.
     * @return true if emailid exist, false if emailid doesnt exist.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    boolean isUserPresent(String userEmailId)
            throws GameOfTwentyTwoYardsException;

    /**
     * Checks the Login Failed is exceeded more than 3 times.
     *
     * @param user of User, user object where the login fails is to calculated.
     * @return true if Login fails exceeded, false if Login Fails is Fine.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    void isLoginFailsExceeded(User user)
            throws GameOfTwentyTwoYardsException;
}
