package com.ideas2it.gameoftwentytwoyards.service.impl;

import java.lang.NullPointerException;
import java.lang.StringBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.common.Gender;
import com.ideas2it.gameoftwentytwoyards.common.UserRole;
import com.ideas2it.gameoftwentytwoyards.dao.UserDAO;
import com.ideas2it.gameoftwentytwoyards.entities.User;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.exception.InvalidPasswordException;
import com.ideas2it.gameoftwentytwoyards.exception.EmailAlreadyExistException;
import com.ideas2it.gameoftwentytwoyards.exception.LoginFailsExceededException;
import com.ideas2it.gameoftwentytwoyards.exception.UserNotFoundException;
import com.ideas2it.gameoftwentytwoyards.info.UserInfo;
import com.ideas2it.gameoftwentytwoyards.service.UserService;
import com.ideas2it.gameoftwentytwoyards.utils.PasswordEncryptionUtil;

/**
 * Performs the create, update calculations and delete operations on a user
 * and also prevents the access of dao layer and pojo class.
 */
@Service
public class UserServiceImpl implements UserService {
    private UserDAO userDAO;

    /**
     * Constructor initialises the objects that is used throughout the user
     * service layer. 
     */
    @Autowired
    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public void createUser(UserInfo userInfo)
            throws GameOfTwentyTwoYardsException {
        String encryptedPassword = PasswordEncryptionUtil.encryptPassword(
                                      userInfo.getPassword());
        userInfo.setPassword(encryptedPassword);
        userInfo.setUserStatus(Boolean.TRUE);
        User user = convertUserInfoToUser(userInfo);
        int id = userDAO.createUser(user);
        if (0 == id) {
            throw new EmailAlreadyExistException("Email already exist!!!");
        }
        
    }

    @Override
    public boolean validateUser(String userEmailId, String rawPassword)
            throws GameOfTwentyTwoYardsException {
        String password = PasswordEncryptionUtil.encryptPassword(rawPassword);
        boolean isUserValid = Boolean.FALSE;
        if (isUserPresent(userEmailId)) {
            User user = userDAO.getUserForValidate(userEmailId);
            String actualPassword = user.getPassword();
            if (actualPassword.equals(password) && user.getUserStatus()) {
                isUserValid  = Boolean.TRUE;
            } else {
                isUserValid  = Boolean.FALSE;
                isLoginFailsExceeded(user);
            }
        } else {
            isUserValid = Boolean.FALSE;
        }
        return isUserValid;
    }

    @Override
    public boolean isUserPresent(String userEmailId)
            throws GameOfTwentyTwoYardsException {
        boolean isEmailIdExist = Boolean.FALSE;
        if (null == userDAO.getUserForValidate(userEmailId)) {
            isEmailIdExist = Boolean.FALSE;
            throw new UserNotFoundException("Invalid EmailId !!!");
        } else {
            isEmailIdExist = Boolean.TRUE;
        }
        return isEmailIdExist;
    }

    @Override
    public void isLoginFailsExceeded(User user)
            throws GameOfTwentyTwoYardsException {
        int loginFails = user.getLoginFailAttempts();
        loginFails++;
        if (3 >= loginFails) {
            user.setLoginFailAttempts(loginFails);
            userDAO.updateUser(user);
            throw new InvalidPasswordException("Incorrect Password !!!");
        } else {
            user.setLoginFailAttempts(loginFails);
            user.setUserStatus(Boolean.FALSE);
            userDAO.updateUser(user);
            throw new LoginFailsExceededException
                    ("You are currently restricted!!!");
        }
    }

    private User convertUserInfoToUser(UserInfo userInfo) {
        User user = new User();
        user.setId(user.getId());
        user.setUserStatus(userInfo.getUserStatus());
        user.setLoginFailAttempts(userInfo.getLoginFailAttempts());
        if (null != userInfo.getUserGender()) {
            user.setUserGender(userInfo.getUserGender());
        }
        if (null != userInfo.getUserRole()) {
            user.setUserRole(userInfo.getUserRole());
        }
        if (null != userInfo.getUserName()) {
            user.setUserName(userInfo.getUserName());
        }
        if (null != userInfo.getUserEmailId()) {
            user.setUserEmailId(userInfo.getUserEmailId());
        }
        if (null != userInfo.getPassword()) {
            user.setPassword(userInfo.getPassword());
        }
        return user;
    }   

    private UserInfo convertUserToUserInfo(User user) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(user.getId());
        userInfo.setUserStatus(user.getUserStatus());
        userInfo.setLoginFailAttempts(user.getLoginFailAttempts());
        if (null != user.getUserGender()) {
            userInfo.setUserGender(user.getUserGender());
        }
        if (null != user.getUserRole()) {
            userInfo.setUserRole(user.getUserRole());
        }
        if (null != user.getUserName()) {
            userInfo.setUserName(user.getUserName());
        }
        if (null != user.getUserEmailId()) {
            userInfo.setUserEmailId(user.getUserEmailId());
        }
        if (null != user.getPassword()) {
            userInfo.setPassword(user.getPassword());
        }
        return userInfo;
    }             
}
