package com.ideas2it.gameoftwentytwoyards.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.dao.OverDAO;
import com.ideas2it.gameoftwentytwoyards.entities.Match;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.entities.Over;
import com.ideas2it.gameoftwentytwoyards.entities.OverDetail;
import com.ideas2it.gameoftwentytwoyards.info.OverStartInfo;
import com.ideas2it.gameoftwentytwoyards.info.PlayerInfo;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.service.MatchService;
import com.ideas2it.gameoftwentytwoyards.service.OverService;
import com.ideas2it.gameoftwentytwoyards.service.PlayerService;
import com.ideas2it.gameoftwentytwoyards.service.OverDetailService;
import com.ideas2it.gameoftwentytwoyards.utils.CommonUtil;

@Service
public class OverDetailServiceImpl implements OverDetailService {
    private OverService overService;
    private PlayerService playerService;
    private MatchService matchService;

    /**
     * Constructor initialises the objects that is used throughout the match
     * service layer. 
     */
    @Autowired
    public OverDetailServiceImpl(OverService overService,
            PlayerService playerService, MatchService matchService) {
        this.overService = overService;
        this.playerService = playerService;
        this.matchService = matchService;
    }

    @Override
    public JSONObject generateBallAndSaveDetail(int firstBatsmanId,
            int secondBatsmanId, int bowlerId, int overId, int strikerId,
            int matchId, int ballNumber)
            throws GameOfTwentyTwoYardsException {
        int batsmanId = 0;
        if (0 != strikerId) {
            batsmanId = strikerId;
        } else {
            strikerId = firstBatsmanId;
            batsmanId = firstBatsmanId;
        }
        Player batsman = CommonUtil.convertPlayerInfoToPlayer(playerService.fetchPlayer(batsmanId));
        Player bowler = CommonUtil.convertPlayerInfoToPlayer(playerService.fetchPlayer(bowlerId));
        Match match = CommonUtil.convertMatchInfoToMatch(matchService.retrieveMatch(matchId));
        Over over = overService.fetchOverById(overId);
        OverDetail overDetail = new OverDetail();
        overDetail.setBatsman(batsman);
        overDetail.setBowler(bowler);
        overDetail.setMatchId(matchId);
        overDetail.setOver(over);
        return fetchBallOutcome(overDetail, overId, strikerId, firstBatsmanId,
                secondBatsmanId, ballNumber);
    }

    private JSONObject fetchBallOutcome(OverDetail overDetail, int overId,
            int strikerId, int firstBatsmanId, int secondBatsmanId, int ballNumber)
            throws GameOfTwentyTwoYardsException {
        int randomNumber = (int)(Math.random() * 25 + 1);
        String ballOutcome = CommonUtil.getBallOutcome(randomNumber);
        boolean isStrikeChange = Boolean.FALSE;
        int runsOnBall = 0;
        int runsByBatsman = 0;
        switch (ballOutcome) {
            case "wd":
                overDetail.setIsWide(Boolean.TRUE);
                overDetail.setBallNumber(ballNumber);
                runsOnBall++;
                break;
            case "nb":
                overDetail.setIsNoBall(Boolean.TRUE);
                overDetail.setBallNumber(ballNumber);
                runsOnBall++;
                break;
            case "B":
                overDetail.setIsByes(Boolean.TRUE);
                overDetail.setBallNumber(ballNumber);
                ballNumber++;
                runsOnBall++;
                break;
            case "lb":
                overDetail.setIsLegByes(Boolean.TRUE);
                overDetail.setBallNumber(ballNumber);
                ballNumber++;
                runsOnBall++;
                break;
            case "W":
                overDetail.setIsWicket(Boolean.TRUE);
                overDetail.setBallNumber(ballNumber);
                ballNumber++;
                break;
            default:
                runsOnBall = Integer.parseInt(ballOutcome);
                overDetail.setBallNumber(ballNumber);
                ballNumber++;
                if ((1 == runsOnBall) || (3 == runsOnBall) || (5 == runsOnBall)) {
                    isStrikeChange = Boolean.TRUE;
                    if (strikerId == firstBatsmanId) {
                        strikerId = secondBatsmanId;
                    } else {
                        strikerId = firstBatsmanId;
                    }
                }
                runsByBatsman = runsOnBall;
                break;
        }
        overDetail.setRuns(runsOnBall);
        overService.updateOverWithOverDetail(overDetail, overId);
        return convertDetailsIntoJSON(ballOutcome, runsByBatsman, runsOnBall, strikerId, ballNumber);
    }

    private JSONObject convertDetailsIntoJSON(String ballOutcome,
            int runsByBatsman, int runsOnBall, int strikerId, int ballNumber)
            throws GameOfTwentyTwoYardsException {
        JSONObject scoreSheetJSON = new JSONObject();
        scoreSheetJSON.put("ballOutcome", ballOutcome);
        scoreSheetJSON.put("runsByBatsman", runsByBatsman);
        scoreSheetJSON.put("runsOnBall", runsOnBall);
        scoreSheetJSON.put("strikerId", strikerId);
        scoreSheetJSON.put("ballNumber", ballNumber);
        return scoreSheetJSON;
    }

}
