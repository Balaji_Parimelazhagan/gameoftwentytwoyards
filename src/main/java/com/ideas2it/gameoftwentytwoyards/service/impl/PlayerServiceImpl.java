package com.ideas2it.gameoftwentytwoyards.service.impl;

import java.io.BufferedOutputStream;  
import java.io.File;
import java.io.FileOutputStream;  
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile; 

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.dao.PlayerDAO;
import com.ideas2it.gameoftwentytwoyards.entities.Contact;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.exception.PlayerNotFoundException;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.info.PlayerInfo;
import com.ideas2it.gameoftwentytwoyards.info.PlayerPaginationInfo;
import com.ideas2it.gameoftwentytwoyards.service.PlayerService;
import com.ideas2it.gameoftwentytwoyards.utils.AgeUtil;
import com.ideas2it.gameoftwentytwoyards.utils.CommonUtil;

/**
 * Performs the create, update calculations and delete operations on a player
 * and also prevents the access of dao layer and pojo class.
 */
@Service
public class PlayerServiceImpl implements PlayerService {
    private PlayerDAO playerDAO;
    static final Logger logger = Logger.getLogger(PlayerServiceImpl.class);

    /**
     * Constructor initialises the objects that is used throughout the player
     * service layer. 
     */
    @Autowired
    public PlayerServiceImpl(PlayerDAO playerDAO) {
        this.playerDAO = playerDAO;
    }

    @Override
    public PlayerInfo createPlayer(PlayerInfo playerInfo, CommonsMultipartFile file)
            throws GameOfTwentyTwoYardsException, IOException {
        Player player = CommonUtil.convertPlayerInfoToPlayer(playerInfo);
        int id = playerDAO.insertPlayer(player);
        player = playerDAO.getPlayerById(id);
        return updatePlayerWithPic(player,file, "");
    }

    @Override
    public PlayerInfo fetchPlayer(int id) throws GameOfTwentyTwoYardsException {
        Player player = playerDAO.getPlayerById(id);
        return CommonUtil.convertPlayerToPlayerInfo(player);
    }

    @Override
    public Player fetchPlayerWithTeam(int id) throws GameOfTwentyTwoYardsException {
        return playerDAO.getPlayerById(id);
    }

    @Override
    public void deletePlayer(int playerId)
            throws GameOfTwentyTwoYardsException {
        Player player = playerDAO.getPlayerById(playerId);
        player.setPlayerStatus(Boolean.FALSE);
        player.setTeam(null);
        playerDAO.updatePlayer(player);
    }

    @Override
    public PlayerInfo updatePlayerWithPic(Player player,
            CommonsMultipartFile file, String existingImagePath)
            throws GameOfTwentyTwoYardsException, IOException {
        String filename = file.getOriginalFilename();
        if (!filename.isEmpty()) {
            byte barr[] = file.getBytes(); 
            BufferedOutputStream bufferedOutput=new BufferedOutputStream(  
                    new FileOutputStream(Constants.IMAGE_PATH_TO_STORE
                             + player.getId() + filename));  
            bufferedOutput.write(barr);  
            bufferedOutput.flush();  
            bufferedOutput.close();  
            String fileUrl = Constants.IMAGE_PATH_TO_RETRIEVE
                                + player.getId()
                                + filename;
            player.setImagePath(fileUrl);
        } else {
            player.setImagePath(existingImagePath);
        }
        player = playerDAO.updatePlayer(player);
        return CommonUtil.convertPlayerToPlayerInfo(player);
    }

    @Override
    public PlayerInfo updatePlayerWithPic(PlayerInfo playerInfo,
            CommonsMultipartFile file, String existingImagePath)
            throws GameOfTwentyTwoYardsException, IOException {
        Player player = CommonUtil.convertPlayerInfoToPlayer(playerInfo);
        return updatePlayerWithPic(player, file, existingImagePath);
    }

    @Override
    public Player upatePlayerToStorage(Player player) 
            throws GameOfTwentyTwoYardsException, IOException {
        Contact contact = player.getContact();
        contact.setPlayer(player);
        player.setContact(contact);
        return playerDAO.updatePlayer(player);
    }

    @Override
    public int fetchPlayersCount() throws GameOfTwentyTwoYardsException {
        return playerDAO.retrievePlayersCount();
    }

    @Override
    public List<Player> retrievePlayerByLimit(int viewAllOffset)
            throws GameOfTwentyTwoYardsException {
        return playerDAO.retrievePlayerByLimit(viewAllOffset);
    }

    @Override
    public List<Player> fetchPlayersByCountry(String countryAsString)
            throws GameOfTwentyTwoYardsException {
        return playerDAO.fetchPlayersByCountry(countryAsString);
    }

    @Override
    public List<Player> fetchPlayersToBeAdded(List<Integer> playerIds)
            throws GameOfTwentyTwoYardsException {
        return playerDAO.fetchPlayersToBeAdded(playerIds);
    }

    @Override
    public int fetchPlayersCountByRole(int teamId,
            String PlayerRole) throws GameOfTwentyTwoYardsException {
        return playerDAO.fetchPlayersCountByRole(teamId, PlayerRole);
    }

    @Override
    public JSONArray paginateAndFetchAsJson(int page)
            throws GameOfTwentyTwoYardsException {
        int pageOffset = (page * 5) - 5;
        List<Player> players = retrievePlayerByLimit(pageOffset);
        JSONArray array = new JSONArray();
        for (Player player : players) {
            JSONObject playerJSON = new JSONObject();
            playerJSON.put(Constants.ID, player.getId());
            playerJSON.put(Constants.PLAYER_NAME, player.getPlayerName());
            playerJSON.put(Constants.PLAYER_ROLE, player.getPlayerRole());
            playerJSON.put(Constants.BATTING_STYLE,
                    player.getBattingStyle());
            playerJSON.put(Constants.BOWLING_STYLE,
                    player.getBowlingStyle());
            playerJSON.put(Constants.PACE_TYPE, player.getPaceType());
            playerJSON.put(Constants.COUNTRY_NAME,
                    player.getCountryName().toString());
            array.put(playerJSON);
        }
        return array;
    }


    @Override
    public List<Player> paginateAndFetch(int page)
            throws GameOfTwentyTwoYardsException {
        int pageOffset = (page * 5) - 5;
        List<Player> players = retrievePlayerByLimit(pageOffset);
        return players;
    }

    @Override
    public PlayerPaginationInfo fetchPagesWithPlayers()
            throws GameOfTwentyTwoYardsException {
        Set<Player> players = new HashSet<Player>(playerDAO.retrievePlayerByLimit(0));
        List<Integer> pages = new ArrayList<Integer>();
        int totalCount = fetchPlayersCount();
        int PagesCount = totalCount / 5;
        if (0 != (totalCount % 5)) {
            PagesCount = PagesCount + 1;
        }
        for (int loopCount = 1; loopCount <= PagesCount; loopCount++) {
            pages.add(loopCount);
        }
        PlayerPaginationInfo playerPaginationInfo = new PlayerPaginationInfo();
        playerPaginationInfo.setPlayerInfos(CommonUtil.convertPlayersToPlayerInfos(players));
        playerPaginationInfo.setPages(pages);
        return playerPaginationInfo;
    }
}
