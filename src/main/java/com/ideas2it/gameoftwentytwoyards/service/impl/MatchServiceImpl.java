package com.ideas2it.gameoftwentytwoyards.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.common.MatchType;
import com.ideas2it.gameoftwentytwoyards.dao.MatchDAO;
import com.ideas2it.gameoftwentytwoyards.entities.Match;
import com.ideas2it.gameoftwentytwoyards.info.MatchInfo;
import com.ideas2it.gameoftwentytwoyards.info.TeamInfo;
import com.ideas2it.gameoftwentytwoyards.info.MatchPaginationInfo;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.exception.MatchNotFoundException;
import com.ideas2it.gameoftwentytwoyards.service.MatchService;
import com.ideas2it.gameoftwentytwoyards.service.TeamService;
import com.ideas2it.gameoftwentytwoyards.utils.DateUtil;
import com.ideas2it.gameoftwentytwoyards.utils.CommonUtil;

@Service
public class MatchServiceImpl implements MatchService {
    private MatchDAO matchDAO;
    private TeamService teamService;

    /**
     * Constructor initialises the objects that is used throughout the match
     * service layer. 
     */
    @Autowired
    public MatchServiceImpl(TeamService teamService, MatchDAO matchDAO) {
        this.matchDAO = matchDAO;
        this.teamService = teamService;
    }

    @Override
    public MatchPaginationInfo createMatch(MatchInfo matchInfo, String date)
            throws GameOfTwentyTwoYardsException {
        Date matchDate = DateUtil.parseDate(date);
        matchInfo.setMatchDate(matchDate);
        Match match = CommonUtil.convertMatchInfoToMatch(matchInfo);
        int id = matchDAO.createMatch(match);
        MatchPaginationInfo matchPaginationInfo = filterTeamsByDate(id);
        return matchPaginationInfo;
                
    }

    @Override
    public MatchInfo updateMatch(String[] teamIds, MatchInfo matchInfo, String date)
            throws GameOfTwentyTwoYardsException {
        Date matchDate = DateUtil.parseDate(date);
        matchInfo.setMatchDate(matchDate);
        Match match = CommonUtil.convertMatchInfoToMatch(matchInfo);
        Set<Team> teams  = fetchTeamsForMatch(teamIds);
        match.setTeamsForMatch(teams);
        match = matchDAO.updateMatch(match);
        return CommonUtil.convertMatchToMatchInfo(match);
    }

    @Override
    public MatchPaginationInfo filterTeamsByDate(int id)
             throws GameOfTwentyTwoYardsException {
        Match match = matchDAO.retrieveMatch(id);
        Date matchDate = match.getMatchDate();
        Set<Team> teamsForMatch = new HashSet();
        Set<Team> teams = teamService.fetchAllTeams();
        for(Team team : teams) {
            boolean isMatchPresent = Boolean.FALSE;
            if(null != team.getMatches()) {
                Set<Match> matches = team.getMatches();
                for(Match singleMatch : matches) {
                    if (matchDate.equals(match.getMatchDate())) {
                        isMatchPresent = Boolean.TRUE;
                    }
                }
                if(!isMatchPresent) {
                    teamsForMatch.add(team);
                }
            }              
        }
        MatchPaginationInfo matchPaginationInfo = new MatchPaginationInfo();
        matchPaginationInfo.setMatchInfo(CommonUtil.convertMatchToMatchInfo(match));
        matchPaginationInfo.setTeamInfos(CommonUtil.convertTeamsToTeamInfos(teams));
        return matchPaginationInfo;
    }

    @Override
    public MatchInfo retrieveMatch(int matchId)
             throws GameOfTwentyTwoYardsException {
         if (null == matchDAO.retrieveMatch(matchId)) {
            throw new MatchNotFoundException(Constants.PHRASE_CONTENT_MISSING);
        } else {
            Match match = matchDAO.retrieveMatch(matchId); 
            return CommonUtil.convertMatchToMatchInfo(match);
        }
    }

    @Override
    public Match addTeamsForMatch(String[] teamIds, int matchId)
            throws GameOfTwentyTwoYardsException {
        Match match = matchDAO.retrieveMatch(matchId);
        Set<Team> teams  = fetchTeamsForMatch(teamIds);
        match.setTeamsForMatch(teams);
        return matchDAO.updateMatch(match);
    }

    @Override
    public void deleteMatch(int matchId) throws GameOfTwentyTwoYardsException {
        matchDAO.deleteMatch(matchId);
    }

    @Override
    public Set<Team> fetchTeamsForMatch(String[] teamIds)
            throws GameOfTwentyTwoYardsException {
        Set<Team> teams = null;
        if(null != teamIds) {
            List<Integer> ids = new ArrayList<Integer>();
            for (String id : teamIds) {
                ids.add(Integer.parseInt(id));
            }
            teams  = teamService.fetchTeamsForMatch(ids);
        }
        return teams;
    }

    @Override
    public List<Match> retrieveMatchesByLimit(int Offset)
            throws GameOfTwentyTwoYardsException {
        return matchDAO.retrieveMatchesByLimit(Offset);
    }

    @Override
    public JSONArray paginateAndFetchAsJson(int page)
            throws GameOfTwentyTwoYardsException {
        int pageOffset = (page * 5) - 5;
        List<Match> matches = retrieveMatchesByLimit(pageOffset);
        JSONArray array = new JSONArray();
        for(Match match : matches) {
            JSONObject matchJSON = new JSONObject();
            matchJSON.put(Constants.ID, match.getId());
            matchJSON.put(Constants.MATCH_NAME, match.getMatchName());
            matchJSON.put(Constants.MATCH_LOCATION,
                    match.getMatchLocation());
            matchJSON.put(Constants.MATCH_DATE, match.getMatchDate());
            matchJSON.put(Constants.MATCH_TYPE,
                    match.getMatchType().toString());
            array.put(matchJSON);
        }
        return array;
    }

    @Override
    public List<Match> paginateAndFetch(int page)
            throws GameOfTwentyTwoYardsException {
        int pageOffset = (page * 5) - 5;
        List<Match> matches = retrieveMatchesByLimit(pageOffset);
        return matches;
    }

    @Override
    public MatchPaginationInfo fetchTeamsForSelection(int matchId)
            throws GameOfTwentyTwoYardsException {
        Match match = matchDAO.retrieveMatch(matchId);
        Set<TeamInfo> teams = CommonUtil.convertTeamsToTeamInfos(match.getTeamsForMatch());
        List<TeamInfo> teamForMatch = new ArrayList(teams);
        TeamInfo battingTeam = null;
        TeamInfo bowlingTeam = null;
        if (null != teamForMatch.get(0)) {
            battingTeam = teamForMatch.get(0);
        }
        if (null != teamForMatch.get(1)) {
            bowlingTeam = teamForMatch.get(1);
        }
        MatchPaginationInfo matchPaginationInfo = new MatchPaginationInfo();
        matchPaginationInfo.setMatchInfo(CommonUtil.convertMatchToMatchInfo(match));
        matchPaginationInfo.setBattingTeam(battingTeam);
        battingTeam = matchPaginationInfo.getBattingTeam();
        matchPaginationInfo.setBowlingTeam(bowlingTeam);
        return matchPaginationInfo;
    }

    @Override
    public MatchPaginationInfo fetchPagesWithMatches()
            throws GameOfTwentyTwoYardsException {
        Set<Match> matches = new HashSet(matchDAO.retrieveMatchesByLimit(0));
        List<Integer> pages = new ArrayList<Integer>();
        int totalCount = matchDAO.fetchMatchesCount();
        System.out.println("***********************CAMEBACK SERVICE**************");
        int PagesCount = totalCount / 5;
        if(0 != (totalCount % 5)) {
            PagesCount = PagesCount + 1;
        }
        for(int loopCount = 1; loopCount <= PagesCount; loopCount++) {
            pages.add(loopCount);
        }
        MatchPaginationInfo matchPaginationInfo = new MatchPaginationInfo();
        matchPaginationInfo.setMatchInfos(CommonUtil.convertMatchesToMatchInfos(matches));
        System.out.println("***********************SERVICE CAME 210**************");
        matchPaginationInfo.setPages(pages);
        return matchPaginationInfo;
    }
}
