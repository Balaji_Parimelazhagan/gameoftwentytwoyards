package com.ideas2it.gameoftwentytwoyards.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.dao.OverDAO;
import com.ideas2it.gameoftwentytwoyards.entities.Match;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.entities.Over;
import com.ideas2it.gameoftwentytwoyards.entities.OverDetail;
import com.ideas2it.gameoftwentytwoyards.info.OverStartInfo;
import com.ideas2it.gameoftwentytwoyards.info.PlayerInfo;
import com.ideas2it.gameoftwentytwoyards.info.TeamInfo;
import com.ideas2it.gameoftwentytwoyards.info.MatchInfo;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.service.TeamService;
import com.ideas2it.gameoftwentytwoyards.service.MatchService;
import com.ideas2it.gameoftwentytwoyards.service.OverService;
import com.ideas2it.gameoftwentytwoyards.service.PlayerService;
import com.ideas2it.gameoftwentytwoyards.utils.CommonUtil;

@Service
public class OverServiceImpl implements OverService {
    private OverDAO overDAO;
    private MatchService matchService;
    private PlayerService playerService;
    private TeamService teamService;

    /**
     * Constructor initialises the objects that is used throughout the match
     * service layer. 
     */
    @Autowired
    public OverServiceImpl(OverDAO overDAO, MatchService matchService,
            PlayerService playerService, TeamService teamService) {
        this.overDAO = overDAO;
        this.matchService = matchService;
        this.playerService = playerService;
        this.teamService = teamService;
    }

    @Override
    public OverStartInfo startOver(int matchId, String[] batsmanIds,
            int bowlerId, String battingTeamName, String bowlingTeamName)
            throws GameOfTwentyTwoYardsException {
        MatchInfo matchInfo = matchService.retrieveMatch(matchId);
        Match match = CommonUtil.convertMatchInfoToMatch(matchInfo);
        int overNo = overDAO.fetchLatestOverId(matchId);
        Over over = new Over();
        over.setMatchId(matchId);
        over.setOverNo(++overNo);
        int id = overDAO.insertOver(over);
        OverStartInfo overStartInfo = new OverStartInfo();
        overStartInfo.setBattingTeamName(battingTeamName);
        overStartInfo.setBowlingTeamName(bowlingTeamName);
        overStartInfo.setOverNo(--overNo);
        overStartInfo.setMatchInfo(matchInfo);
        overStartInfo.setTotalRuns(0);
        return fetchBatsmenAndBowler(batsmanIds, bowlerId, overStartInfo);
    }

    public Over fetchOverById(int id) throws GameOfTwentyTwoYardsException {
        return overDAO.retrieveOver(id);
    }

    @Override
    public void updateOverWithOverDetail(OverDetail overDetail, int overId)
            throws GameOfTwentyTwoYardsException {
        Over over = overDAO.retrieveOver(overId);
        Set<OverDetail> overDetails = null;
        if(null != over.getOverDetails()) {
            overDetails = over.getOverDetails();
            overDetails.add(overDetail);
            over.setOverDetails(overDetails);
        } else {
            overDetails.add(overDetail);
            over.setOverDetails(overDetails);
        }
        overDAO.updateOver(over);
    }

    @Override
    public OverStartInfo chooseBowlerForNextOver(OverStartInfo overStartInfo)
            throws GameOfTwentyTwoYardsException {
                
        Player bowler = new Player();
        bowler = playerService.fetchPlayerWithTeam(
                    overStartInfo.getBowler().getId());
        overStartInfo.setBowler(CommonUtil.convertPlayerToPlayerInfo(bowler));
        int strikerId = overStartInfo.getStrikerId();
        int firstBatsmanId = overStartInfo.getFirstBatsman().getId();
        int secondBatsmanId = overStartInfo.getSecondBatsman().getId();
        if (strikerId == firstBatsmanId) {
            strikerId = secondBatsmanId;
        } else {
            strikerId = firstBatsmanId;
        }
        overStartInfo.setStrikerId(strikerId);
        Team team = bowler.getTeam();
        TeamInfo bowlingTeam = teamService.retrieveTeam(team.getId());
        Set<PlayerInfo> playersInTeam = bowlingTeam.getPlayersInTeam();
        Set<PlayerInfo> bowlersForNextOver = new HashSet();
        for(PlayerInfo player : playersInTeam) {
            if ((bowler.getId() != player.getId()) &&
                    ("Bowler".equals(player.getPlayerRole())) &&
                    ("All-Rounder".equals(player.getPlayerRole()))) {
                bowlersForNextOver.add(player);
            }
        }
        bowlingTeam.setPlayersInTeam(bowlersForNextOver);
        overStartInfo.setBowlingTeam(bowlingTeam);
        MatchInfo matchInfo = new MatchInfo();
        matchInfo.setId(overStartInfo.getMatchInfo().getId());
        overStartInfo.setMatchInfo(matchInfo);
        return overStartInfo;
    }

    @Override
    public OverStartInfo startNextOver(OverStartInfo overStartInfo,int bowlerId)
            throws GameOfTwentyTwoYardsException {
        PlayerInfo bowler = playerService.fetchPlayer(bowlerId);
        overStartInfo.setBowler(bowler);
        int matchId = overStartInfo.getMatchInfo().getId();
        MatchInfo matchInfo = matchService.retrieveMatch(matchId);
        Match match = CommonUtil.convertMatchInfoToMatch(matchInfo);
        int overNo = overDAO.fetchLatestOverId(
                        overStartInfo.getMatchInfo().getId());
        Over over = new Over();
        over.setMatchId(matchId);
        over.setOverNo(++overNo);
        int id = overDAO.insertOver(over);
        overStartInfo.setBowlingTeamName(
                overStartInfo.getBowlingTeam().getTeamName());
        overStartInfo.setOverNo(--overNo);
        overStartInfo.setMatchInfo(matchInfo);
        overStartInfo.setBallNumber(0);
        return overStartInfo;
    }

   @Override
    public OverStartInfo chooseBatsmanAfterWicket(OverStartInfo overStartInfo)
            throws GameOfTwentyTwoYardsException {
        int strikerId = overStartInfo.getStrikerId();
        
        Player dismissedBatsman = new Player();
        dismissedBatsman =
                playerService.fetchPlayerWithTeam(overStartInfo.getStrikerId());
        Team team = dismissedBatsman.getTeam();
        TeamInfo battingTeam = teamService.retrieveTeam(team.getId());
        overStartInfo.setBattingTeam(battingTeam);
        MatchInfo matchInfo = new MatchInfo();
        matchInfo.setId(overStartInfo.getMatchInfo().getId());
        overStartInfo.setMatchInfo(matchInfo);
        return overStartInfo;
    }

    @Override
    public OverStartInfo fetchNewBatsman(OverStartInfo overStartInfo,
            int batsmanId) throws GameOfTwentyTwoYardsException {
        PlayerInfo newBatsman = new PlayerInfo();
        newBatsman = playerService.fetchPlayer(batsmanId);
        int strikerId = overStartInfo.getStrikerId();
        int firstBatsmanId = overStartInfo.getFirstBatsman().getId();
        int secondBatsmanId = overStartInfo.getSecondBatsman().getId();
        if (strikerId == firstBatsmanId) {
            overStartInfo.setFirstBatsmanRuns(0);
            overStartInfo.setFirstBatsman(newBatsman);
            overStartInfo.setStrikerId(newBatsman.getId());
        } else {
            overStartInfo.setSecondBatsmanRuns(0);
            overStartInfo.setSecondBatsman(newBatsman);
            overStartInfo.setStrikerId(newBatsman.getId());
        }
        return overStartInfo;
    }

    /**
     * Fetches the Batsmen and Bowler who are about to play that over based
     * on the ids of sets into the info object and the info object.
     *
     * @param batsmanIds of String[], It is the ids of the two batsmen who are
     * heading to play that over.
     * @param bowlerId, It is the id of the Bowler who his going to bowl that
     * over.
     *
     * @throws GameOfTwentyTwoYardsException, it is user defined exceptions
     * which has a context within it.
     */
    private OverStartInfo fetchBatsmenAndBowler(String[] batsmanIds,
            int bowlerId, OverStartInfo overStartInfo)
            throws GameOfTwentyTwoYardsException {
        PlayerInfo bowler = playerService.fetchPlayer(bowlerId);
        List<Player> players = null;

        /* converts the ids into a list of integer ( since IN Query accepts the
         parameter as list of integer) and fetches the batsmen*/
        if(null != batsmanIds) {
            List<Integer> ids = new ArrayList<Integer>();
            for (String id : batsmanIds) {
                ids.add(Integer.parseInt(id));
            }
            players = playerService.fetchPlayersToBeAdded(ids);
        }
        PlayerInfo firstBatsman =
                CommonUtil.convertPlayerToPlayerInfo(players.get(0));
        PlayerInfo secondBatsman =
                CommonUtil.convertPlayerToPlayerInfo(players.get(1));
        overStartInfo.setBowler(bowler);
        overStartInfo.setFirstBatsman(firstBatsman);
        overStartInfo.setSecondBatsman(secondBatsman);
        overStartInfo.setStrikerId(firstBatsman.getId());
        return overStartInfo;
    }
}
