package com.ideas2it.gameoftwentytwoyards.service.impl;

import java.lang.StringBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.dao.TeamDAO;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.info.TeamInfo;
import com.ideas2it.gameoftwentytwoyards.info.TeamPaginationInfo;
import com.ideas2it.gameoftwentytwoyards.service.PlayerService;
import com.ideas2it.gameoftwentytwoyards.service.TeamService;
import com.ideas2it.gameoftwentytwoyards.utils.CommonUtil;

/**
 * Performs the create, update calculations and delete operations on a player
 * and also prevents the access of dao layer and pojo class.
 */
@Service
public class TeamServiceImpl implements TeamService {
    private PlayerService playerService;
    private TeamDAO teamDAO;

    /**
     * Constructor initialises the objects that is used throughout the team
     * service layer. 
     */
    @Autowired
    public TeamServiceImpl(PlayerService playerService, TeamDAO teamDAO) {
        this.playerService = playerService;
        this.teamDAO = teamDAO;
    }

    @Override
    public TeamPaginationInfo createTeam(TeamInfo teamInfo)
            throws GameOfTwentyTwoYardsException {
        Team team = CommonUtil.convertTeamInfoToTeam(teamInfo);
        int id = teamDAO.createTeam(team);
        return fetchTeamAndPlayersByCountry(id);
    }

    @Override
    public Team validateTeamCombination(int teamId, Team team)
            throws GameOfTwentyTwoYardsException {
        boolean teamStatus = Boolean.FALSE;
        int allRoundersCount = playerService.fetchPlayersCountByRole(
                                  teamId, Constants.ALL_ROUNDER);
        int batsmansCount = playerService.fetchPlayersCountByRole(
                               teamId, Constants.BATSMAN);
        int bowlersCount = playerService.fetchPlayersCountByRole(
                              teamId, Constants.BOWLER);
        int wKeepersCount = playerService.fetchPlayersCountByRole(
                               teamId, Constants.WICKET_KEEPER);
        int totalPlayers = allRoundersCount + batsmansCount + bowlersCount
                                    + wKeepersCount;
            if (totalPlayers != 11 || 3 > bowlersCount || 1 > wKeepersCount
                    || 3 > batsmansCount) {
                teamStatus = Boolean.FALSE;
            } else {
                teamStatus = Boolean.TRUE;
            }
        team.setTeamStatus(teamStatus);
        Team existingTeam = teamDAO.retrieveTeam(teamId);
        team.setMatches(existingTeam.getMatches());
        return teamDAO.updateTeam(team);
    }

    @Override
    public Set<Team> fetchAllTeams() throws GameOfTwentyTwoYardsException {
        return teamDAO.fetchAllTeams();
    }

    @Override
    public TeamPaginationInfo fetchTeamAndPlayersByCountry(int id)
             throws GameOfTwentyTwoYardsException {
        Team team = teamDAO.retrieveTeam(id);
        String country = team.getCountry().toString();
        Set<Player> players = new HashSet<Player>(
                                 playerService.fetchPlayersByCountry(country));
        TeamPaginationInfo teamPaginationInfo = new TeamPaginationInfo();
        teamPaginationInfo.setTeamInfo(CommonUtil.convertTeamToTeamInfo(team));
        teamPaginationInfo.setPlayerInfos(CommonUtil.convertPlayersToPlayerInfos(players));
        return teamPaginationInfo;
    }

    @Override
    public TeamInfo updateTeamWithPlayers(String[] playerIds, TeamInfo teamInfo)
            throws GameOfTwentyTwoYardsException {
        Team team = CommonUtil.convertTeamInfoToTeam(teamInfo);
        Set<Player> players = fetchPlayersToBeAdded(playerIds);
        team.setPlayersInTeam(players);
        team = teamDAO.updateTeam(team);
        team = validateTeamCombination(team.getId(), team);
            System.out.println(team.getTeamName());
        return CommonUtil.convertTeamToTeamInfo(team);
    }

    @Override
    public Set<Player> fetchPlayersToBeAdded(String[] playerIds)
            throws GameOfTwentyTwoYardsException {
        Set<Player> players = null;
        if(null != playerIds) {
            List<Integer> ids = new ArrayList<Integer>();
            for (String id : playerIds) {
                ids.add(Integer.parseInt(id));
            }
            List<Player> playersToBeAdded  =
                    playerService.fetchPlayersToBeAdded(ids);
            players = new HashSet<Player>(playersToBeAdded);
        }
        return players;
    }

    @Override
    public TeamInfo retrieveTeam(int teamId) throws GameOfTwentyTwoYardsException {
        Team team = teamDAO.retrieveTeam(teamId);
        return CommonUtil.convertTeamToTeamInfo(team);
    }

    @Override
    public void deleteTeam(int teamId) throws GameOfTwentyTwoYardsException {
        teamDAO.deleteTeam(teamId);
    }

    @Override
    public void addPlayersToTeam(Set<Player> PlayersToTeam, Team team) {
        team.setPlayersInTeam(PlayersToTeam);
    }

    @Override
    public Set<Team> fetchTeamsForMatch(List<Integer> teamIds)
             throws GameOfTwentyTwoYardsException {
        return teamDAO.fetchTeamsForMatch(teamIds);
    }

    @Override
    public List<Team> retrieveTeamsByLimit(int Offset)
            throws GameOfTwentyTwoYardsException {
        return teamDAO.retrieveTeamsByLimit(Offset);
    }

    @Override
    public JSONArray paginateAndFetchAsJson(int page)
            throws GameOfTwentyTwoYardsException {
        int pageOffset = (page * 5) - 5;
        List<Team> teams = retrieveTeamsByLimit(pageOffset);
        JSONArray array = new JSONArray();
        for(Team team : teams) {
            JSONObject teamJSON = new JSONObject();
            teamJSON.put(Constants.ID, team.getId());
            teamJSON.put(Constants.TEAM_NAME, team.getTeamName());
            teamJSON.put(Constants.TEAM_STATUS, team.getTeamStatus());
            teamJSON.put(Constants.COUNTRY, team.getCountry().toString());
            array.put(teamJSON);
        }
        return array;
    }


    @Override
    public List<Team> paginateAndFetch(int page)
            throws GameOfTwentyTwoYardsException {
        int pageOffset = (page * 5) - 5;
        List<Team> teams = retrieveTeamsByLimit(pageOffset);
        return teams;
    }

    @Override
    public TeamPaginationInfo fetchPagesWithTeams() throws GameOfTwentyTwoYardsException {
        Set<Team> teams = new HashSet(teamDAO.retrieveTeamsByLimit(0));
        List<Integer> pages = new ArrayList<Integer>();
        int totalCount = teamDAO.fetchTeamsCount();
        int PagesCount = totalCount / 5;
        if(0 != (totalCount % 5)) {
            PagesCount = PagesCount + 1;
        }
        for(int loopCount = 1; loopCount <= PagesCount; loopCount++) {
            pages.add(loopCount);
        }
        TeamPaginationInfo teamPaginationInfo = new TeamPaginationInfo();
        teamPaginationInfo.setTeamInfos(CommonUtil.convertTeamsToTeamInfos(teams));
        teamPaginationInfo.setPages(pages);
        return teamPaginationInfo;
    }
}
