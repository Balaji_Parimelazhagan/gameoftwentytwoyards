package com.ideas2it.gameoftwentytwoyards.service;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ideas2it.gameoftwentytwoyards.info.OverStartInfo;
import com.ideas2it.gameoftwentytwoyards.entities.Over;
import com.ideas2it.gameoftwentytwoyards.entities.OverDetail;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;

public interface OverService {

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param details of the match.
     * @return matchInfo of MatchInfo, consist of match and teams without date
     * collisions.
     */
    OverStartInfo startOver(int matchId, String[] batsmanIds,
             int bowlerId, String battingTeamName, String bowlingTeamName)
            throws GameOfTwentyTwoYardsException;

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param details of the match.
     * @return matchInfo of MatchInfo, consist of match and teams without date
     * collisions.
     */
    Over fetchOverById(int id) throws GameOfTwentyTwoYardsException;

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param details of the match.
     * @return matchInfo of MatchInfo, consist of match and teams without date
     * collisions.
     */
    void updateOverWithOverDetail(OverDetail overDetail, int overId)
            throws GameOfTwentyTwoYardsException;

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param details of the match.
     * @return matchInfo of MatchInfo, consist of match and teams without date
     * collisions.
     */
     OverStartInfo chooseBowlerForNextOver(OverStartInfo overStartInfo)
            throws GameOfTwentyTwoYardsException;

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param details of the match.
     * @return matchInfo of MatchInfo, consist of match and teams without date
     * collisions.
     */
     OverStartInfo startNextOver(OverStartInfo overStartInfo,int bowlerId)
            throws GameOfTwentyTwoYardsException;

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param details of the match.
     * @return matchInfo of MatchInfo, consist of match and teams without date
     * collisions.
     */
    OverStartInfo chooseBatsmanAfterWicket(OverStartInfo overStartInfo)
            throws GameOfTwentyTwoYardsException;

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param details of the match.
     * @return matchInfo of MatchInfo, consist of match and teams without date
     * collisions.
     */
    OverStartInfo fetchNewBatsman(OverStartInfo overStartInfo,int batsmanId)
            throws GameOfTwentyTwoYardsException;
}

