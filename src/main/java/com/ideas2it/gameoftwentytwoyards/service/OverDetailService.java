package com.ideas2it.gameoftwentytwoyards.service;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ideas2it.gameoftwentytwoyards.info.OverStartInfo;
import com.ideas2it.gameoftwentytwoyards.entities.Over;
import com.ideas2it.gameoftwentytwoyards.entities.OverDetail;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;

public interface OverDetailService {

    /**
     * Collects the Match information from user and stores as an object, It also
     * performs in retrieval of teams which doesnt have the date Collisions.
     *
     * @param details of the match.
     * @return matchInfo of MatchInfo, consist of match and teams without date
     * collisions.
     */
    public JSONObject generateBallAndSaveDetail(int firstBatsmanId,
            int secondBatsmanId, int bowlerId, int overId, int strikerId,
            int matchId, int ballNumber)
            throws GameOfTwentyTwoYardsException;
}

