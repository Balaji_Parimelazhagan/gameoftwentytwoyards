package com.ideas2it.gameoftwentytwoyards.utils;

import java.time.LocalDate;
import java.time.Period;

public class AgeUtil {
    /**
     * Converts the DOB from yyyy-mm-dd to dd-mm-yyyyy, Validates the Player's
     * age(10-120) is entered.
     *
     * @param playerDOB as String, Date of Birth of the player.
     * @return checkAgeLimit(ageCheck, validAgeCheck), if the entered DOB is in
     *  correct format then the method is called.
     */
    public static String fetchPlayerAge(String playerDOB) {
        String playerAge = "";
        LocalDate dob = LocalDate.parse(playerDOB);
        LocalDate currentDate = LocalDate.now();
        Period age = Period.between(dob, currentDate);
        playerAge = Integer.toString(age.getYears()) + "years"
                       + Integer.toString(age.getMonths()) + "months"
                       + Integer.toString(age.getDays()) + "days";
        return playerAge;
    }

}
