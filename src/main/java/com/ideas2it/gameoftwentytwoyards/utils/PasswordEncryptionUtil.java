package com.ideas2it.gameoftwentytwoyards.utils;

import java.math.BigInteger; 
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

/**
 * Encrypts the Given data.
 */
public class PasswordEncryptionUtil {

    static final Logger logger = Logger.getLogger(PasswordEncryptionUtil.class);

    /**
     * Encryptes the raw password entered by the user.
     *
     * @param password of String, password entered by the user.
     */
    public static String encryptPassword(String password) {
        String encryptedPassword = null;
        try {
            MessageDigest encryption = MessageDigest.getInstance("SHA-512"); 
            byte[] messageDigest = encryption.digest(password.getBytes()); 
            BigInteger number = new BigInteger(1, messageDigest); 
            encryptedPassword = number.toString(16); 
            while (encryptedPassword.length() < 32) { 
                encryptedPassword = "0" + encryptedPassword; 
            }
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getMessage());
        }
        return encryptedPassword;
    }
}
