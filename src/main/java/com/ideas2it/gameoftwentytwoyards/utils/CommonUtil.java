package com.ideas2it.gameoftwentytwoyards.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.ideas2it.gameoftwentytwoyards.entities.Contact;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.entities.Over;
import com.ideas2it.gameoftwentytwoyards.entities.Match;
import com.ideas2it.gameoftwentytwoyards.info.PlayerInfo;
import com.ideas2it.gameoftwentytwoyards.info.TeamInfo;
import com.ideas2it.gameoftwentytwoyards.info.MatchInfo;
import com.ideas2it.gameoftwentytwoyards.info.OverInfo;

public class CommonUtil {

    public static Player convertPlayerInfoToPlayer(PlayerInfo playerInfo) {
        Player player = new Player();
        player.setId(playerInfo.getId());
        if (null != playerInfo.getCountryName()) {
            player.setCountryName(playerInfo.getCountryName());
        }
        if (null != playerInfo.getBattingStyle()) {
            player.setBattingStyle(playerInfo.getBattingStyle());
        }
        if (null != playerInfo.getBowlingStyle()) {
            player.setBowlingStyle(playerInfo.getBowlingStyle());
        }
        if (null != playerInfo.getDOB()) {
            player.setDOB(playerInfo.getDOB());
        }
        if (null != playerInfo.getImagePath()) {
            player.setImagePath(playerInfo.getImagePath());
        }
        if (null != playerInfo.getPaceType()) {
            player.setPaceType(playerInfo.getPaceType());
        }
        if (null != playerInfo.getPlayerName()) {
            player.setPlayerName(playerInfo.getPlayerName());
        }
        if (null != playerInfo.getPlayerRole()) {
            player.setPlayerRole(playerInfo.getPlayerRole());
        }/*
        if (null != playerInfo.getTeam()) {
            Team team = convertTeamInfoToTeam(playerInfo.getTeam());
            player.setTeam(team);
        }*/
        Contact contact = player.getContact();
        contact.setId(playerInfo.getContactId());
        if (null != playerInfo.getAddress()) {
            contact.setAddress(playerInfo.getAddress());
        }
        if (null != playerInfo.getCity()) {
            contact.setCity(playerInfo.getCity());
        }
        if (null != playerInfo.getContactNumber()) {
            contact.setContactNumber(playerInfo.getContactNumber());
        }
        contact.setPlayer(player);
        player.setContact(contact);
        return player;
    }

    public static PlayerInfo convertPlayerToPlayerInfo(Player player) {
        PlayerInfo playerInfo = new PlayerInfo();
        playerInfo.setId(player.getId());
        if (null != player.getCountryName()) {
            playerInfo.setCountryName(player.getCountryName());
        }
        if (null != player.getBattingStyle()) {
            playerInfo.setBattingStyle(player.getBattingStyle());
        }
        if (null != player.getBowlingStyle()) {
            playerInfo.setBowlingStyle(player.getBowlingStyle());
        }
        if (null != player.getDOB()) {
            playerInfo.setDOB(player.getDOB());
        }
        if (null != player.getImagePath()) {
            playerInfo.setImagePath(player.getImagePath());
        }
        if (null != player.getPaceType()) {
            playerInfo.setPaceType(player.getPaceType());
        }
        if (null != player.getPlayerAge()) {
            playerInfo.setPlayerAge(player.getPlayerAge());
        }
        if (null != player.getPlayerName()) {
            playerInfo.setPlayerName(player.getPlayerName());
        }
        if (null != player.getPlayerRole()) {
            playerInfo.setPlayerRole(player.getPlayerRole());
        }/*
        if (null != player.getTeam()) {
            TeamInfo teamInfo = convertTeamToTeamInfo(player.getTeam());
            playerInfo.setTeam(teamInfo);
        }*/
        Contact contact = player.getContact();
        playerInfo.setContactId(contact.getId());
        if (null != contact.getAddress()) {
            playerInfo.setAddress(contact.getAddress());
        }
        if (null != contact.getCity()) {
            playerInfo.setCity(contact.getCity());
        }
        if (null != contact.getContactNumber()) {
            playerInfo.setContactNumber(contact.getContactNumber());
        }
        return playerInfo;
    }

    public static Set<PlayerInfo> convertPlayersToPlayerInfos(Set<Player> players) {
        Set<PlayerInfo> playerInfos = new HashSet<PlayerInfo>();
        for (Player player : players) {
            playerInfos.add(convertPlayerToPlayerInfo(player));
        }
        return playerInfos;
    }

    public static Set<Player> convertPlayerInfosToPlayers(Set<PlayerInfo> playerInfos) {
        Set<Player> players = new HashSet<Player>();
        for (PlayerInfo playerInfo : playerInfos) {
            players.add(convertPlayerInfoToPlayer(playerInfo));
        }
        return players;
    }

    public static Team convertTeamInfoToTeam(TeamInfo teamInfo) {
        Team team = new Team();
        team.setId(teamInfo.getId());
        team.setTeamStatus(teamInfo.getTeamStatus());
        if (null != teamInfo.getCountry()) {
            team.setCountry(teamInfo.getCountry());
        }
        if (null != teamInfo.getTeamName()) {
            team.setTeamName(teamInfo.getTeamName());
        }/*
        if (null != teamInfo.getMatches()) {
            Set<MatchInfo> matchInfos = teamInfo.getMatches();
            Set<Match> matches = new HashSet<Match>(convertMatchInfosToMatches(matchInfos));
            team.setMatches(matches);
        }*/
        if (null != teamInfo.getPlayersInTeam()) {
            Set<PlayerInfo> playerInfos = teamInfo.getPlayersInTeam();
            Set<Player> players = new HashSet<Player>(convertPlayerInfosToPlayers(playerInfos));
            team.setPlayersInTeam(players);
        }
        return team;
    }

    public static TeamInfo convertTeamToTeamInfo(Team team) {
        TeamInfo teamInfo = new TeamInfo();
        teamInfo.setId(team.getId());
        teamInfo.setTeamStatus(team.getTeamStatus());
        if (null != team.getCountry()) {
            teamInfo.setCountry(team.getCountry());
        }
        if (null != team.getTeamName()) {
            teamInfo.setTeamName(team.getTeamName());
        }
        /*if (null != team.getMatches()) {
            Set<Match> matches = team.getMatches();
            Set<MatchInfo> matchInfos = new HashSet<MatchInfo>(convertMatchesToMatchInfos(matches));
            teamInfo.setMatches(matchInfos);
        }*/
        if (null != team.getPlayersInTeam()) {
            Set<Player> players = team.getPlayersInTeam();
            Set<PlayerInfo> playerInfos =
                    new HashSet<PlayerInfo>(convertPlayersToPlayerInfos(players));
            teamInfo.setPlayersInTeam(playerInfos);
        }
        return teamInfo;
    }

    public static Set<TeamInfo> convertTeamsToTeamInfos(Set<Team> teams) {
        Set<TeamInfo> teamInfos = new HashSet<TeamInfo>();
        for (Team team : teams) {
            teamInfos.add(convertTeamToTeamInfo(team));
        }
        return teamInfos;
    }

    public static Set<Team> convertTeamInfosToTeams(Set<TeamInfo> teamInfos) {
        Set<Team> teams = new HashSet<Team>();
        for (TeamInfo teamInfo : teamInfos) {
            teams.add(convertTeamInfoToTeam(teamInfo));
        }
        return teams;
    }


    public static Match convertMatchInfoToMatch(MatchInfo matchInfo) {
        Match match = new Match();
        match.setId(matchInfo.getId());
        if (null != matchInfo.getMatchDate()) {
            match.setMatchDate(matchInfo.getMatchDate());
        }
        if (null != matchInfo.getMatchType()) {
            match.setMatchType(matchInfo.getMatchType());
        }
        if (null != matchInfo.getMatchLocation()) {
            match.setMatchLocation(matchInfo.getMatchLocation());
        }
        if (null != matchInfo.getMatchName()) {
            match.setMatchName(matchInfo.getMatchName());
        }
        if (null != matchInfo.getTeamsForMatch()) {
            Set<TeamInfo> teamInfos = matchInfo.getTeamsForMatch();
            Set<Team> teams = new HashSet<Team>(convertTeamInfosToTeams(teamInfos));
            match.setTeamsForMatch(teams);
        }
        return match;
    }

    public static MatchInfo convertMatchToMatchInfo(Match match) {
        MatchInfo matchInfo = new MatchInfo();
        matchInfo.setId(match.getId());
        if (null != match.getMatchDate()) {
            matchInfo.setMatchDate(match.getMatchDate());
        }
        if (null != match.getMatchType()) {
            matchInfo.setMatchType(match.getMatchType());
        }
        if (null != match.getMatchLocation()) {
            matchInfo.setMatchLocation(match.getMatchLocation());
        }
        if (null != match.getMatchName()) {
            matchInfo.setMatchName(match.getMatchName());
        }
        if (null != match.getTeamsForMatch()) {
            Set<Team> teams = match.getTeamsForMatch();
            Set<TeamInfo> teamInfos =
                new HashSet<TeamInfo>(convertTeamsToTeamInfos(teams));
            matchInfo.setTeamsForMatch(teamInfos);
        }
        return matchInfo;
    }

    public static Set<MatchInfo> convertMatchesToMatchInfos(Set<Match> matches) {
        Set<MatchInfo> matchInfos = new HashSet<MatchInfo>();
        for (Match match : matches) {
            matchInfos.add(convertMatchToMatchInfo(match));
        }
        return matchInfos;
    }

    public static Set<Match> convertMatchInfosToMatches(Set<MatchInfo> matchInfos) {
        Set<Match> matches = new HashSet<Match>();
        for (MatchInfo matchInfo : matchInfos) {
            matches.add(convertMatchInfoToMatch(matchInfo));
        }
        return matches;
    }

    public static OverInfo convertOverToOverInfo(Over over) {
        OverInfo overInfo = new OverInfo();
        overInfo.setId(over.getId());
        overInfo.setIsMaiden(over.getIsMaiden());
        overInfo.setOverNo(over.getOverNo());
        overInfo.setRuns(over.getRuns());
        overInfo.setWicketsCount(over.getWicketsCount());
        overInfo.setFoursCount(over.getFoursCount());
        overInfo.setSixersCount(over.getSixersCount());
        overInfo.setWidesCount(over.getWidesCount());
        overInfo.setByesCount(over.getByesCount());
        overInfo.setLegByesCount(over.getLegByesCount());
        overInfo.setTotalExtras(over.getTotalExtras());
        return overInfo;
    }

    public static String getBallOutcome(int randomNumber) {
        Map<Integer,String> ballOutcome = new HashMap<Integer,String>();
        ballOutcome.put(1,"1");
        ballOutcome.put(2,"2");
        ballOutcome.put(3,"3");
        ballOutcome.put(4,"4");
        ballOutcome.put(5,"5");
        ballOutcome.put(6,"6");
        ballOutcome.put(7,"wd");
        ballOutcome.put(8,"nb");
        ballOutcome.put(9,"B");
        ballOutcome.put(10,"lb");
        ballOutcome.put(11,"W");
        ballOutcome.put(13,"0");
        ballOutcome.put(14,"1");
        ballOutcome.put(15,"0");
        ballOutcome.put(16,"1");
        ballOutcome.put(17,"0");
        ballOutcome.put(18,"4");
        ballOutcome.put(19,"6");
        ballOutcome.put(20,"W");
        ballOutcome.put(21,"4");
        ballOutcome.put(22,"6");
        ballOutcome.put(23,"1");
        ballOutcome.put(24,"0");
        ballOutcome.put(25,"2");
        return ballOutcome.get(randomNumber);
    }
}
