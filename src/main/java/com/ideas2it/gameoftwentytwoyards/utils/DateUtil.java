package com.ideas2it.gameoftwentytwoyards.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Parses the given date.
 */
public class DateUtil {
    /**
     * Parses the date from string into Date type.
     *
     * @param dateAsString of String, Date which is to parsed
     * @return date as Date, parsed date.
     */
    public static Date parseDate(String dateAsString) {
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = dateformat.parse(dateAsString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
