package com.ideas2it.gameoftwentytwoyards.controller;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;  
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.info.TeamInfo;
import com.ideas2it.gameoftwentytwoyards.info.TeamPaginationInfo;
import com.ideas2it.gameoftwentytwoyards.service.TeamService;
import com.ideas2it.gameoftwentytwoyards.common.Constants;

/**
 * Deals with the interaction between the user such as collecting the required
 * data for the operation and views the manipulated data to the user using
 * jsp.
 */
@Controller
public class TeamController {
    private TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    static final Logger logger = Logger.getLogger(TeamController.class);

    /**
     * Creates a TeamInfo object, adds to the model attribute. This object
     * is sent to the front end where the attributes are mapped into 
     * this object.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     *
     * @return CreateTeam of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping("/addTeam")
    private String addTeam(Model model) {
            TeamInfo teamInfo = new TeamInfo();
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
        return "CreateTeam";
    }

    /**
     * Receives the model object, creates the team, and fetches the players
     * with status as true and team id as Null forwards it for team player
     * selection page.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param teamInfo of TeamInfo, It is the object where the attributes of the
     * is present and should be inserted into the database.
     *
     * @return FetchPlayersForTeam of String, It is JSP page name which has to
     * be redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping(value="/createTeam",method = RequestMethod.POST)
    public String createTeam(Model model,
            @ModelAttribute(Constants.TEAM_INFO) TeamInfo teamInfo) {
        try {
            TeamPaginationInfo teamPaginationInfo =
                    teamService.createTeam(teamInfo);
            model.addAttribute("teamInfo", teamPaginationInfo.getTeamInfo());
            model.addAttribute("players", teamPaginationInfo.getPlayerInfos());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "FetchPlayersForTeam";
    }

    /**
     * Receives the attributes of player from the user, then the attributes
     * get mapped into the object and finally will be updated into the 
     * database. It also gets the updated object and views it.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param playerIds of String[], it is the ids of the player choosen by the
     * user for which players has to be fetched and set into the team object and 
     * updated.
     * @param teamInfo of TeamInfo, It is the object where the attributes of the
     * is present and should be updated.
     *
     * @return ViewTeam of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping(value = "/saveTeam", method = RequestMethod.POST)
    public String saveTeam(Model model,
            @RequestParam(required = false, name = "playerIds")
            String[] playerIds, @ModelAttribute("teamInfo") TeamInfo teamInfo) {
        try {
            teamInfo = teamService.updateTeamWithPlayers(playerIds, teamInfo);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewTeam";
    }

    /**
     * Receives the id of the team, fetches the corresponding object and
     * views it.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param int of id, it is the id based on which the team is retrieved.
     *
     * @return ViewTeam of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping(value="/viewTeam",method = RequestMethod.POST)
    public String editPlayer(@RequestParam("id") int id,Model model) {
        try {
            TeamInfo teamInfo = teamService.retrieveTeam(id);
            model.addAttribute(Constants.TEAM_INFO,teamInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewTeam";
    }

    /**
     * Fetches the first five teams with status as true in the database, and
     * generates the pages with respect to the total number of teams with status
     * as true.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     *
     * @return ViewTeams of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping("/viewTeams")    
    public String viewTeams(Model model){
        try {
            TeamPaginationInfo teamPaginationInfo =
                    teamService.fetchPagesWithTeams();
            model.addAttribute("teams",teamPaginationInfo.getTeamInfos());
            model.addAttribute("pages",teamPaginationInfo.getPages());
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewTeams";    
    }

    /**
     * Receives the id of the team which is to be edited and fetch the team
     * based on that id and views it for edit.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param id of int, it is the id of the team which is going to be edited.
     *
     * @return UpdateTeam of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page. 
     */
    @RequestMapping(value="/editTeam",method = RequestMethod.POST)
    public String editTeam(Model model, @RequestParam("id") int id){
        try {
            TeamPaginationInfo teamPaginationInfo =
                    teamService.fetchTeamAndPlayersByCountry(id);
            model.addAttribute(Constants.TEAM_INFO,
                    teamPaginationInfo.getTeamInfo());
            model.addAttribute("players", teamPaginationInfo.getPlayerInfos());
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "UpdateTeam";    
    }

    /**
     * Receives the details of the team as info object, fetches the players with
     * respect to the player ids received and updates the team.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param playerIds of String[], it is the ids of the player choosen by the
     * user for which players has to be fetched and set into the team object and
     * updated.
     * @param teamInfo TeamInfo, It is the object where the updated attributes
     * of the team is present and should be updated.
     *
     * @return ViewTeam of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page
     */
    @RequestMapping("/updateTeam")    
    public String editTeam(Model model,
            @RequestParam(required = false , name = "playerIds")
            String[] playerIds, @ModelAttribute("teamInfo") TeamInfo teamInfo) {
        try {
            teamInfo = teamService.updateTeamWithPlayers(playerIds, teamInfo);
            model.addAttribute(Constants.TEAM_INFO, teamInfo);
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewTeam";    
    }

    /**
     * Hard Deletes the team from the database based on the id.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param id of int, it is the id of the team which has to be deleted.
     *
     * @return ViewDeleteMessage of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page. 
     */
    @RequestMapping(value="/deleteTeam",method = RequestMethod.POST)
    public String deleteTeam(Model model, @RequestParam("id") int id) {
        try {
            teamService.deleteTeam(id);
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewDeleteMessage";    
    }

    /**
     * Retrieves the teams(5 teams) with respect to the page number entered
     * by the user, converts them into a JSON Array and returns as a response.
     * This function called by the AJAX method.
     *
     * @param page of int, it is the page number for which the corresponding 5
     * teams has to be fetched.
     * @param response of HttpServletResponse, it is the response object to
     * which the JSON array is binded and sent.
     */
    @RequestMapping(value="/viewAllTeams",
         method=RequestMethod.GET, produces="application/json")
    private void viewAllTeams(@RequestParam("page") int page,
            HttpServletResponse response) {
        try {
            JSONArray array = teamService.paginateAndFetchAsJson(page);
            response.setContentType("application/json");
            response.getWriter().write(array.toString());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
    }
}
