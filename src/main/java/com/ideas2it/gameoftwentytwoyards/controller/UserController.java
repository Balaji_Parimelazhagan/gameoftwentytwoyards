package com.ideas2it.gameoftwentytwoyards.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import com.ideas2it.gameoftwentytwoyards.common.Gender;
import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.info.UserInfo;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.exception.InvalidPasswordException;
import com.ideas2it.gameoftwentytwoyards.exception.EmailAlreadyExistException;
import com.ideas2it.gameoftwentytwoyards.exception.LoginFailsExceededException;
import com.ideas2it.gameoftwentytwoyards.exception.UserNotFoundException;
import com.ideas2it.gameoftwentytwoyards.service.UserService;

/**
 * Deals with the interaction between the userInfo such as collecting the required
 * data for the operation and views the manipulated data to the userInfo using
 * jsp.
 */
@Controller
public class UserController {
    private UserService userService;
    /**
     * Constructor initialises the objects that is used throughout the userInfo
     * dao layer. 
     */
    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    static final Logger logger = Logger.getLogger(TeamController.class);

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping("/signUp")
    private String signUpUser(Model model) {
            UserInfo userInfo = new UserInfo();
            model.addAttribute("userInfo", userInfo);
        return "UserSignUp";
    }

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping(value="/saveUser", method = RequestMethod.POST)
    public String saveUser(Model model, @ModelAttribute("userInfo") UserInfo userInfo) {
        String redirectedPage = null;
        try {
            userService.createUser(userInfo);
            redirectedPage = "index";
        } catch (EmailAlreadyExistException e) {
            model.addAttribute(Constants.MESSAGE, e.getMessage());
            redirectedPage = "UserSignUp";
            logger.error(e.getMessage());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return redirectedPage;
    }

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping(value="/login", method = RequestMethod.POST)
    public String loginUser(
            @RequestParam(Constants.USER_EMAIL_ID) String emailId,
            @RequestParam(Constants.PASSWORD) String password,
            Model model, HttpServletRequest request) {
        String redirectedPage = null;
        try {
            if(userService.validateUser(emailId, password)) {
                HttpSession session = request.getSession(Boolean.FALSE);
                session.setAttribute(Constants.USER_EMAIL_ID,
                        emailId);
                redirectedPage = "MainMenu";
            }
        } catch (UserNotFoundException | InvalidPasswordException
                | LoginFailsExceededException e) {
            model.addAttribute(Constants.MESSAGE, e.getMessage());
            System.out.println("reached ");
            redirectedPage = "index";
            logger.error(e.getMessage());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return redirectedPage;
    }

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping("/logOut")
    private String logOutUser(HttpServletRequest request) {
            HttpSession session = request.getSession(Boolean.FALSE);
            session.invalidate();
        return "index";
    }
}
