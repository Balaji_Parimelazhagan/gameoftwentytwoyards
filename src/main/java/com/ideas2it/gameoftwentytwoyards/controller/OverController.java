package com.ideas2it.gameoftwentytwoyards.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;  
import org.springframework.ui.Model;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.info.OverStartInfo;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.service.OverService;

/**
 * Deals with the interaction between the user such as collecting the required
 * data for the operation and views the manipulated data to the user using
 * jsp.
 */
@Controller
public class OverController {
    private OverService overService;

    @Autowired
    public OverController(OverService overService) {
        this.overService = overService;
    }

    static final Logger logger = Logger.getLogger(PlayerController.class);

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping("/startOver")
    private String startOver(Model model, @RequestParam("batsmen")
            String[] batsmanIds, @RequestParam("matchId") int matchId,
            @RequestParam("battingTeamName") String battingTeamName,
            @RequestParam("bowlingTeamName") String bowlingTeamName,
            @RequestParam("bowler") int bowlerId) {
        try {
            OverStartInfo overStartInfo = overService.startOver(matchId,
                    batsmanIds, bowlerId, battingTeamName, bowlingTeamName);
            model.addAttribute("overStartInfo", overStartInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
            return "Scoresheet";
    }

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping(value="/chooseBowler", method = RequestMethod.POST)
    public String chooseBowler(Model model,
            @ModelAttribute("overStartInfo") OverStartInfo overStartInfo) {
        try {
            overStartInfo = overService.chooseBowlerForNextOver(
                    overStartInfo);
            model.addAttribute("overStartInfo", overStartInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ChooseBowler";
    }

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping(value="/startNextOver", method = RequestMethod.POST)
    private String startNextOver(Model model, 
            @ModelAttribute("overStartInfo") OverStartInfo overStartInfo,
            @RequestParam("bowlerId") int bowlerId) {
        try {
            overStartInfo = overService.startNextOver(overStartInfo,
                    bowlerId);
            model.addAttribute("overStartInfo", overStartInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
            return "Scoresheet";
    }

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping(value="/chooseBatsman", method = RequestMethod.POST)
    public String chooseBatsman(Model model,
            @ModelAttribute("overStartInfo") OverStartInfo overStartInfo) {
        try {
            overStartInfo = overService.chooseBatsmanAfterWicket(overStartInfo);
            model.addAttribute("overStartInfo", overStartInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ChooseBatsman";
    }

    /**
     * Receives the attributes from the create jsp and passes it to service.
     *
     * @param request of HttpServletRequest, request from the server.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping(value="/resumeMatchAfterWicket", method = RequestMethod.POST)
    private String resumeMatchAfterWicket(Model model, 
            @ModelAttribute("overStartInfo") OverStartInfo overStartInfo,
            @RequestParam("batsmanId") int batsmanId) {
        try {
            overStartInfo = overService.fetchNewBatsman(overStartInfo,
                    batsmanId);
            model.addAttribute("overStartInfo", overStartInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
            return "Scoresheet";
    }

}
