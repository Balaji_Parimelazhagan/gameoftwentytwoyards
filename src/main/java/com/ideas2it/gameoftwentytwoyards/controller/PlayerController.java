package com.ideas2it.gameoftwentytwoyards.controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.multipart.commons.CommonsMultipartFile; 
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;  
import org.springframework.ui.Model;

import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.info.PlayerInfo;
import com.ideas2it.gameoftwentytwoyards.info.PlayerPaginationInfo;
import com.ideas2it.gameoftwentytwoyards.service.PlayerService;
import com.ideas2it.gameoftwentytwoyards.common.Constants;

/**
 * Deals with the interaction between the user such as collecting the required
 * data for the operation and views the manipulated data to the user using
 * jsp.
 */
@Controller
public class PlayerController {
    private PlayerService playerService;
    static final Logger logger = Logger.getLogger(PlayerController.class);

    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    /**
     * Creates a PlayerInfo object, adds to the model attribute. This object
     * is sent to the front end where the attributes are mapped into 
     * this object.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     *
     * @return CreatePlayer of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping("/createPlayer")
    private String createPlayer(Model model) {
            PlayerInfo playerInfo = new PlayerInfo();
            model.addAttribute(Constants.PLAYER_INFO, playerInfo);
        return "CreatePlayer";
    }

    /**
     * Receives the attributes of player from the user, then the attributes
     * get mapped into the object and finally will be updated into the 
     * database. It also gets the updated object and views it.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param CommonsMultipartFile of file, It is the object where the profile
     * pic of the player is present for which the path as to be has to inserted
     * into database.
     * @param playerInfo of PlayerInfo, It is the object where the attributes of
     * the player is present which has to inserted into database.
     *
     * @return ViewPlayer of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping(value="/savePlayer", method = RequestMethod.POST)
    public String savePlayer(@RequestParam CommonsMultipartFile file,
            Model model, @ModelAttribute(Constants.PLAYER_INFO)
            PlayerInfo playerInfo) {
        try {
            playerInfo = playerService.createPlayer(playerInfo, file);
            model.addAttribute(Constants.PLAYER_INFO,playerInfo);
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewPlayer";
    }

    /**
     * Fetches the first five players with status as true in the database,
     * and generates the pages with respect to the total number of player with
     * status as true.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     *
     * @return ViewMatches of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping("/viewPlayers")    
        public String viewPlayer(Model model){
       try {
            PlayerPaginationInfo playerPaginationInfo =
                    playerService.fetchPagesWithPlayers();
            model.addAttribute("players", playerPaginationInfo.getPlayerInfos());
            model.addAttribute("pages", playerPaginationInfo.getPages());
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewPlayers";    
    }

    /**
     * Soft Deletes the player from the database based on the id.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param id of int, it is the id of the player which has to be deleted.
     *
     * @return ViewDeleteMessage of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page. 
     */
    @RequestMapping(value="/deletePlayer",method = RequestMethod.POST)  
    public String deletePlayer(@RequestParam("id") int id,Model model) {
        try {
            playerService.deletePlayer(id);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewDeleteMessage";
    }

    /**
     * Receives the id of the player, fetches the corresponding object and
     * views it.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param int of id, it is the id based on which the player is retrieved.
     *
     * @return ViewPlayer of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping(value="/viewPlayer",method = RequestMethod.POST)  
    public String editPlayer(@RequestParam("id") int id,Model model) {
        try {
            PlayerInfo playerInfo = playerService.fetchPlayer(id);
            model.addAttribute(Constants.PLAYER_INFO,playerInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewPlayer";
    }

    /**
     * Receives the id of the player which is to be edited and fetches the player
     * based on that id and views it for edit.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param id of int, it is the id of the match which is going to be edited.
     *
     * @return UpdatePlayer of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page. 
     */
    @RequestMapping(value="/editPlayer",method = RequestMethod.POST)  
    public String delete(@RequestParam("id") int id,Model model) {
        try {
            PlayerInfo playerInfo = playerService.fetchPlayer(id);
            model.addAttribute(Constants.PLAYER_INFO,playerInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "UpdatePlayer";
    }

    /**
     * Receives the details of the match as info object, parse the
     * date, fetches the teams with respect to the team ids received and
     * updates thw match.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param CommonsMultipartFile of file, It is the object where the profile
     * pic of the player is present for which the path as to be has to inserted
     * into database.
     * @param playerInfo PlayerInfo, It is the object where the updated
     * attributes of the player is present and should be updated.
     *
     * @return ViewPlayer of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page
     */
    @RequestMapping(value="/updatePlayer",method = RequestMethod.POST)
    public String delete(@RequestParam CommonsMultipartFile file,
            Model model, @ModelAttribute("playerInfo") PlayerInfo playerInfo,
            @RequestParam("imagePath") String existingImagePath) {
        try {
            playerInfo = playerService.updatePlayerWithPic(
                                       playerInfo, file, existingImagePath);
            model.addAttribute(Constants.PLAYER_INFO,playerInfo);
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewPlayer";
    }

    /**
     * Retrieves the players(5 players) with respect to the page number entered
     * by the user, converts them into a JSON Array and returns as a response.
     * This function called by the AJAX method.
     *
     * @param page of int, it is the page number for which the corresponding 5
     * players has to be fetched.
     * @param response of HttpServletResponse, it is the response object to
     * which the JSON array is binded and sent.
     */
    @RequestMapping(value="/viewAllPlayers",
         method=RequestMethod.GET, produces="application/json")
    private void viewAllPlayers(@RequestParam("page") int page,
            HttpServletResponse response) {
        try {
            JSONArray array = playerService.paginateAndFetchAsJson(page);
            response.setContentType("application/json");
            response.getWriter().write(array.toString());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
    }
}
