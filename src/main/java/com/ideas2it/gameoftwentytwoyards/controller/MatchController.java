package com.ideas2it.gameoftwentytwoyards.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam; 
import org.springframework.ui.Model;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.info.MatchInfo;
import com.ideas2it.gameoftwentytwoyards.info.TeamInfo;
import com.ideas2it.gameoftwentytwoyards.info.MatchPaginationInfo;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.service.MatchService;

/**
 * Deals with the interaction between the user such as collecting the required
 * data for the operation and views the manipulated data to the user using
 * jsp.
 */
@Controller
public class MatchController {
    private MatchService matchService;

    @Autowired
    public MatchController(MatchService matchService) {
        this.matchService = matchService;
    }

    static final Logger logger = Logger.getLogger(PlayerController.class);

    /**
     * Creates a MatchInfo object, adds to the model attribute. This object
     * is sent to the front end where the attributes are mapped into 
     * this object.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     *
     * @return CreateMatch of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping("/addMatch")
    private String addMatch(Model model) {
            MatchInfo matchInfo = new MatchInfo();
            model.addAttribute(Constants.MATCH_INFO, matchInfo);
        return "CreateMatch";
    }

    /**
     * Receives the model object and date from the front end, creates the match,
     * and fetches the teams(which does not have match, on this match date)
     * for the match and forwards it for team selection page.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param matchInfo MatchInfo, It is the object where the attributes of the
     * is present and should be updated.
     * @param date of String, it is the date assigned for the  match which has,
     * converted into a Date object and set into the match object.
     *
     * @return FetchTeamsForMatch of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping(value="/createMatch",method = RequestMethod.POST)
    public String createMatch(Model model,
            @ModelAttribute(Constants.MATCH_INFO) MatchInfo matchInfo,
            @RequestParam("date") String date) {
        try {
            MatchPaginationInfo matchPaginationInfo =
                    matchService.createMatch(matchInfo, date);
            model.addAttribute("matchInfo", matchPaginationInfo.getMatchInfo());
            model.addAttribute("teams", matchPaginationInfo.getTeamInfos());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "FetchTeamsForMatch";
    }

    /**
     * Receives the attributes of match from the user, then the attributes
     * get mapped into the object and finally will be updated into the 
     * database. It also gets the updated object and views it.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param date of String, it is the date assigned for the  match which has,
     * converted into a Date object and set into the match object.
     * @param teamIds of String[], it is the ids of the team choose by the user
     * for which teams has to be fetched and set into the match object and 
     * updated.
     *
     * @return ViewMatch of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping(value="/saveMatch",method = RequestMethod.POST)
    public String saveMatch(Model model, @RequestParam("date") String date,
            @RequestParam(name = "teamIds", required = false) String[] teamIds,
            @ModelAttribute(Constants.MATCH_INFO) MatchInfo matchInfo) {
        try {
            matchInfo = matchService.updateMatch(teamIds, matchInfo, date);
            model.addAttribute(Constants.MATCH_INFO, matchInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewMatch";
    }

    /**
     * Fetches the first five matches in the database, and generates the pages
     * with respect to the total number of matches.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     *
     * @return ViewMatches of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping("/viewMatches")    
    public String viewMatches(Model model){
        try {
            MatchPaginationInfo matchPaginationInfo =
                    matchService.fetchPagesWithMatches();
            model.addAttribute("matches", matchPaginationInfo.getMatchInfos());
            model.addAttribute("pages", matchPaginationInfo.getPages());
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewMatches";    
    }

    /**
     * Receives the id of the match which is to be edited and fetch the match
     * based on that id and views it for edit.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param id of int, it is the id of the match which is going to be edited.
     *
     * @return UpdateMatch of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page. 
     */
    @RequestMapping(value="/editMatch",method = RequestMethod.POST)  
    public String editMatch(Model model, @RequestParam("id") int id){
        try {
            MatchPaginationInfo matchPaginationInfo =
                    matchService.filterTeamsByDate(id);
            model.addAttribute(Constants.MATCH_INFO,
                    matchPaginationInfo.getMatchInfo());
            model.addAttribute("teams", matchPaginationInfo.getTeamInfos());
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "UpdateMatch";    
    }

    /**
     * Receives the details of the match as info object, parse the
     * date, fetches the teams with respect to the team ids received and
     * updates thw match.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param date of String, it is the date assigned for the  match which has,
     * converted into a Date object and set into the match object.
     * @param teamIds of String[], it is the ids of the team choose by the user
     * for which teams has to be fetched and set into the match object and 
     * updated.
     * @param matchInfo MatchInfo, It is the object where the updated attributes
     * of the match is present and should be updated.
     *
     * @return ViewMatch of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page
     */
    @RequestMapping(value="/updateMatch", method = RequestMethod.POST)
    public String updateMatch(Model model, @RequestParam("date") String date,
            @RequestParam(required = false , name = "teamIds") String[] teamIds,
            @ModelAttribute("matchInfo") MatchInfo matchInfo) {
        try {
            matchInfo = matchService.updateMatch(teamIds, matchInfo, date);
            model.addAttribute("match", matchInfo);
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewMatch";    
    }

    /**
     * Receives the id of the match, fetches the corresponding object and
     * views it.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param int of id, it is the id based on which the match is retrieved.
     *
     * @return ViewMatch of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page.
     */
    @RequestMapping("/viewMatch")  
    public String viewMatch(@RequestParam("id") int id,Model model) {
        try {
            MatchInfo matchInfo = matchService.retrieveMatch(id);
            model.addAttribute("match", matchInfo);
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewMatch";
    }

    /**
     * Hard Deletes the match from the database based on the id.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param id of int, it is the id of the match which has to be deleted.
     *
     * @return ViewDeleteMessage of String, It is JSP page name which has to be
     * redirected, This will reach spring.xml file and will be redirected to
     * the jsp page. 
     */
    @RequestMapping(value="/deleteMatch",method = RequestMethod.POST)  
    public String deleteMatch(Model model, @RequestParam("id") int id) {
        try {
            matchService.deleteMatch(id);
         } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "ViewDeleteMessage";    
    }

    /**
     * Retrieves the matches(5 matches) with respect to the page number entered
     * by the user, converts them into a JSON Array and returns as a response.
     * This function called by the AJAX method.
     *
     * @param page of int, it is the page number for which the corresponding 5
     * matches has to be fetched.
     * @param response of HttpServletResponse, it is the response object to
     * which the JSON array is binded and sent.
     */
    @RequestMapping(value="/viewAllMatches",
         method=RequestMethod.GET, produces="application/json")
    private void viewAllMatches(@RequestParam("page") int page,
            HttpServletResponse response) {
        try {
            JSONArray array = matchService.paginateAndFetchAsJson(page);
            response.setContentType("application/json");
            response.getWriter().write(array.toString());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Fetches the match for play, with respect to the id, This match will
     * contain two teams and players of the teams in it.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param id of int, it is the id of the match which is going to be played.
     *
     * @return PlayerSelectionForMatch of String, It is JSP page name which has
     * to be redirected, This will reach spring.xml file and will be redirected
     * to the jsp page.
     */
    @RequestMapping("/playMatch")  
    public String fetchMatchForPlay(@RequestParam("id") int id,Model model) {
        try {
            MatchPaginationInfo matchPaginationInfo =
                    matchService.fetchTeamsForSelection(id);
            TeamInfo battingTeam = matchPaginationInfo.getBattingTeam();
            model.addAttribute("match", matchPaginationInfo.getMatchInfo());
            model.addAttribute("battingTeam",
                    matchPaginationInfo.getBattingTeam());
            model.addAttribute("bowlingTeam",
                    matchPaginationInfo.getBowlingTeam());
        } catch (GameOfTwentyTwoYardsException e) {
            logger.error(e.getMessage());
        }
        return "PlayerSelectionForMatch";
    }


}
