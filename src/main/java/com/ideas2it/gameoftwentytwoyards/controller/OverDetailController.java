package com.ideas2it.gameoftwentytwoyards.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.ModelAttribute; 
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.service.OverDetailService;

/**
 * Deals with the interaction between the user such as collecting the required
 * data for the operation and views the manipulated data to the user using
 * jsp.
 */
@Controller
public class OverDetailController {
    private OverDetailService overDetailService;

    @Autowired
    public OverDetailController(OverDetailService overDetailService) {
        this.overDetailService = overDetailService;
    }

    static final Logger logger = Logger.getLogger(OverDetailController.class);

    /**
     * Generates the runs, assigns the runs for the corresponding batsman,
     * changes the strike when a odd number of runs is scored.
     *
     * @param model of Model, it is the model instance where the attributes
     * are going to be mapped.
     * @param reponse of HttpServletResponse, reponse to the server.
     */
    @RequestMapping(value="/bowlNextDelivery",
         method=RequestMethod.GET, produces="application/json")
    private void bowlNextDelivery(Model model,
            @RequestParam("firstBatsmanId") int firstBatsmanId,
            @RequestParam("secondBatsmanId") int secondBatsmanId,
            @RequestParam("bowlerId") int bowlerId, @RequestParam("overId")
            int overId, @RequestParam("strikerId") int strikerId,
            @RequestParam("matchId") int matchId,
            @RequestParam("ballNumber") int ballNumber,
            HttpServletResponse response) {
        try {
            JSONObject scoreSheetJSON =
                    overDetailService.generateBallAndSaveDetail(firstBatsmanId,
                    secondBatsmanId, bowlerId, overId, strikerId, matchId,
                    ballNumber);
            response.setContentType("application/json");
            response.getWriter().write(scoreSheetJSON.toString());
        } catch (GameOfTwentyTwoYardsException | IOException e) {
            logger.error(e.getMessage());
        }
    }
}
