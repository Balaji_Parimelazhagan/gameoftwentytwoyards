package com.ideas2it.gameoftwentytwoyards.exception;

/**
 * Contains the catches the exception and .
 */
public class TeamNotFoundException extends RuntimeException {
    public TeamNotFoundException(String exceptionPhrase) {
        super(exceptionPhrase);
    }
}
