package com.ideas2it.gameoftwentytwoyards.exception;

/**
 * Contains the catches the exception and .
 */
public class MatchNotFoundException extends RuntimeException {
    public MatchNotFoundException(String exceptionPhrase) {
        super(exceptionPhrase);
    }
}
