package com.ideas2it.gameoftwentytwoyards.exception;

/**
 * Catches the exception Whenever user password doesn't match.
 */
public class LoginFailsExceededException extends RuntimeException {
    public LoginFailsExceededException(String exceptionPhrase) {
        super(exceptionPhrase);
    }
}
