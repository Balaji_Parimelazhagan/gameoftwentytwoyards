package com.ideas2it.gameoftwentytwoyards.exception;

/**
 * Contains the catches the exception and .
 */
public class PlayerNotFoundException extends RuntimeException {
    public PlayerNotFoundException(String exceptionPhrase) {
        super(exceptionPhrase);
    }
}
