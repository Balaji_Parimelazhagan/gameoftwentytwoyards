package com.ideas2it.gameoftwentytwoyards.exception;

/**
 * Catches the exception Whenever user password doesn't match.
 */
public class InvalidPasswordException extends RuntimeException {
    public InvalidPasswordException(String exceptionPhrase) {
        super(exceptionPhrase);
    }
}
