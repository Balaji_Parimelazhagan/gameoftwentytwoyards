package com.ideas2it.gameoftwentytwoyards.exception;

/**
 * Catches the exception Whenever user email id doesnt exists.
 */
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String exceptionPhrase) {
        super(exceptionPhrase);
    }
}
