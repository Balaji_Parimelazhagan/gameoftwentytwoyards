package com.ideas2it.gameoftwentytwoyards.exception;

/**
 * Catches the exception Whenever user email id doesnt exists.
 */
public class EmailAlreadyExistException extends RuntimeException {
    public EmailAlreadyExistException(String exceptionPhrase) {
        super(exceptionPhrase);
    }
}
