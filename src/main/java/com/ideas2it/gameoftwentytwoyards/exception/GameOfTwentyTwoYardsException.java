package com.ideas2it.gameoftwentytwoyards.exception;

/**
 * Contains the exceptions of the project.
 */
public class GameOfTwentyTwoYardsException extends Exception {
    public GameOfTwentyTwoYardsException(
                 String exceptionPhrase, Throwable exception) {
        super(exceptionPhrase, exception);
    }
}
