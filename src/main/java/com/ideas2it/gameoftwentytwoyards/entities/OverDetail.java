package com.ideas2it.gameoftwentytwoyards.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;

/**
 * Player is a pojo class that performs getter and setter methods to get and
 * set methods respectively.
 *
 */
@Entity 
@Table(name= "over_detail")
public class OverDetail {
	@Column(name = "is_byes")
    private boolean isByes;
	@Column(name = "is_leg_byes")
    private boolean isLegByes;
	@Column(name = "is_no_ball")
    private boolean isNoBall;
	@Column(name = "is_wicket")
    private boolean isWicket;
	@Column(name = "is_wide")
    private boolean isWide;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
	@Column(name = "ball_number")
    private int ballNumber;
	@Column(name = "runs")
    private int runs;
    @ManyToOne
	@JoinColumn(name="batsman_id")
    private Player batsman;
    @ManyToOne
	@JoinColumn(name="bowler_id")
    private Player bowler;
    @ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "over_id")
    private Over over;
	@Column(name = "match_id")
    private int matchId;

    public boolean getIsByes() {
        return isByes;
    }

    public boolean getIsLegByes() {
        return isLegByes;
    }

    public boolean getIsNoBall() {
        return isNoBall;
    }

    public boolean getIsWicket() {
        return isWicket;
    }

    public boolean getIsWide() {
        return isWide;
    }

    public int getId() {
        return id;
    }

    public int getBallNumber() {
        return ballNumber;
    }

    public int getRuns() {
        return runs;
    }

    public Player getBatsman() {
        return batsman;
    }

    public Player getBowler() {
        return bowler;
    }

    public Over getOver() {
        return over;
    }

    public int getMatchId() {
        return matchId;
    }

    //****************SETTERS***************************

    public void setIsByes(boolean isByes) {
        this.isByes = isByes;
    }

    public void setIsLegByes(boolean isLegByes) {
        this.isLegByes = isLegByes;
    }

    public void setIsNoBall(boolean isNoBall) {
        this.isNoBall = isNoBall;
    }

    public void setIsWicket(boolean isWicket) {
        this.isWicket = isWicket;
    }

    public void setIsWide(boolean isWide) {
        this.isWide = isWide;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public void setBatsman(Player batsman) {
        this.batsman = batsman;
    }

    public void setBowler(Player bowler) {
        this.bowler = bowler;
    }

    public void setOver(Over over) {
        this.over = over;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    
    /*public boolean equals(Object object) {
        Player player = (Player) object;
        if (this == object || this.id == player.getId()) {
            return Boolean.TRUE;
        } else if (!(object instanceof Player)) { 
            return Boolean.FALSE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public String toString() {
        String status = "";
        if (this.playerStatus) {
           status = "ACTIVE";
        } else {
            status = "INACTIVE";
        }
        StringBuilder playerDetails = new StringBuilder("");
        playerDetails.append("\n*****************************")
        .append("\n***GAME OF 22 YARDS***")
        .append("\nPLAYER ID:").append(this.id)
        .append("    NAME:").append(this.playerName)
        .append("   AGE:").append(this.playerAge)
        .append("    COUNTRY NAME:").append(this.countryName)
        .append("\nPLAYER STATUS:").append(status)
        .append("    ROLE:").append(this.playerRole)
        .append("    BATTING TYPE:").append(this.battingStyle)
        .append("    BOWLING TYPE:").append(this.bowlingStyle)
        .append(contact.toString())
        .append("\n*****************************");
        return playerDetails.toString();
    }*/
}
