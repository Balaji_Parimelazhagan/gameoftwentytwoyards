package com.ideas2it.gameoftwentytwoyards.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ideas2it.gameoftwentytwoyards.common.Country;

/**
 * Contains the Details of the team.
 */ 
@Entity 
@Table(name= "team")
public class Team {
	@Column(name = "status")
    private boolean teamStatus;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
	@Column(name = "country")
    private Country country;

	@Column(name = "name")
    private String teamName;

    @ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "team_match", joinColumns = { 
			@JoinColumn(name = "team_id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "match_id", 
					nullable = false, updatable = false) })
    private Set<Match> matches = new HashSet<Match>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "team_id")
    private Set<Player> playersInTeam = new HashSet<Player>();

    public Team() {
    }

    public boolean getTeamStatus() {
        return teamStatus;
    }

    public int getId() {
        return id ;
    }

    public Country getCountry() {
        return country;
    }

    public String getTeamName() {
        return teamName;
    }

    public Set<Match> getMatches() {
        return matches;
    }

    public Set<Player> getPlayersInTeam() {
        return playersInTeam;
    }

    public void setTeamStatus(boolean teamStatus) {
        this.teamStatus = teamStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void setMatches(Set<Match> matches) {
        this.matches = matches;
    }

    public void setPlayersInTeam(Set<Player> playersInTeam) {
        this.playersInTeam = playersInTeam;
    }

    public boolean equals(Object object) {
        Team team = (Team) object;
        if (this == object || team.getId() == this.id) {
            return Boolean.TRUE;
        } else if (!(object instanceof Team)) { 
            return Boolean.FALSE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public String toString() {
        String status = "";
        if (this.teamStatus) {
           status = "COMPLETE";
        } else {
            status = "INCOMPLETE";
        }
        StringBuilder teamDetails = new StringBuilder("");
        teamDetails.append("\n*****************************")
        .append("\n******* TEAM ******")
        .append("\nTEAM ID:").append(this.id)
        .append("    NAME:").append(this.teamName)
        .append("    COUNTRY:").append(this.country)
        .append("    STATUS:").append(status)
        .append("\n*****************************");
        return teamDetails.toString();
    }

}
