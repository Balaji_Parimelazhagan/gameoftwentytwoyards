package com.ideas2it.gameoftwentytwoyards.entities;

import java.util.Set;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.ideas2it.gameoftwentytwoyards.common.MatchType;

/**
 * Match is a pojo class contais the details of a single match.
 */
@Entity 
@Table(name= "matches")
public class Match {
    
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

	@Column(name = "date")
    private Date matchDate;

	@Enumerated(EnumType.STRING)
	@Column(name = "match_type")
    private MatchType matchType;

	@Column(name = "location")
    private String matchLocation;

	@Column(name = "name")
    private String matchName;

    @ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "team_match", joinColumns = { 
			@JoinColumn(name = "match_id", nullable = false, updatable = false) }, 
			inverseJoinColumns = { @JoinColumn(name = "team_id", 
					nullable = false, updatable = false) })
    private Set<Team> teamsForMatch;

    /**
     * Getters and Setters to fetch and insert properties.
     */

    public int getId() {
        return id;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public MatchType getMatchType() {
        return matchType;
    }

    public String getMatchLocation() {
        return matchLocation;
    }

    public String getMatchName() {
        return matchName;
    }

    public Set<Team> getTeamsForMatch() {
        return teamsForMatch;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMatchDate(Date matchDate) {
        this.matchDate = matchDate;
    }

    public void setMatchType(MatchType matchType) {
        this.matchType = matchType;
    }

    public void setMatchLocation(String matchLocation) {
        this.matchLocation = matchLocation;
    }

    public void setMatchName(String matchName) {
        this.matchName = matchName;
    }

    public void setTeamsForMatch(Set<Team> teamsForMatch) {
        this.teamsForMatch = teamsForMatch;
    }

    public boolean equals(Object object) {
        Match match = (Match) object;
        if (this == object || match.getId() == this.id) {
            return Boolean.TRUE;
        } else if (!(object instanceof Match)) { 
            return Boolean.FALSE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public String toString() {
        StringBuilder matchDetails = new StringBuilder("");
        matchDetails.append("\n*****************************")
        .append("\n***GAME OF 22 YARDS***")
        .append("\nMATCH ID:").append(this.id)
        .append("\nMATCH NAME:").append(this.matchName)
        .append("\nMATCH LOCATION:").append(this.matchLocation)
        .append("\nMATCH TYPE:").append(this.matchType)
        .append("\n*****************************");
        return matchDetails.toString();
    }
}
