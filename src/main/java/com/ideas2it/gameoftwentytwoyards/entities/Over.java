package com.ideas2it.gameoftwentytwoyards.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.FetchType;

/**
 * Player is a pojo class that performs getter and setter methods to get and
 * set methods respectively.
 *
 */
@Entity 
@Table(name= "over")
public class Over {
	@Column(name = "is_maiden")
    private boolean isMaiden;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

	@Column(name = "over_no")
    private int overNo;

	@Column(name = "runs")
    private int runs;

	@Column(name = "wickets_count")
    private int wicketsCount;

	@Column(name = "fours_count")
    private int foursCount;

	@Column(name = "sixers_count")
    private int sixersCount;

	@Column(name = "wides_count")
    private int widesCount;

	@Column(name = "byes_count")
    private int byesCount;

	@Column(name = "leg_byes_count")
    private int legByesCount;

	@Column(name = "total_extras")
    private int totalExtras;

	@Column(name = "match_id")
    private int matchId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "over_id")
    private Set<OverDetail> overDetails;

    public boolean getIsMaiden() {
        return isMaiden;
    }

    public int getOverNo() {
        return overNo;
    }

    public int getId() {
        return id;
    }

    public int getRuns() {
        return runs;
    }

    public int getWicketsCount() {
        return wicketsCount;
    }

    public int getFoursCount() {
        return foursCount;
    }

    public int getSixersCount() {
        return sixersCount;
    }

    public int getWidesCount() {
        return widesCount;
    }

    public int getByesCount() {
        return byesCount;
    }

    public int getLegByesCount() {
        return legByesCount;
    }

    public int getTotalExtras() {
        return totalExtras;
    }

    public int getMatchId() {
        return matchId;
    }

    public Set<OverDetail> getOverDetails() {
        return overDetails;
    }


    //****************SETTERS***************************

    public void setIsMaiden(boolean isMaiden) {
        this.isMaiden = isMaiden;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOverNo(int overNo) {
        this.overNo = overNo;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public void setWicketsCount(int wicketsCount) {
        this.wicketsCount = wicketsCount;
    }

    public void setFoursCount(int foursCount) {
        this.foursCount = foursCount;
    }

    public void setSixersCount(int sixersCount) {
        this.sixersCount = sixersCount;
    }

    public void setWidesCount(int widesCount) {
        this.widesCount = widesCount;
    }

    public void setByesCount(int byesCount) {
        this.byesCount = byesCount;
    }

    public void setLegByesCount(int legByesCount) {
        this.legByesCount = legByesCount;
    }

    public void setTotalExtras(int totalExtras) {
        this.totalExtras = totalExtras;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public void setOverDetails(Set<OverDetail> overDetails) {
        this.overDetails = overDetails;
    }
    
    /*public boolean equals(Object object) {
        Player player = (Player) object;
        if (this == object || this.id == player.getId()) {
            return Boolean.TRUE;
        } else if (!(object instanceof Player)) { 
            return Boolean.FALSE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public String toString() {
        String status = "";
        if (this.playerStatus) {
           status = "ACTIVE";
        } else {
            status = "INACTIVE";
        }
        StringBuilder playerDetails = new StringBuilder("");
        playerDetails.append("\n*****************************")
        .append("\n***GAME OF 22 YARDS***")
        .append("\nPLAYER ID:").append(this.id)
        .append("    NAME:").append(this.playerName)
        .append("   AGE:").append(this.playerAge)
        .append("    COUNTRY NAME:").append(this.countryName)
        .append("\nPLAYER STATUS:").append(status)
        .append("    ROLE:").append(this.playerRole)
        .append("    BATTING TYPE:").append(this.battingStyle)
        .append("    BOWLING TYPE:").append(this.bowlingStyle)
        .append(contact.toString())
        .append("\n*****************************");
        return playerDetails.toString();
    }*/
}


