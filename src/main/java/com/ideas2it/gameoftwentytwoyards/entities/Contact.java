package com.ideas2it.gameoftwentytwoyards.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;

import com.ideas2it.gameoftwentytwoyards.entities.Player;

/**
 * Holds the contact details of the player
 */
@Entity 
@Table(name= "contact")
public class Contact {

	@Column(name = "address")
    private String address;

	@Column(name = "city")
    private String city;

	@Column(name = "contact_number")
    private String contactNumber;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
	@JoinColumn(name="player_id")
    private Player player;

    /**
     * Getters and Setters of storing and retriving the data of contact
     * information.
     */
    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public int getId() {
        return id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean equals(Object object) {
        Contact contact = (Contact) object;
        if (this == object) {
            return Boolean.TRUE;
        } else if (!(object instanceof Contact)) { 
            return Boolean.FALSE;
        } else if (this.id == contact.getId()) {
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }


    public String toString() {
        StringBuilder contactDetails = new StringBuilder("");
        contactDetails.append("\nADDRESS:").append(this.address)
        .append("    CITY:").append(this.city)
        .append("\nCONTACT NUMBER:").append(this.contactNumber);
        return contactDetails.toString();
    }
}
