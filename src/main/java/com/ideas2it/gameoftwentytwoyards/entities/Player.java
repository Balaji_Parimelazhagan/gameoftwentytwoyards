package com.ideas2it.gameoftwentytwoyards.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.FetchType;

import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.entities.Contact;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.utils.AgeUtil;

/**
 * Player is a pojo class that performs getter and setter methods to get and
 * set methods respectively.
 *
 */
@Entity 
@Table(name= "player")
public class Player {
	@Column(name = "status")
    private boolean playerStatus;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

	@Enumerated(EnumType.STRING)
	@Column(name = "country")
    private Country countryName;

	@Column(name = "batting_style")
    private String battingStyle;

	@Column(name = "bowling_style")
    private String bowlingStyle;

	@Column(name = "DOB")
    private String DOB;

	@Column(name = "image_path")
    private String imagePath;

	@Column(name = "pace_type")
    private String paceType;

	@Column(name = "name")
    private String playerName;

	@Column(name = "role")
    private String playerRole;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "team_id")
    private Team team;

    @OneToOne(mappedBy="player", cascade=CascadeType.ALL)
    private Contact contact = new Contact();

    public Player() {
        this.playerStatus = Boolean.TRUE;
    }
    /**
     * Getters and Setters of the player pojo class.
     */
    public boolean getPlayerStatus() {
        
        return playerStatus;
    }

    public int getId() {
        return id;
    }

    public Country getCountryName() {
        return countryName;
    }

    public String getBattingStyle() {
        return battingStyle;
    }

    public String getBowlingStyle() {
        return bowlingStyle;
    }

    public String getDOB() {
        return DOB;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getPaceType() {
        return paceType;
    }

    public String getPlayerAge() {
        return AgeUtil.fetchPlayerAge(this.DOB);
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getPlayerRole() {
        return playerRole;
    }

    public Team getTeam() {
        return team;
    }

    public Contact getContact() {
        return this. contact;
    }

    public void setPlayerStatus(boolean playerStatus) {
        this.playerStatus = playerStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCountryName(Country countryName) {
        this.countryName = countryName;
    }

    public void setBattingStyle(String battingStyle) {
        this.battingStyle = battingStyle;
    }

    public void setBowlingStyle(String bowlingStyle) {
        this.bowlingStyle = bowlingStyle;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setPaceType(String paceType) {
        this.paceType = paceType;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setPlayerRole(String playerRole) {
        this.playerRole = playerRole;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public boolean equals(Object object) {
        Player player = (Player) object;
        if (this == object || this.id == player.getId()) {
            return Boolean.TRUE;
        } else if (!(object instanceof Player)) { 
            return Boolean.FALSE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public String toString() {
        String status = "";
        if (this.playerStatus) {
           status = "ACTIVE";
        } else {
            status = "INACTIVE";
        }
        StringBuilder playerDetails = new StringBuilder("");
        playerDetails.append("\n*****************************")
        .append("\n***GAME OF 22 YARDS***")
        .append("\nPLAYER ID:").append(this.id)
        .append("    NAME:").append(this.playerName)
        .append("    COUNTRY NAME:").append(this.countryName)
        .append("\nPLAYER STATUS:").append(status)
        .append("    ROLE:").append(this.playerRole)
        .append("    BATTING TYPE:").append(this.battingStyle)
        .append("    BOWLING TYPE:").append(this.bowlingStyle)
        .append(contact.toString())
        .append("\n*****************************");
        return playerDetails.toString();
    }
}


