package com.ideas2it.gameoftwentytwoyards.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ideas2it.gameoftwentytwoyards.common.Gender;
import com.ideas2it.gameoftwentytwoyards.common.UserRole;

/**
 * Player is a pojo class that performs getter and setter methods to get and
 * set methods respectively.
 *
 */
@Entity 
@Table(name= "user")
public class User {
	@Column(name = "status")
    private boolean userStatus;

	@Id
    private int id;

	@Column(name = "login_fails")
    private int loginFailAttempts;

	@Enumerated(EnumType.STRING)
	@Column(name = "gender")
    private Gender userGender;

	@Enumerated(EnumType.STRING)
	@Column(name = "role")
    private UserRole userRole;

	@Column(name = "name")
    private String userName;

	@Column(name = "email_id", unique = true)
    private String userEmailId;

	@Column(name = "password")
    private String password;

    /**
     * Getters and Setters of the player pojo class.
     */
    public boolean getUserStatus() {
        return userStatus;
    }

    public int getId() {
        return id;
    }

    public int getLoginFailAttempts() {
        return loginFailAttempts;
    }

    public Gender getUserGender() {
        return userGender;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public String getPassword() {
        return password;
    }

    public void setUserStatus(boolean userStatus) {
        this.userStatus = userStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLoginFailAttempts(int loginFailAttempts) {
        this.loginFailAttempts = loginFailAttempts;
    }

    public void setUserGender(Gender userGender) {
        this.userGender = userGender;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}


