package com.ideas2it.gameoftwentytwoyards.info;

/**
 * PlayerInfo is a pojo class that performs getter and setter methods to get and
 * set methods respectively.
 *
 */
public class OverStartInfo {
    private int ballNumber;
    private int overNo;
    private int strikerId;
    private int totalRuns;
    private int firstBatsmanRuns;
    private int firstBatsmanFoursCount;
    private int firstBatsmanSixersCount;
    private int firstBatsmanBallsCount;
    private int secondBatsmanRuns;
    private int secondBatsmanFoursCount;
    private int secondBatsmanSixersCount;
    private int secondBatsmanBallsCount;
    private String battingTeamName;
    private String bowlingTeamName;
    private PlayerInfo firstBatsman;
    private PlayerInfo secondBatsman;
    private PlayerInfo bowler;
    private OverInfo overInfo;
    private MatchInfo matchInfo;
    private TeamInfo bowlingTeam;
    private TeamInfo battingTeam;

    public int getBallNumber() {
        return ballNumber;
    }

    public int getOverNo() {
        return overNo;
    }

    public int getStrikerId() {
        return strikerId;
    }

    public int getTotalRuns() {
        return totalRuns;
    }

    public int getFirstBatsmanRuns() {
        return firstBatsmanRuns;
    }

    public int getFirstBatsmanFoursCount() {
        return firstBatsmanFoursCount;
    }

    public int getFirstBatsmanSixersCount() {
        return firstBatsmanSixersCount;
    }

    public int getFirstBatsmanBallsCount() {
        return firstBatsmanBallsCount;
    }

    public int getSecondBatsmanRuns() {
        return secondBatsmanRuns;
    }

    public int getSecondBatsmanFoursCount() {
        return secondBatsmanFoursCount;
    }

    public int getSecondBatsmanSixersCount() {
        return secondBatsmanSixersCount;
    }

    public int getSecondBatsmanBallsCount() {
        return secondBatsmanBallsCount;
    }

    public String getBattingTeamName() {
        return battingTeamName;
    }
    
    public String getBowlingTeamName() {
        return bowlingTeamName;
    }

    public PlayerInfo getFirstBatsman() {
        return firstBatsman;
    }
    
    public PlayerInfo getSecondBatsman() {
        return secondBatsman;
    }
    
    public PlayerInfo getBowler() {
        return bowler;
    }

    public OverInfo getOverInfo() {
        return overInfo;
    }

    public MatchInfo getMatchInfo() {
        return matchInfo;
    }

    public TeamInfo getBowlingTeam() {
        return bowlingTeam;
    }

    public TeamInfo getBattingTeam() {
        return battingTeam;
    }

    //****************SETTERS***************************


    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }

    public void setOverNo(int overNo) {
        this.overNo = overNo;
    }

    public void setTotalRuns(int totalRuns) {
        this.totalRuns = totalRuns;
    }

    public void setFirstBatsmanRuns(int firstBatsmanRuns) {
        this.firstBatsmanRuns = firstBatsmanRuns;
    }

    public void setFirstBatsmanFoursCount(int firstBatsmanFoursCount) {
        this.firstBatsmanFoursCount = firstBatsmanFoursCount;
    }

    public void setFirstBatsmanSixersCount(int firstBatsmanSixersCount) {
        this.firstBatsmanSixersCount = firstBatsmanSixersCount;
    }

    public void setFirstBatsmanBallsCount(int firstBatsmanBallsCount) {
        this.firstBatsmanBallsCount = firstBatsmanBallsCount;
    }

    public void setSecondBatsmanRuns(int secondBatsmanRuns) {
        this.secondBatsmanRuns = secondBatsmanRuns;
    }

    public void setSecondBatsmanFoursCount(int secondtBatsmanFoursCount) {
        this.secondBatsmanFoursCount = secondBatsmanFoursCount;
    }

    public void setSecondBatsmanSixersCount(int secondBatsmanSixersCount) {
        this.secondBatsmanSixersCount = secondBatsmanSixersCount;
    }

    public void setSecondBatsmanBallsCount(int secondBatsmanBallsCount) {
        this.secondBatsmanBallsCount = secondBatsmanBallsCount;
    }

    public void setStrikerId(int strikerId) {
        this.strikerId = strikerId;
    }
    
    public void setBattingTeamName(String battingTeamName) {
        this.battingTeamName = battingTeamName;
    }
    
    public void setBowlingTeamName(String bowlingTeamName) {
        this.bowlingTeamName = bowlingTeamName;
    }

    public void setFirstBatsman(PlayerInfo firstBatsman) {
        this.firstBatsman = firstBatsman;
    }
    
    public void setSecondBatsman(PlayerInfo secondBatsman) {
        this.secondBatsman = secondBatsman;
    }
    
    public void setBowler(PlayerInfo bowler) {
        this.bowler = bowler;
    }

    public void setOverInfo(OverInfo overInfo) {
        this.overInfo = overInfo;
    }

    public void setMatchInfo(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }

    public void setBowlingTeam(TeamInfo bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }

    public void setBattingTeam(TeamInfo battingTeam) {
        this.battingTeam = battingTeam;
    }
}


