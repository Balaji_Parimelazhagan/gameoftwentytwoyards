package com.ideas2it.gameoftwentytwoyards.info;

/**
 * Player is a pojo class that performs getter and setter methods to get and
 * set methods respectively.
 *
 */
public class OverInfo {
    private boolean isMaiden;
    private int id;
    private int overNo;
    private int runs;
    private int wicketsCount;
    private int foursCount;
    private int sixersCount;
    private int widesCount;
    private int byesCount;
    private int legByesCount;
    private int totalExtras;
    private MatchInfo matchInfo;

    public boolean getIsMaiden() {
        return isMaiden;
    }

    public int getOverNo() {
        return overNo;
    }

    public int getId() {
        return id;
    }

    public int getRuns() {
        return runs;
    }

    public int getWicketsCount() {
        return wicketsCount;
    }

    public int getFoursCount() {
        return foursCount;
    }

    public int getSixersCount() {
        return sixersCount;
    }

    public int getWidesCount() {
        return widesCount;
    }

    public int getByesCount() {
        return byesCount;
    }

    public int getLegByesCount() {
        return legByesCount;
    }

    public int getTotalExtras() {
        return totalExtras;
    }

    public MatchInfo getMatchInfo() {
        return matchInfo;
    }

    //****************SETTERS***************************

    public void setIsMaiden(boolean isMaiden) {
        this.isMaiden = isMaiden;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setOverNo(int overNo) {
        this.overNo = overNo;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public void setWicketsCount(int wicketsCount) {
        this.wicketsCount = wicketsCount;
    }

    public void setFoursCount(int foursCount) {
        this.foursCount = foursCount;
    }

    public void setSixersCount(int sixersCount) {
        this.sixersCount = sixersCount;
    }

    public void setWidesCount(int widesCount) {
        this.widesCount = widesCount;
    }

    public void setByesCount(int byesCount) {
        this.byesCount = byesCount;
    }

    public void setLegByesCount(int legByesCount) {
        this.legByesCount = legByesCount;
    }

    public void setTotalExtras(int totalExtras) {
        this.totalExtras = totalExtras;
    }

    public void setMatchInfo(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }

    
    /*public boolean equals(Object object) {
        Player player = (Player) object;
        if (this == object || this.id == player.getId()) {
            return Boolean.TRUE;
        } else if (!(object instanceof Player)) { 
            return Boolean.FALSE;
        } else {
            return Boolean.FALSE;
        }
    }

    public int hashCode() {
        return this.id;
    }

    public String toString() {
        String status = "";
        if (this.playerStatus) {
           status = "ACTIVE";
        } else {
            status = "INACTIVE";
        }
        StringBuilder playerDetails = new StringBuilder("");
        playerDetails.append("\n*****************************")
        .append("\n***GAME OF 22 YARDS***")
        .append("\nPLAYER ID:").append(this.id)
        .append("    NAME:").append(this.playerName)
        .append("   AGE:").append(this.playerAge)
        .append("    COUNTRY NAME:").append(this.countryName)
        .append("\nPLAYER STATUS:").append(status)
        .append("    ROLE:").append(this.playerRole)
        .append("    BATTING TYPE:").append(this.battingStyle)
        .append("    BOWLING TYPE:").append(this.bowlingStyle)
        .append(contact.toString())
        .append("\n*****************************");
        return playerDetails.toString();
    }*/
}


