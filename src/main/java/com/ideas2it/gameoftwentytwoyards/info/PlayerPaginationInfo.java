package com.ideas2it.gameoftwentytwoyards.info;

import java.util.List;
import java.util.Set;

/**
 * Player is a pojo class contais the details of a single match.
 */
public class PlayerPaginationInfo {
    private Set<PlayerInfo> playerInfos;
    private List<Integer> pages;
    
    public Set<PlayerInfo> getPlayerInfos() {
        return playerInfos;
    }

    public List<Integer> getPages() {
        return pages;
    }

    public void setPlayerInfos(Set<PlayerInfo> playerInfos) {
        this.playerInfos = playerInfos;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }
}
