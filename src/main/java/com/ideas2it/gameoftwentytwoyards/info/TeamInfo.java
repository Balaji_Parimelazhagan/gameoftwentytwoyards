package com.ideas2it.gameoftwentytwoyards.info;

import java.util.List;
import java.util.HashSet;
import java.util.Set;

import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.entities.Match;

/**
 * Team is a pojo class contais the details of a single team.
 */
public class TeamInfo {
    private boolean teamStatus;
    private int id;
    private Country country;
    private String teamName;
    private Set<MatchInfo> matches = new HashSet<MatchInfo>();
    private Set<PlayerInfo> playersInTeam = new HashSet<PlayerInfo>();

    /**
     * Getters and Setters to fetch and insert properties.
     */


    public boolean getTeamStatus() {
        return teamStatus;
    }

    public int getId() {
        return id ;
    }

    public Country getCountry() {
        return country;
    }

    public String getTeamName() {
        return teamName;
    }

    public Set<MatchInfo> getMatches() {
        return matches;
    }

    public Set<PlayerInfo> getPlayersInTeam() {
        return playersInTeam;
    }

    /***********************SETTERS*********************/

    public void setTeamStatus(boolean teamStatus) {
        this.teamStatus = teamStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public void setMatches(Set<MatchInfo> matches) {
        this.matches = matches;
    }

    public void setPlayersInTeam(Set<PlayerInfo> playersInTeam) {
        this.playersInTeam = playersInTeam;
    }
}
