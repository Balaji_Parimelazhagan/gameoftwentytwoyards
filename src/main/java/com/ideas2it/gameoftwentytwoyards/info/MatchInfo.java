package com.ideas2it.gameoftwentytwoyards.info;

import java.util.List;
import java.util.Set;
import java.util.Date;

import com.ideas2it.gameoftwentytwoyards.common.MatchType;
import com.ideas2it.gameoftwentytwoyards.entities.Match;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.info.TeamInfo;

/**
 * Match is a pojo class contais the details of a single match.
 */
public class MatchInfo {
    private int id;
    private Date matchDate;
    private MatchType matchType;
    private String matchLocation;
    private String matchName;
    private Set<TeamInfo> teamsForMatch;

    /**
     * Getters and Setters to fetch and insert properties.
     */
    public int getId() {
        return id;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public MatchType getMatchType() {
        return matchType;
    }

    public String getMatchLocation() {
        return matchLocation;
    }

    public String getMatchName() {
        return matchName;
    }

    public Set<TeamInfo> getTeamsForMatch() {
        return teamsForMatch;
    }
    /********************************SETTERS******************/
    public void setId(int id) {
        this.id = id;
    }

    public void setMatchDate(Date matchDate) {
        this.matchDate = matchDate;
    }

    public void setMatchType(MatchType matchType) {
        this.matchType = matchType;
    }

    public void setMatchLocation(String matchLocation) {
        this.matchLocation = matchLocation;
    }

    public void setMatchName(String matchName) {
        this.matchName = matchName;
    }

    public void setTeamsForMatch(Set<TeamInfo> teamsForMatch) {
        this.teamsForMatch = teamsForMatch;
    }
}
