package com.ideas2it.gameoftwentytwoyards.info;

import java.util.List;

import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.utils.AgeUtil;

/**
 * Player is a pojo class contais the details of a single match.
 */
public class PlayerInfo {
    private boolean playerStatus;
    private int id;
    private int contactId;
    private Country countryName;
    private String battingStyle;
    private String bowlingStyle;
    private String DOB;
    private String imagePath;
    private String paceType;
    private String playerAge;
    private String playerName;
    private String playerRole;
    private String address;
    private String city;
    private String contactNumber;
    private TeamInfo team;


    /**
     * Getters and Setters of the player pojo class.
     */
    public boolean getPlayerStatus() {
        return playerStatus;
    }

    public int getId() {
        return id;
    }

    public int getContactId() {
        return contactId;
    }

    public Country getCountryName() {
        return countryName;
    }

    public String getBattingStyle() {
        return battingStyle;
    }

    public String getBowlingStyle() {
        return bowlingStyle;
    }

    public String getDOB() {
        return DOB;
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getPaceType() {
        return paceType;
    }

    public String getPlayerAge() {
        return AgeUtil.fetchPlayerAge(this.DOB);
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getPlayerRole() {
        return playerRole;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public TeamInfo getTeam() {
        return team;
    }
//***********************************************************
    public void setPlayerStatus(boolean playerStatus) {
        this.playerStatus = playerStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setContactId(int contactId) {
        this.contactId = contactId;
    }

    public void setCountryName(Country countryName) {
        this.countryName = countryName;
    }

    public void setBattingStyle(String battingStyle) {
        this.battingStyle = battingStyle;
    }

    public void setBowlingStyle(String bowlingStyle) {
        this.bowlingStyle = bowlingStyle;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setPaceType(String paceType) {
        this.paceType = paceType;
    }

    public void setPlayerAge(String playerAge) {
        this.playerAge = playerAge;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setPlayerRole(String playerRole) {
        this.playerRole = playerRole;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public void setTeam(TeamInfo team) {
        this.team = team;
    }
}
