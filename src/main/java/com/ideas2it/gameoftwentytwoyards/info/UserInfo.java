package com.ideas2it.gameoftwentytwoyards.info;

import com.ideas2it.gameoftwentytwoyards.common.Gender;
import com.ideas2it.gameoftwentytwoyards.common.UserRole;

/**
 * Player is a pojo class that performs getter and setter methods to get and
 * set methods respectively.
 *
 */
public class UserInfo {
    private boolean userStatus;
    private int id;
    private int loginFailAttempts;
    private Gender userGender;
    private UserRole userRole;
    private String userName;
    private String userEmailId;
    private String password;

    /**
     * Getters and Setters of the player pojo class.
     */
    public boolean getUserStatus() {
        return userStatus;
    }

    public int getId() {
        return id;
    }

    public int getLoginFailAttempts() {
        return loginFailAttempts;
    }

    public Gender getUserGender() {
        return userGender;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public String getPassword() {
        return password;
    }

    public void setUserStatus(boolean userStatus) {
        this.userStatus = userStatus;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLoginFailAttempts(int loginFailAttempts) {
        this.loginFailAttempts = loginFailAttempts;
    }

    public void setUserGender(Gender userGender) {
        this.userGender = userGender;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
