package com.ideas2it.gameoftwentytwoyards.info;

import java.util.List;
import java.util.Set;

/**
 * Match is a pojo class contais the details of a single match.
 */
public class MatchPaginationInfo {
    private MatchInfo matchInfo;
    private Set<TeamInfo> teamInfos;
    private Set<MatchInfo> matchInfos;
    private List<Integer> pages;
    private TeamInfo battingTeam;
    private TeamInfo bowlingTeam;

    /**
     * Getters and Setters to fetch and insert properties.
     */
    public MatchInfo getMatchInfo() {
        return matchInfo;
    }

    public Set<TeamInfo> getTeamInfos() {
        return teamInfos;
    }

    public Set<MatchInfo> getMatchInfos() {
        return matchInfos;
    }

    public List<Integer> getPages() {
        return pages;
    }
    public TeamInfo getBattingTeam() {
        return battingTeam;
    }
    public TeamInfo getBowlingTeam() {
        return bowlingTeam;
    }
    /********************************SETTERS******************/
    
    public void setMatchInfo(MatchInfo matchInfo) {
        this.matchInfo = matchInfo;
    }

    public void setTeamInfos(Set<TeamInfo> teamInfos) {
        this.teamInfos = teamInfos;
    }

    public void setMatchInfos(Set<MatchInfo> matchInfos) {
        this.matchInfos = matchInfos;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }
    public void setBattingTeam(TeamInfo battingTeam) {
        this.battingTeam = battingTeam;
    }
    public void setBowlingTeam(TeamInfo bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }
}
