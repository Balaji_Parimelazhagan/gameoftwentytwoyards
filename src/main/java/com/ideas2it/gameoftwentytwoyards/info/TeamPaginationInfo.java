package com.ideas2it.gameoftwentytwoyards.info;

import java.util.List;
import java.util.Set;

/**
 * Player is a pojo class contais the details of a single match.
 */
public class TeamPaginationInfo {
    private TeamInfo teamInfo;
    private Set<TeamInfo> teamInfos;
    private Set<PlayerInfo> playerInfos;
    private List<Integer> pages;
    
    public TeamInfo getTeamInfo() {
        return teamInfo;
    }
    
    public Set<TeamInfo> getTeamInfos() {
        return teamInfos;
    }
    
    public Set<PlayerInfo> getPlayerInfos() {
        return playerInfos;
    }

    public List<Integer> getPages() {
        return pages;
    }

    public void setPlayerInfos(Set<PlayerInfo> playerInfos) {
        this.playerInfos = playerInfos;
    }

    public void setTeamInfos(Set<TeamInfo> teamInfos) {
        this.teamInfos = teamInfos;
    }

    public void setPages(List<Integer> pages) {
        this.pages = pages;
    }
    
    public void setTeamInfo(TeamInfo teamInfo) {
        this.teamInfo = teamInfo;
    }
}
