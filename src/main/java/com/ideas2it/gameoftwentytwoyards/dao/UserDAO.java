package com.ideas2it.gameoftwentytwoyards.dao;

import com.ideas2it.gameoftwentytwoyards.entities.User;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;

public interface UserDAO {

    /**
     * Inserts the user details with contact information into the Database.
     *
     * @param user of User, Object to be added in the DB.
     * @return getPlayer(userId), calls the function for retreiving the detail
     * as object to display the created user.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    int createUser(User user) throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the user details from the Database, and
     * converts into an object.
     *
     * @param userId of int, id of the user to be retrieved.
     * @return user, it is the object that contains the details of the user.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    User getUser(int userId) throws GameOfTwentyTwoYardsException;

    /**
     * Updates the user details to the Database.
     *
     * @param user of User, object to updated in the Database.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    void updateUser(User user) throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the User for corresponding email id from the database.
     *
     * @param emailId of String, emailid for which the password dhould
     * be obtained.
     * @return user of User, It is user object used for validation.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    User getUserForValidate(String emailId)
            throws GameOfTwentyTwoYardsException;
}
