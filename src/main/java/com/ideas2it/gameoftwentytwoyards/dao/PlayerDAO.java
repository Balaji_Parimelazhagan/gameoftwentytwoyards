package com.ideas2it.gameoftwentytwoyards.dao;

import java.util.List;

import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;

/**
 * Performs insert, fetch, update, delete operation of data with database.
 * Also provide
 * object whenever needed.
 *
 */
public interface PlayerDAO {

    /**
     * Inserts the player details with contact information into the Database.
     *
     * @param player of Player, Object to be added in the DB.
     * @return getPlayer(playerId), calls the function for retreiving the detail
     * as object to display the created player.
     */
    int insertPlayer
            (Player player) throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the player details from the Database, and
     * converts into an object.
     *
     * @param playerId of int, id of the player to be retrieved.
     * @return player, it is the object that contains the details of the player.
     */
    Player getPlayerById
            (int playerId) throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the player details from the Database, and
     * converts into an object and stores it in arraylist.
     *
     * @param offset of int, is the integer which displays 5 players at a time.
     * @return playersInList, it is the arraylist that contains
     * the details of 5 players.
     */
    List<Player> retrievePlayerByLimit
            (int offset) throws GameOfTwentyTwoYardsException;

    /**
     * Combines the two tables and updates the data in the database for
     * corressponding id.
     *
     * @param player of Player, object that contains the updated information.
     * @return getPlayer(playerId), calls the method for data retrieve to
     * display the updates information.
     */
    Player updatePlayer
            (Player player) throws GameOfTwentyTwoYardsException;

    /**
     * Gets the total number of rows in the id feild.
     *
     * @return playersCount as int, totalPlayers in the table.
     */
    int retrievePlayersCount() throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the Players By country five at a time.
     * 
     * @param countryAsString of String, is the country based on which the
     * players are filtereed.
     * @param offsetForView of int, is the offset which limits the players
     * count by five.
     * @return playersByCountry, is the arrayList which contains the details
     * of 5 players.
     */
    List<Player> fetchPlayersByCountry
            (String countryAsString) throws GameOfTwentyTwoYardsException;

    /**
     * Adds the player to the team, which is done by updating the team_id
     * column to corressponding team's id.
     *
     * @param teamId of int, is the team id that should be updated.
     * @param playersId of String, it is ids of the players where the
     * team id should be updated.
     */
    List<Player> fetchPlayersToBeAdded
            (List<Integer> playerIds) throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the Players By Role of a specific team.
     *
     * @param playersByRole of String, has the player's id in string format
     * seperated by - symbol.
     */
    int fetchPlayersCountByRole(int teamId,
            String playerRole) throws GameOfTwentyTwoYardsException;
}
