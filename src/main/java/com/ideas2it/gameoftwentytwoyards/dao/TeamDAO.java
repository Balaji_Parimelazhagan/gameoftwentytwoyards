package com.ideas2it.gameoftwentytwoyards.dao;

import java.util.List;
import java.util.Set;

import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;

public interface TeamDAO {

    /**
     * Inserts the team details into the Database.
     *
     * @param team of Team, Object contains the details to be inserted.
     * @return teamId of int , is the team id of the inserted object.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    int createTeam
            (Team team) throws GameOfTwentyTwoYardsException;

    /**
     * Updates the Team inforamtion in the database.
     *
     * @param team of Team, Object contains the details to be updated.
     * @return retrieveTeam() of Team, calls the method for data retrieve to
     * display the updated information.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Team updateTeam
            (Team team) throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the team details from the Database, and
     * converts into an object.
     *
     * @param teamId of int, id of the team to be retrieved.
     * @return team, it is the object that contains the details of the team.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Team retrieveTeam
            (int teamId) throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the team details from the Database, and
     * converts into an object and stores it in arraylist (5 teams in a list).
     *
     * @param offsetForView of int, is the integer which displays 5 teams
     * at a time.
     * @return teams, it is the arraylist that contains
     * the details of 5 teams.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Set<Team> fetchAllTeams()
            throws GameOfTwentyTwoYardsException;

    /**
     * Performs hard delete and deletes the entire row of the table for
     * corressponding id.
     *
     * @param playerId of int, it is team id to be deleted.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    void deleteTeam
            (int teamId) throws GameOfTwentyTwoYardsException;

    /**
     * Gets the total number of rows in the id feild.
     *
     * @return playersCount as int, totalPlayers in the table.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    int fetchTeamsCount() throws GameOfTwentyTwoYardsException;

    /**
     * Fetches the Teams For the Match.
     * 
     * @param countryAsString of String, is the country based on which the
     * players are filtereed.
     * @param offsetForView of int, is the offset which limits the players
     * count by five.
     * @return playersByCountry, is the arrayList which contains the details
     * of 5 players.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
    Set<Team> fetchTeamsForMatch (List<Integer> teamIds)
            throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the Team details from the Database, and
     * converts into an object and stores it in arraylist.
     *
     * @param offset of int, is the integer which displays 5 teams at a time.
     * @return playersInList, it is the arraylist that contains
     * the details of 5 teams.
     */
    List<Team> retrieveTeamsByLimit (int offset)
            throws GameOfTwentyTwoYardsException;
}
