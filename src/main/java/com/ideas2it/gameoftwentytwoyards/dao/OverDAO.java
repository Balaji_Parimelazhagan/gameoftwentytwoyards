package com.ideas2it.gameoftwentytwoyards.dao;

import java.util.List;

import com.ideas2it.gameoftwentytwoyards.entities.Over;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;

public interface OverDAO {
    
    /**
     * Inserts the match details into the Database.
     *
     * @param match of Match, Object contains the details to be inserted.
     * @return matchId of int , is the match id of the inserted object.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
     int insertOver(Over over) throws GameOfTwentyTwoYardsException;

     /**
     * Retrieves the match details from the Database, and
     * converts into an object.
     *
     * @param matchId of int, id of the match to be retrieved.
     * @return match, it is the object that contains the details of the match.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
     int fetchLatestOverId(int matchId) throws GameOfTwentyTwoYardsException;
    /**
     * Updates the match inforamtion in the database.
     *
     * @param match of Match, Object contains the details to be updated.
     * @return match of Match, contains the updated match details.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
     void updateOver(Over over) throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the match details from the Database, and
     * converts into an object.
     *
     * @param matchId of int, id of the match to be retrieved.
     * @return match, it is the object that contains the details of the match.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     */
     Over retrieveOver(int id) throws GameOfTwentyTwoYardsException;

    /**
     * Hard deletes the match information.
     *
     * @param matchId of int, it is match id to be deleted.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     *
     void deleteMatch(int matchId) throws GameOfTwentyTwoYardsException;

    /**
     * Gets the total number of matches.
     *
     * @return matchesCount as int, total matches in the table.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     *
     int fetchMatchesCount() throws GameOfTwentyTwoYardsException;

    /**
     * Retrieves the match details from the Database, and
     * converts into an object and stores it in arraylist.
     *
     * @param offset of int, is the integer which displays 5 matches at a time.
     * @return matchesInList, it is the arraylist that contains
     * the details of 5 matches.
     * @throws GameOfTwentyTwoYardsException, user defined exception.
     *
     List<Match> retrieveMatchesByLimit
            (int offset) throws GameOfTwentyTwoYardsException;*/
}
