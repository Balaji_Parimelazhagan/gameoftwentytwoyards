package com.ideas2it.gameoftwentytwoyards.dao.impl;

import java.lang.NullPointerException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.gameoftwentytwoyards.entities.Over;
import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.dao.OverDAO;

@Repository
public class OverDAOImpl implements OverDAO {
    private SessionFactory factory;

    static final Logger logger = Logger.getLogger(OverDAOImpl.class);

    /**
     * Constructor initialises the objects that is used throughout the match
     * dao layer. 
     */
    @Autowired
    public OverDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }
    
    @Override
    public int insertOver
            (Over over) throws GameOfTwentyTwoYardsException {
        int id = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            id = (Integer) session.save(over);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_CREATE_MATCH, e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
        return id;
    }

    @Override
    public int fetchLatestOverId(int matchId) throws GameOfTwentyTwoYardsException {
        int overNo = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                              "select max(overNo) from Over where match_id = :matchId");
            query.setParameter("matchId", matchId);
            if (null != query.getSingleResult()) {
                overNo = (Integer)query.getSingleResult();
            }
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_MATCHES_COUNT, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return overNo;
    }

    @Override
    public Over retrieveOver
            (int id) throws GameOfTwentyTwoYardsException {
        Over over = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            over = (Over) session.get(Over.class, id);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_MATCH + id, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return over;
    }


    @Override
    public void updateOver
            (Over over) throws GameOfTwentyTwoYardsException {
        int overId = over.getId();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            session.update(over);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_UPDATE_MATCH + overId, e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
    }
    /*
    @Override
    public void deleteMatch(int matchId) throws GameOfTwentyTwoYardsException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            Match match = (Match) session.get(Match.class, matchId);
            session.delete(match);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                   Constants.ERROR_MESSAGE_DELETE_MATCH + matchId, e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
    }

    @Override
    public int fetchMatchesCount() throws GameOfTwentyTwoYardsException {
        int matchesCount = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                              "select count(id) from Match");
            matchesCount = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_MATCHES_COUNT, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return matchesCount;
    }

    @Override
    public List<Match> retrieveMatchesByLimit
            (int offset) throws GameOfTwentyTwoYardsException {
        List<Match> matchesInList = new ArrayList<Match>();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("From Match");
            query.setFirstResult(offset);
            query.setMaxResults(5);
            matchesInList = query.list();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_MATCHES, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return matchesInList;
    }*/
}
