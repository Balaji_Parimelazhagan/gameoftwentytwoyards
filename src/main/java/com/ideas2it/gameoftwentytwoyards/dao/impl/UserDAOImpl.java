package com.ideas2it.gameoftwentytwoyards.dao.impl;

import java.lang.NullPointerException;
import java.sql.Connection;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.common.Gender;
import com.ideas2it.gameoftwentytwoyards.common.UserRole;
import com.ideas2it.gameoftwentytwoyards.entities.User;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.dao.UserDAO;

/**
 * Performs insert, fetch, update, delete operation of data with database.
 * Also provide
 * object whenever needed.
 *
 */
@Repository
public class UserDAOImpl implements UserDAO {
    private SessionFactory factory;

    static final Logger logger = Logger.getLogger(UserDAOImpl.class);

    /**
     * Constructor initialises the objects that is used throughout the user
     * dao layer. 
     */
    @Autowired
    public UserDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public int createUser
            (User user) throws GameOfTwentyTwoYardsException {
        int id = 1;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_CREATE_USER + user.getUserName(), e);
        } catch (PersistenceException e) {
            id = 0;
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return id;
    }

    @Override
    public User getUser
            (int userId) throws GameOfTwentyTwoYardsException {
        User user = new User();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            user = (User)session.get(User.class, userId);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_USER + userId, e);
        } finally {
            session.close();
        }
        return user;
    }

    @Override
    public void updateUser(User user) throws GameOfTwentyTwoYardsException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            session.update(user);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_UPDATE_USER + user.getId(), e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
    }

    @Override
    public User getUserForValidate(String emailId)
            throws GameOfTwentyTwoYardsException {
        User user = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                             "from User where userEmailId = :email_id");
            query.setString("email_id", emailId);
            user = (User) query.getSingleResult();
        } catch (NoResultException e) {
            user = null;
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_USER + emailId, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return user;
    }
}
