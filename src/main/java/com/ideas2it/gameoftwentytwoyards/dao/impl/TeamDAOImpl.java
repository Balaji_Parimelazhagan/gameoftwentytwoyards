package com.ideas2it.gameoftwentytwoyards.dao.impl;

import java.lang.NullPointerException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.dao.TeamDAO;

@Repository
public class TeamDAOImpl implements TeamDAO {
    private SessionFactory factory;

    static final Logger logger = Logger.getLogger(TeamDAOImpl.class);

    /**
     * Constructor initialises the objects that is used throughout the team
     * dao layer.
     */     
    @Autowired                                                                            
    public TeamDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public int createTeam
            (Team team) throws GameOfTwentyTwoYardsException {
        int id = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            id = (Integer) session.save(team);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_CREATE_TEAM
                    + team.getTeamName(), e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
        return id;
    }

    @Override
    public Team updateTeam
            (Team team) throws GameOfTwentyTwoYardsException {
        int teamId = team.getId();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            session.update(team);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_UPDATE_TEAM + teamId, e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
        return retrieveTeam(teamId);
    }

    @Override
    public Team retrieveTeam(
            int teamId) throws GameOfTwentyTwoYardsException {
        Team team = new Team();
        Set<Player> players = new HashSet<Player>();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            team = (Team)session.get(Team.class, teamId);
            players.addAll(team.getPlayersInTeam());
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_TEAM + teamId, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        team.setPlayersInTeam(players);
        return team;
    }

    @Override
    public Set<Team> fetchAllTeams
            () throws GameOfTwentyTwoYardsException {
        Set<Team> teams = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("From Team where status = 1");
            List<Team> teamsAsList = query.list();
            teams = new HashSet<Team>(teamsAsList);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_TEAMS, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return teams;
    }

    @Override
    public void deleteTeam(int teamId) throws GameOfTwentyTwoYardsException {
        Team team;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            team = (Team)session.get(Team.class, teamId);
            session.delete(team);
            transaction.commit();
        } catch (HibernateException | NullPointerException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_DELETE_TEAM + teamId, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    @Override
    public int fetchTeamsCount() throws GameOfTwentyTwoYardsException {
        int teamsCount = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                              "select count(id) from Team");
            teamsCount = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_TEAMS_COUNT, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return teamsCount;
    }

    @Override
    public Set<Team> fetchTeamsForMatch(
            List<Integer> teamIds) throws GameOfTwentyTwoYardsException {
        List<Team> teams= new ArrayList<Team>();
        Set<Team> teamsForMatch;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                             "from Team where id IN :teamIds" );
            query.setParameter("teamIds", teamIds);
            teams = query.list();
            teamsForMatch = new HashSet<Team>(teams);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_MATCH_TEAM + teamIds, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return teamsForMatch;
    }

    @Override
    public List<Team> retrieveTeamsByLimit
            (int offset) throws GameOfTwentyTwoYardsException {
        List<Team> teamsInList = new ArrayList<Team>();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("From Team");
            query.setFirstResult(offset);
            query.setMaxResults(5);
            teamsInList = query.list();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_TEAMS, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return teamsInList;
    }
}
