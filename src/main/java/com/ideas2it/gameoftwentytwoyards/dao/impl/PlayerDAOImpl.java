package com.ideas2it.gameoftwentytwoyards.dao.impl;

import java.lang.StringBuilder;
import java.lang.NullPointerException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.entities.Contact;
import com.ideas2it.gameoftwentytwoyards.entities.Player;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.dao.PlayerDAO;

/**
 * Performs insert, fetch, update, delete operation of data with database.
 * Also provide
 * object whenever needed.
 *
 */
@Repository
public class PlayerDAOImpl implements PlayerDAO {
    private SessionFactory factory;

    static final Logger logger = Logger.getLogger(PlayerDAOImpl.class);

    /**
     * Constructor initialises the objects that is used throughout the player
     * dao layer. 
     */
    @Autowired
    public PlayerDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public int insertPlayer
            (Player player) throws GameOfTwentyTwoYardsException {
        int id = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            id = (Integer) session.save(player);
            
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_CREATE_PLAYER
                    + player.getPlayerName(), e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
        return id;
    }

    @Override
    public Player getPlayerById
            (int playerId) throws GameOfTwentyTwoYardsException {
        Player playerForTeamExchange = new Player();
        Player player = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            player = (Player)session.get(Player.class, playerId);
            playerForTeamExchange.setTeam(player.getTeam());
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_PLAYER + playerId, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        player.setTeam(playerForTeamExchange.getTeam());
        return player;
    }

    @Override
    public List<Player> retrievePlayerByLimit
            (int offset) throws GameOfTwentyTwoYardsException {
        List<Player> playersInList = new ArrayList<Player>();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("From Player where status = 1");
            query.setFirstResult(offset);
            query.setMaxResults(5);
            playersInList = query.list();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_PLAYERS, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return playersInList;
    }

    @Override
    public Player updatePlayer
            (Player player) throws GameOfTwentyTwoYardsException {
        int playerId = player.getId();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            session.update(player);
        } catch (HibernateException e) {
            transaction.rollback();
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_UPDATE_PLAYER + playerId, e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
        return player;
    }

    @Override
    public int retrievePlayersCount() throws GameOfTwentyTwoYardsException {
        int playersCount = 0;
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                              "select count(id) from Player where status = 1");
            playersCount = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_PLAYERS_COUNT, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return playersCount;
    }

    @Override
    public List<Player> fetchPlayersByCountry
            (String countryAsString) throws GameOfTwentyTwoYardsException {
        List<Player> playersByCountry = new ArrayList<Player>();
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                             "from Player where country = "
                             + ":country and team_id = null and status = 1");
            query.setString("country", countryAsString);
            playersByCountry = query.list();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_COUNTRY_PLAYERS
                    + countryAsString , e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return playersByCountry;
    }

    @Override
    public List<Player> fetchPlayersToBeAdded
            (List<Integer> playerIds) throws GameOfTwentyTwoYardsException {
        List<Player> playersToBeAdded = new ArrayList<Player>();
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                    "FROM Player WHERE id IN :playerIds");
            query.setParameter("playerIds", playerIds);
            playersToBeAdded = query.getResultList();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_TEAM_PLAYERS, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return playersToBeAdded;
    }

    @Override
    public int fetchPlayersCountByRole(int teamId,
            String playerRole) throws GameOfTwentyTwoYardsException {
        int playersCount = 0;
        Session session = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                             "select count(id) from Player where "
                             + "team_id = :teamId and role = :playerRole");
            query.setParameter("teamId", teamId);
            query.setParameter("playerRole", playerRole);
            playersCount = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_PLAYER_ROLES_COUNT + teamId,
                    e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return playersCount;
    }
}
