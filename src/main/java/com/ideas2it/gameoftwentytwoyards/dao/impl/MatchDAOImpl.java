package com.ideas2it.gameoftwentytwoyards.dao.impl;

import java.lang.NullPointerException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

import org.apache.log4j.Logger;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ideas2it.gameoftwentytwoyards.common.Constants;
import com.ideas2it.gameoftwentytwoyards.common.Country;
import com.ideas2it.gameoftwentytwoyards.exception.GameOfTwentyTwoYardsException;
import com.ideas2it.gameoftwentytwoyards.entities.Match;
import com.ideas2it.gameoftwentytwoyards.entities.Team;
import com.ideas2it.gameoftwentytwoyards.dao.MatchDAO;

@Repository
public class MatchDAOImpl implements MatchDAO {
    private SessionFactory factory;

    static final Logger logger = Logger.getLogger(MatchDAOImpl.class);

    /**
     * Constructor initialises the objects that is used throughout the match
     * dao layer. 
     */
    @Autowired
    public MatchDAOImpl(SessionFactory factory) {
        this.factory = factory;
    }
    
    @Override
    public int createMatch
            (Match match) throws GameOfTwentyTwoYardsException {
        int id = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            id = (Integer) session.save(match);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_CREATE_MATCH
                    + match.getMatchName(), e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
        return id;
    }

    @Override
    public Match updateMatch
            (Match match) throws GameOfTwentyTwoYardsException {
        int matchId = match.getId();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            session.update(match);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_UPDATE_MATCH + matchId, e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
        match = retrieveMatch(matchId);
        return match;
    }

    @Override
    public Match retrieveMatch
            (int matchId) throws GameOfTwentyTwoYardsException {
        Match match = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            match = (Match) session.get(Match.class, matchId);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_MATCH + matchId, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return match;
    }

    @Override
    public void deleteMatch(int matchId) throws GameOfTwentyTwoYardsException {
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            transaction = session.beginTransaction();
            Match match = (Match) session.get(Match.class, matchId);
            session.delete(match);
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            transaction.rollback();
            throw new GameOfTwentyTwoYardsException(
                   Constants.ERROR_MESSAGE_DELETE_MATCH + matchId, e);
        } finally {
            if (null != session && null != transaction) {
                transaction.commit();
                session.close();
            }
        }
    }

    @Override
    public int fetchMatchesCount() throws GameOfTwentyTwoYardsException {
        int matchesCount = 0;
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery(
                              "select count(id) from Match");
            matchesCount = ((Long) query.getSingleResult()).intValue();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_FETCH_MATCHES_COUNT, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return matchesCount;
    }

    @Override
    public List<Match> retrieveMatchesByLimit
            (int offset) throws GameOfTwentyTwoYardsException {
        List<Match> matchesInList = new ArrayList<Match>();
        Session session = null;
        Transaction transaction = null;
        try {
            session = factory.openSession();
            Query query = session.createQuery("From Match");
            query.setFirstResult(offset);
            query.setMaxResults(5);
            matchesInList = query.list();
        } catch (HibernateException e) {
            logger.error(e.getMessage());
            throw new GameOfTwentyTwoYardsException(
                    Constants.ERROR_MESSAGE_RETRIEVE_MATCHES, e);
        } finally {
            if (null != session) {
                session.close();
            }
        }
        return matchesInList;
    }
}
