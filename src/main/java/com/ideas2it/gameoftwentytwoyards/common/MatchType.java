package com.ideas2it.gameoftwentytwoyards.common;

import java.util.HashMap;
import java.util.Map;

public enum MatchType {
    OneDayInternationalMatch(1),
    t20Match(2),
    TestMatch(3);
    private int value;

    /**
     * Constructor which intialises values for classes from feilds.
     * @param value of int
     */
    private MatchType(int value) {
        this.value = value;
    }

    /**
     * Puts the feild and clases into map as pairs, whenever called returns the
     * class.
     * @param index of int.
     * @return countryIndexPair.get(index), returns the value for the key.
     */
    public static MatchType getMatchType(int index) {
        Map<Integer, MatchType> matchTypeInMap 
                = new HashMap<Integer, MatchType>();

        // stores the values with corresponding key.
        for (MatchType matchType : MatchType.values()) {
        matchTypeInMap.put(matchType.value, matchType);
        }
        return matchTypeInMap.get(index);
    }
}
