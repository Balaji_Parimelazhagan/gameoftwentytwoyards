package com.ideas2it.gameoftwentytwoyards.common;

/**
 * Country is an enumeration that performs getter method to pass country name.
 */
public enum Country {
    Australia,
    Afghanistan,
    Bangladesh,
    England,
    India,
    NewZealand,
    Pakistan,
    SriLanka,
    SouthAfrica,
    WestIndies;
}
