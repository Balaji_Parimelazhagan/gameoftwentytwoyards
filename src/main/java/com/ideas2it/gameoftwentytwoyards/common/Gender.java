package com.ideas2it.gameoftwentytwoyards.common;

/**
 * Gender as enumeration.
 */
public enum Gender {
    Male,
    Female,
    Transgender,
}
