package com.ideas2it.gameoftwentytwoyards.common;

import java.lang.StringBuilder;
import java.util.HashMap;

/**
 * Contains the constants used inside the program
 */
public class Constants {
    
    //Label Consatants
    public final static String LOGIN_URL = "/UserController";
    public final static String ADD = "add";
    public final static String NAME = "name";
    public final static String USER_ROLE = "userRole";
    public final static String USER_GENDER = "userGender";
    public final static String USER_EMAIL_ID = "userEmailId";
    public final static String PASSWORD = "password";
    public final static String CREATE = "create";
    public final static String DELETE = "delete";
    public final static String UPDATE = "update";
    public final static String SIGN_UP = "signUp";
    public final static String LOG_IN = "login";
    public final static String LOG_OUT = "logOut";
    public final static String SUBMIT = "submit";
    public final static String EDIT = "edit";
    public final static String SAVE = "save";
    public final static String VIEW = "view";
    public final static String MESSAGE = "message";
    public final static String VIEW_TEAMS = "viewTeams";
    public final static String VIEW_MATCHES = "viewMatches";
    public final static String VIEW_PLAYERS = "viewPlayers";
    public final static String VIEW_ALL_PLAYERS = "viewAllPlayers";
    public final static String VIEW_ALL_TEAMS = "viewAllTeams";
    public final static String VIEW_ALL_MATCHES = "viewAllMatches";
    public final static String ID = "id";
    public final static String MATCH_NAME = "matchName";
    public final static String MATCH_LOCATION = "matchLocation";
    public final static String MATCH_DATE = "matchDate";
    public final static String MATCH_TYPE = "matchType";
    public final static String PLAYER = "player";
    public final static String PLAYER_INFO = "playerInfo";
    public final static String PLAYER_ID = "playerId";
    public final static String PLAYERS = "players";
    public final static String PLAYER_NAME = "playerName";
    public final static String PLAYER_ROLE = "playerRole";
    public final static String ROLE = "role";
    public final static String BATTING_STYLE = "battingStyle";
    public final static String BOWLING_STYLE = "bowlingStyle";
    public final static String PACE_TYPE = "paceType";
    public final static String DOB = "DOB";
    public final static String COUNTRY = "country";
    public final static String COUNTRY_NAME = "countryName";
    public final static String ADRESS = "adress";
    public final static String CITY = "city";
    public final static String CONTACT = "contact";
    public final static String CONTACT_NUMBER = "contactNumber";
    public final static String FILE_TO_UPLOAD = "fileToUpload";
    public final static String IMAGE_PATH_TO_STORE =
            "/home/ubuntu/Desktop/uploaded-images/";
    public final static String IMAGE_PATH_TO_RETRIEVE =
            "http://localhost:8080/uploaded-images/";
    public final static String ALL_ROUNDER = "All-Rounder";
    public final static String BATSMAN = "Batsman";
    public final static String BOWLER = "Bowler";
    public final static String WICKET_KEEPER = "Wicket-Keeper";
    public final static String TEAM = "team";
    public final static String TEAM_INFO = "teamInfo";
    public final static String TEAM_ID = "teamId";
    public final static String TEAM_IDS = "teamIds";
    public final static String TEAM_NAME = "teamName";
    public final static String TEAM_STATUS = "teamStatus";
    public final static String PLAYER_IDS = "playerIds";
    public final static String LOCATION = "location";
    public final static String DATE = "date";
    public final static String TYPE = "type";
    public final static String MATCH = "match";
    public final static String MATCH_INFO = "matchInfo";
    public final static String TEAMS = "teams";
    public final static String MATCH_ID = "matchId";
    public final static String PAGE = "page";
    public final static String MATCHES = "matches";
    public final static String PAGES = "pages";
    public final static String ERROR = "error";

    //Numeric Consatants
    public final static int EIGHT = 8;
    public final static int ELEVEN = 11;
    public final static int FIVE = 5;
    public final static int FOUR = 4;
    public final static int NINE = 9;
    public final static int ONE = 1;
    public final static int SEVEN = 7;
    public final static int SIX = 6;
    public final static int TEN = 10;
    public final static int THREE = 3;
    public final static int TWO = 2;

    //Phrase Constants
    public final static String PHRASE_CONTENT_MISSING = "Content Missing";

    //Error Message
    public final static String ERROR_MESSAGE_CREATE_PLAYER =
            "Issue while Creating Player";
    public final static String ERROR_MESSAGE_RETRIEVE_PLAYER =
            "Issue while Retrieving Player";
    public final static String ERROR_MESSAGE_RETRIEVE_PLAYERS =
            "Issue while Retriving Players";
    public final static String ERROR_MESSAGE_UPDATE_PLAYER =
            "Issue while Updating Player";
    public final static String ERROR_MESSAGE_DELETE_PLAYER =
            "Issue while Deleting Player";
    public final static String ERROR_MESSAGE_FETCH_PLAYERS_COUNT =
            "Issue while Fetching Players count";
    public final static String ERROR_MESSAGE_FETCH_COUNTRY_PLAYERS =
            "Issue while Fetching Players for Country";
    public final static String ERROR_MESSAGE_FETCH_TEAM_PLAYERS =
            "Issue while Fetching Player to be added in the team";
    public final static String ERROR_MESSAGE_FETCH_PLAYER_ROLES_COUNT =
            "Issue while fetch Players Count By Role in team";
    public final static String ERROR_MESSAGE_CREATE_TEAM =
            "Issue while Creating Team";
    public final static String ERROR_MESSAGE_RETRIEVE_TEAM =
            "Issue while Retrieving Team";
    public final static String ERROR_MESSAGE_RETRIEVE_TEAMS =
            "Issue while Retriving Teams";
    public final static String ERROR_MESSAGE_UPDATE_TEAM =
            "Issue while Updating Team";
    public final static String ERROR_MESSAGE_DELETE_TEAM =
            "Issue while Deleting Team";
    public final static String ERROR_MESSAGE_FETCH_TEAMS_COUNT =
            "Issue while Fetching Teams count";
    public final static String ERROR_MESSAGE_FETCH_MATCH_TEAM =
            "Issue while Fetching teams for Match";
    public final static String ERROR_MESSAGE_CREATE_MATCH =
            "Issue while Creating Match";
    public final static String ERROR_MESSAGE_RETRIEVE_MATCH =
            "Issue while Retrieving Match";
    public final static String ERROR_MESSAGE_RETRIEVE_MATCHES =
            "Issue while Retriving Matches";
    public final static String ERROR_MESSAGE_UPDATE_MATCH =
            "Issue while Updating Match";
    public final static String ERROR_MESSAGE_DELETE_MATCH =
            "Issue while Deleting Match";
    public final static String ERROR_MESSAGE_FETCH_MATCHES_COUNT =
            "Issue while Fetching Matches count";
    public final static String ERROR_MESSAGE_CREATE_USER =
            "Issue while Creating User";
    public final static String ERROR_MESSAGE_RETRIEVE_USER =
            "Issue while Retrieving User";
    public final static String ERROR_MESSAGE_UPDATE_USER =
            "Issue while Updating User";
}
